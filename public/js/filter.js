 $('#year').select2({
   placeholder: "Select year",
allowClear: true
  });
    $('#syllabas').select2({
   placeholder: "Select syllabas",
allowClear: true
  });
    $('#class').select2({
   placeholder: "Select classes",
allowClear: true
  });
    $('#type').select2({
   placeholder: "Select Question paper type",
allowClear: true
  });
  //var $eventLog = $(".js-event-log");
var $yearSelect = $("#year");
var $syllabasSelect = $("#syllabas");
var $classSelect = $("#class");
var $qtypeSelect = $("#type");



$yearSelect.on("select2:select", function (e) { log("year", e); });
$syllabasSelect.on("select2:select", function (e) { log("syllabas", e); });
$classSelect.on("select2:select", function (e) { log("class", e); });
$qtypeSelect.on("select2:select", function (e) { log("type", e); });

$yearSelect.on("select2:unselect", function (e) { log("year", e); });
$syllabasSelect.on("select2:unselect", function (e) { log("syllabas", e); });
$classSelect.on("select2:unselect", function (e) { log("class", e); });
$qtypeSelect.on("select2:unselect", function (e) { log("type", e); });

  function log (name, evt) {
  if (!evt) {
    var args = "{}";
  } else {
    var args = JSON.stringify(evt.params, function (key, value) {
      if (value && value.nodeName) return "[DOM node]";
      if (value instanceof $.Event) return "[$.Event]";
      return value;
    });
  }
          var selected = {};
          var years = [];
          var syllabas = [];
          var classes =[];
          var type = [];
 $("select").each(function(index, brand){
           switch ($(this).attr("name")) {
                 case "year":
                   years.push($(this).val());
                 break;
                 case "syllabas":
                   syllabas.push($(this).val());
                 break;
                case "class":
                   classes.push($(this).val());
                 break;
                case "type":
                type.push($(this).val());
                 break;
           }
 });
 selected['year'] = years.join(',');
 selected['syllabas'] = syllabas.join(',');
 selected['class'] = classes.join(',');
 selected['type'] = type.join(',');
console.log(selected);
$_token = "{!! csrf_token() !!}";
  $.ajax({
                url: '?page=1',
                dataType: 'json',
                data: {
                    q: selected,_token: $_token 
                },
                error: function() {
                    alert("Error loading server data.. Please try after sometime");
                },
                success: function(data) {
                   $(".qpaperdata").html(data);
                }
            });
}