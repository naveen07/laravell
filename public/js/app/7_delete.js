	//$('.rest-delete').ready(function() {
	var restful = {
 
		// TODO: add various configurations, e.g.
		// - do_confirm: [ true | false ]
		// - confirm_message: "Are you sure?"
		// - do_remove_parent: [ true | false ]
		// - parent_selector: '.li' '.div' ...
		// - success: (closure)
 
		init: function(elem) {
			elem.on('click', function(e) {
				self=$(this);
				e.preventDefault();
 
				if(confirm('Are you sure?')) {
				  $.ajax({
				    url: self.attr('href'),
                                    data: {
                                      _token: self.attr('csrf-token')
                                    },
				    method: 'DELETE',
				    success: function(data) {
				      self.closest('li').remove(); // todo: make configurable
				    },
				    error: function(data) {
				    	alert("Error while deleting.");
				    	console.log(data);
				    }
				  });
				};
			})
		},
	}
 
	restful.init($('.rest-delete'));
	//});