 $(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            } else {
                getPosts(page);
            }
        }
    });
    $(document).ready(function() {
        $(document).on('click', '.pagination a', function (e) {   
             e.preventDefault();
            var json_data={};
            var data = URI($(this).attr('href')).query(true);
            console.log(data);
           $.each(data,function(index, brand){
              console.log(index);
              console.log(brand);
              switch (index) {
                 case "q[year]":
                  json_data['year'] =brand;
                 break;
                 case "q[syllabas]":
                   json_data['syllabas'] =brand;
                 break;
                case "q[class]":
                   json_data['class'] =brand;
                 break;
                case "q[type]":
                json_data['type'] =brand;
                 break;
           }
            
           });
           getPosts(decodeURIComponent($(this).attr('href').split('page=')[1]),json_data);
        });
    });
    
    function getPosts(page,json_data) {
        
        $_token = "{!! csrf_token() !!}";
          $.ajax({
                url: '?page='+page,
                dataType: 'json',
                data: {
                    q: json_data,_token: $_token 
                },
                error: function() {
                    alert("Error loading server data.. Please try after sometime");
                },
                success: function(data) {
                   $(".qpaperdata").html(data);
                }
            });
    }