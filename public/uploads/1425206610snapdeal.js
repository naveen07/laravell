window.XD = function() {
    var E, D, B = 1,
        C, A = this;
    return {
        postMessage: function(F, H, G) {
            if (!H) {
                return
            }
            G = G || parent;
            if (A.postMessage) {
                G.postMessage(F, H.replace(/([^:]+:\/\/[^\/]+).*/, "$1"))
            } else {
                if (H) {
                    G.location = H.replace(/#.*$/, "") + "#" + (+new Date) + (B++) + "&" + F
                }
            }
        },
        receiveMessage: function(G, F) {
            if (A.postMessage) {
                if (G) {
                    C = function(H) {
                        if ((typeof F === "string" && H.origin !== F) || (Object.prototype.toString.call(F) === "[object Function]" && F(H.origin) === !1)) {
                            return !1
                        }
                        G(H)
                    }
                }
                if (A.addEventListener) {
                    A[G ? "addEventListener" : "removeEventListener"]("message", C, !1)
                } else {
                    A[G ? "attachEvent" : "detachEvent"]("onmessage", C)
                }
            } else {
                E && clearInterval(E);
                E = null;
                if (G) {
                    E = setInterval(function() {
                        var I = document.location.hash,
                            H = /^#?\d+&/;
                        if (I !== D && H.test(I)) {
                            D = I;
                            G({
                                data: I.replace(H, "")
                            })
                        }
                    }, 100)
                }
            }
        }
    }
}();
Array.prototype.remove = function(A) {
    return this.splice(this.indexOf(A), 1)
};
Array.prototype.unique = function() {
    var A = this.concat();
    for (var C = 0; C < A.length; ++C) {
        for (var B = C + 1; B < A.length; ++B) {
            if (A[C] === A[B]) {
                A.splice(B--, 1)
            }
        }
    }
    return A
};
if (!Array.prototype.filter) {
    Array.prototype.filter = function(C) {
        if (this === void 0 || this === null) {
            throw new TypeError()
        }
        var F = Object(this);
        var A = F.length >>> 0;
        if (typeof C !== "function") {
            throw new TypeError()
        }
        var E = [];
        var B = arguments.length >= 2 ? arguments[1] : void 0;
        for (var D = 0; D < A; D++) {
            if (D in F) {
                var G = F[D];
                if (C.call(B, G, D, F)) {
                    E.push(G)
                }
            }
        }
        return E
    }
}
if ("function" !== typeof Array.prototype.reduce) {
    Array.prototype.reduce = function(E) {
        if (null === this || "undefined" === typeof this) {
            throw new TypeError("Array.prototype.reduce called on null or undefined")
        }
        if ("function" !== typeof E) {
            throw new TypeError(E + " is not a function")
        }
        var C = Object(this),
            A = C.length >>> 0,
            B = 0,
            D;
        if (arguments.length >= 2) {
            D = arguments[1]
        } else {
            while (B < A && !B in C) {
                B++
            }
            if (B >= A) {
                throw new TypeError("Reduce of empty array with no initial value")
            }
            D = C[B++]
        }
        for (; B < A; B++) {
            if (B in C) {
                D = E(D, C[B], B, C)
            }
        }
        return D
    }
}
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(C, D) {
        for (var B = (D || 0), A = this.length; B < A; B++) {
            if (this[B] === C) {
                return B
            }
        }
        return -1
    }
}
if (!Array.prototype.lastIndexOf) {
    Array.prototype.lastIndexOf = function(C) {
        if (this == null) {
            throw new TypeError()
        }
        var D = Object(this);
        var A = D.length >>> 0;
        if (A === 0) {
            return -1
        }
        var E = A;
        if (arguments.length > 1) {
            E = Number(arguments[1]);
            if (E != E) {
                E = 0
            } else {
                if (E != 0 && E != (1 / 0) && E != -(1 / 0)) {
                    E = (E > 0 || -1) * Math.floor(Math.abs(E))
                }
            }
        }
        var B = E >= 0 ? Math.min(E, A - 1) : A - Math.abs(E);
        for (; B >= 0; B--) {
            if (B in D && D[B] === C) {
                return B
            }
        }
        return -1
    }
}
if (!Array.prototype.map) {
    Array.prototype.map = function(C) {
        if (this === void 0 || this === null) {
            throw new TypeError()
        }
        var F = Object(this);
        var A = F.length >>> 0;
        if (typeof C !== "function") {
            throw new TypeError()
        }
        var E = new Array(A);
        var B = arguments.length >= 2 ? arguments[1] : void 0;
        for (var D = 0; D < A; D++) {
            if (D in F) {
                E[D] = C.call(B, F[D], D, F)
            }
        }
        return E
    }
}
var enableConsole = localStorage.getItem("enableConsole");
if ((!window.console || window.env == "prod") && !enableConsole) {
    try {
        _console = console;
        console = {};
        console.log = function() {};
        console.debug = function() {}
    } catch (e) {}
}

function forceEnableConsole() {
    localStorage.setItem("enableConsole", "true")
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "")
};
jQuery.expr[":"].Contains = function(B, C, A) {
    return (B.textContent || B.innerText || "").toUpperCase().indexOf(A[3].toUpperCase()) >= 0
};

function supports_html5_storage() {
    try {
        return "localStorage" in window && window.localStorage !== null
    } catch (A) {
        return false
    }
}
if ($.browser.msie) {
    $("map").remove()
}
jQuery.easing.myEasing = function(B, C, A, E, D) {
    return E * Math.sqrt(1 - (C = C / D - 1) * C) + A
};
jQuery.fn.timer = function(A) {
    $(this).each(function(C, D) {
        var B = $(D).find('input.[name="startTime"]').val();
        var E = new Snapdeal.DealTimer();
        E.init($(D), B - A)
    })
};
if ($.browser.msie) {
    $("html").addClass("ie");
    if (parseInt($.browser.version, 10) == 8) {
        $("html").addClass("ie8")
    }
    if (parseInt($.browser.version, 10) == 9) {
        $("html").addClass("ie9")
    }
}
if ($.browser.mozilla && $.browser.version == "11.0") {
    $("html").addClass("ie")
}
if (navigator.userAgent.indexOf("Mac") > 0) {
    $("body").addClass("mac-os")
}

function detectCSSFeature(E) {
    var D = false,
        C = "Webkit Moz ms O".split(" "),
        F = document.createElement("div"),
        A = null;
    E = E.toLowerCase();
    if (F.style[E]) {
        D = true
    }
    if (D === false) {
        A = E.charAt(0).toUpperCase() + E.substr(1);
        for (var B = 0; B < C.length; B++) {
            if (F.style[C[B] + A] !== undefined) {
                D = true;
                break
            }
        }
    }
    return D
};
if (typeof Snapdeal == "undefined") {
    Snapdeal = {}
}
Snapdeal.Cookie = {};
Snapdeal.Cookie.data = "data";
Snapdeal.fxW = {};
Snapdeal.Cookie.set = function(E, C, A, G, F) {
    var B = new Date();
    B.setTime(B.getTime() + (A * 24 * 3600 * 1000));
    var D = E + "=" + escape(C) + ((A) ? ";expires=" + B.toGMTString() : "") + ((G) ? ";path=" + G : ";path=/") + ((F) ? ";domain=" + F : ";domain=.snapdeal.com");
    document.cookie = D
};
Snapdeal.Cookie.remove = function(A, C, B) {
    document.cookie = A + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT" + ((C) ? ";path=" + C : ";path=/") + ((B) ? ";domain=" + B : ";domain=.snapdeal.com")
};
Snapdeal.Cookie.get = function(A) {
    if (document.cookie.length > 0) {
        begin = document.cookie.indexOf(" " + A + "=");
        if (begin != -1) {
            begin += A.length + 2;
            end = document.cookie.indexOf(";", begin);
            if (end == -1) {
                end = document.cookie.length
            }
            return unescape(document.cookie.substring(begin, end))
        } else {
            begin = document.cookie.indexOf(A + "=");
            if (begin != -1) {
                begin += A.length + 1;
                end = document.cookie.indexOf(";", begin);
                if (end == -1) {
                    end = document.cookie.length
                }
                return unescape(document.cookie.substring(begin, end))
            }
        }
    }
    return null
};
Snapdeal.Cookie.remove = function(B) {
    var A = B + "=;expires=Thu, 01-Jan-1970 00:00:01 GMT";
    document.cookie = A
};
Snapdeal.StateList = {};
Snapdeal.StateList.get = function() {
    var A = ["Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chhattisgarh", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal"];
    return A
};
Snapdeal.CommonUtils = {};
Snapdeal.CommonUtils.getQueryStringParameter = function(B) {
    B = B.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var A = "[\\?&]" + B + "=([^&#]*)";
    var D = new RegExp(A);
    var C = D.exec(unescape(window.location.href));
    if (C == null) {
        return null
    } else {
        return C[1]
    }
};
Snapdeal.CommonUtils.validateNumeric = function(A) {
    if (!A) {
        var A = window.event
    }
    if (!A.which) {
        keyPressed = A.keyCode
    } else {
        keyPressed = A.which
    } if (keyPressed >= 48 && keyPressed <= 57 || keyPressed == 8 || keyPressed == 9 || keyPressed > 38 && keyPressed < 40) {
        keyPressed = keyPressed;
        return true
    } else {
        keyPressed = 0;
        return false
    }
};
(function() {
    $.fn.field = function(B, C) {
        if (typeof B != "string") {
            return false
        }
        var D = $(this).find("[name=" + B + "]");
        if (typeof C === "undefined") {
            if (D.length >= 1) {
                switch (D.attr("type")) {
                    case "checkbox":
                        return D.is(":checked");
                        break;
                    case "radio":
                        var A;
                        D.each(function(E, F) {
                            if ($(this).is(":checked")) {
                                A = $(this).val()
                            }
                        });
                        return A;
                        break;
                    default:
                        return D.val();
                        break
                }
            } else {
                return null
            }
        } else {
            switch (D.attr("type")) {
                case "checkbox":
                    D.attr({
                        checked: C
                    });
                    break;
                case "radio":
                    D.each(function(E) {
                        if ($(this).val() == C) {
                            $(this).attr({
                                checked: true
                            })
                        }
                    });
                    break;
                case undefined:
                    $(this).append('<input type="hidden" name="' + B + '" value="' + C + '" />');
                    break;
                default:
                    D.val(C);
                    break
            }
            return D
        }
    }
})();
Snapdeal.Utils = function() {
    return {
        fixArray: function(A) {
            if ($.isArray(A)) {
                return A
            } else {
                return [A]
            }
        },
        filterUndef: function(A) {
            if (A.filter) {
                return A.filter(function(B) {
                    return !!B
                })
            } else {
                return $.grep(A, function(B) {
                    return !!B
                })
            }
        }
    };
    return {
        dateFormat: function(B) {
            var C = B.getDate();
            var A = B.getMonth() + 1;
            var D = B.getFullYear();
            return "" + D + "-" + (A <= 9 ? "0" + A : A) + "-" + (C <= 9 ? "0" + C : C)
        }
    }
}();
Snapdeal.fxW.applyBxSlider = function() {
    $(".isBXSlider").not(".sliderHasBeenAttached").each(function() {
        if ($(this).children().length > 1) {
            $(this).bxSlider({
                nextText: "",
                prevText: "",
                auto: false,
                pager: false,
                infiniteLoop: true,
                hideControlOnEnd: true
            }).addClass("sliderHasBeenAttached")
        }
    })
};
$(function() {
    var A = $("html");
    if ($.browser.msie) {
        A.addClass("ie");
        if (/^8/.test($.browser.version)) {
            A.addClass("ie8")
        }
    }
});

function MigrateHPCLCookie() {
    var B = Snapdeal.Cookie.get("hpcl");
    if (B) {
        var E = B.split("|");
        var C = E.length;
        var D = [];
        for (var A = 0; A < C; ++A) {
            if (E[A] == parseInt(E[A])) {
                return 0
            }
            if (E[A].split("-")[1]) {
                D.push(E[A].split("-")[1])
            }
        }
        Snapdeal.Cookie.set("hpcl", D.join("|"))
    }
}

function updateHPCLCookie(E) {
    if (E) {
        MigrateHPCLCookie();
        var G = E;
        var F = Snapdeal.Cookie.get("hpcl");
        if (!F) {
            Snapdeal.Cookie.set("hpcl", G, 90)
        } else {
            var B = F.split("|");
            var H = false;
            var A;
            for (var D = 0; D < B.length; D++) {
                if (E == B[D]) {
                    A = D;
                    H = true;
                    break
                }
            }
            B.splice(0, 0, G);
            if (H) {
                B.splice(A + 1, 1)
            }
            B.length = Math.min(6, B.length);
            var C = B.join("|");
            C = SanitizeHPCLCookie(C);
            Snapdeal.Cookie.set("hpcl", C, 90)
        }
    }
}

function SanitizeHPCLCookie(A) {
    A = A.replace("||", "|");
    if (A.charAt(0) == "|") {
        A = A.substring(1, A.length)
    }
    if (A.charAt(A.length - 1) == "|") {
        A = A.substring(0, (A.length - 1))
    }
    return A
}

function getDate(B) {
    var E = new Date(B);
    var C = E.getDate();
    var A = E.getMonth() + 1;
    var D = E.getFullYear();
    return norm(C) + "/" + norm(A) + "/" + D
}

function norm(A) {
    return (A <= 9 ? "0" + A : A)
}

function treatAsUTC(B) {
    var A = new Date(B);
    A.setMinutes(A.getMinutes() - A.getTimezoneOffset());
    return A
}

function daysBetween(B, C) {
    var A = 24 * 60 * 60 * 1000;
    return (treatAsUTC(C) - treatAsUTC(B)) / A
}
$(window).load(function() {
    window.isLoaded = true
});

function escapeHtml(A) {
    var B = document.createElement("div");
    B.appendChild(document.createTextNode(A));
    return B.innerHTML
}

function unescapeHtml(A) {
    var C = document.createElement("div");
    C.innerHTML = A;
    var B = C.childNodes[0];
    return B ? B.nodeValue : ""
}
var timer;
$(".sdTitleTip").live("mouseover", function() {
    if ($(".sdTitleTipContainer").length == 0) {
        var A = this;
        clearTimeout(timer);
        timer = setTimeout(function() {
            sdTitleTip($(A))
        }, 500)
    }
});
$(".sdTitleTip").live("mouseleave", function() {
    clearTimeout(timer);
    $(".sdTitleTipContainer").fadeOut("fast", function() {
        $(".sdTitleTipContainer").remove()
    })
});

function sdTitleTip(B) {
    var A = '<div class="sdTitleTipContainer">' + $(B).attr("sdtitle") + "</div>";
    $(B).append(A);
    $(".sdTitleTipContainer").css("margin-left", -parseInt(($(".sdTitleTipContainer").width() - $(B).width() + 16) / 2));
    $(".sdTitleTipContainer").fadeIn()
}

function logUToUIDMapping() {}
$(document).click(function(D) {
    var C = D.target;
    var E = C.children;
    var A = $(".commonClose");
    var B = "";
    if (!E) {
        A.hide()
    }
    if (E.length < 1) {
        if (!$(C).parents(".commonClose").length > 0) {
            A.hide()
        }
    }
    if (E && E[0] && !$.attr(E[0], "class")) {
        if (!$(C).parents(".commonClose").length > 0) {
            A.hide()
        }
    }
    if (E && E[0] && (B = $.attr(E[0], "class")) && B.indexOf("commonClose") == -1) {
        if (!$(C).parents(".commonClose").length > 0) {
            A.hide()
        }
    } else {
        if (E && E[0] && (B = $.attr(E[0], "class")) && B.indexOf("commonClose") != -1) {
            A.hide();
            $(E).show()
        }
    }
});
$(".joinTheTeam").click(function() {
    Snapdeal.omni.fire("headerJoinTheTeam", {})
});
window.windowLoaded = $.Deferred();
$(window).load(function() {
    windowLoaded.resolve()
});
$(function() {
    $("#system_message .close").click(function() {
        $("#system_message").slideToggle("fast")
    });
    $(".icon-collaps").live("click", function() {
        $(this).toggleClass("icon-expand");
        collapsDiv = $(this).parent().siblings('div[collaps=""]');
        if (collapsDiv.is(":visible")) {
            collapsDiv.slideUp("fast", function() {
                if (window.updateSdScroll) {
                    updateSdScroll()
                }
            })
        } else {
            collapsDiv.slideDown("fast", function() {
                if (window.updateSdScroll) {
                    updateSdScroll()
                }
            })
        }
    });
    $(".node").live("click", function() {
        $(this).toggleClass("upCategory").parent("li").next("ul").slideToggle()
    });
    $(".moreSubcat").live("click", function() {
        $(this).toggleClass("lessSubcat").prev().slideToggle()
    });
    $(".know-more-close").click(function() {
        $(".know-more-outer").hide();
        return false
    });
    if ($("#categoryId").val() != "" && $("#categoryId").val() != null) {
        if ($("#changeBackToAll").val() != "true") {
            $('a[catId="' + $("#categoryId").val() + '"][vid="' + $("#vertical").val() + '"]').click()
        }
    }
    $(".collectDemographs").each(function() {
        var B = new Snapdeal.collectDemographWidget();
        B.render(this)
    });
    $("#back-top").hide();
    $(window).scroll(function() {
        if ($(this).scrollTop() > 1000) {
            $("#back-top").fadeIn()
        } else {
            $("#back-top").fadeOut()
        }
    });
    $("#back-top a").click(function() {
        $("body,html").animate({
            scrollTop: 0
        }, 800);
        return false
    });
    $(".recommendViewCookie").live("click", function() {
        if (Snapdeal.signupWidConfig.loggedIn) {
            window.location.href = httpPath + "/myrecommendations"
        } else {
            showLogin("/myrecommendations")
        }
    });
    searchUrlUpdate();
    Snapdeal.headerVersion = (Snapdeal.Cookie.get("alps") || "2");
    if (document.addEventListener) {
        document.addEventListener("keydown", A, false)
    } else {
        document.attachEvent("onkeydown", A)
    }

    function A(C) {
        var B = document.activeElement;
        if (B.tagName == "INPUT") {
            if (C.keyCode == 40 && (B.value == "" || B.value == B.placeholder)) {
                $(this).focusout();
                window.scrollBy(0, 90)
            } else {
                if (C.keyCode == 38 && (B.value == "" || B.value == B.placeholder)) {
                    window.scrollBy(0, -90)
                }
            }
        }
    }
});
$(window).load(function() {
    if (loadingTime == 0) {
        loadingTime = (new Date().getTime() - startTime) / 1000
    }
    var A = Snapdeal.getAjaxResourcesPath("/" + $(".ajaxFooter").attr("u"));
    if (A != null && A != "") {
        $(".ajaxFooter").load(A, function() {
            $(".lazyBg").removeClass("lazyBg")
        })
    }
    if (abEnabled != null && abEnabled != undefined && abEnabled == true) {
        loadABtesting()
    }
});
$(window).load(function() {
    if (loadingTime == 0) {
        loadingTime = (new Date().getTime() - startTime) / 1000
    }
    checkAuth(function() {
        isAuthenticated = false
    }, function() {
        isAuthenticated = true
    })
});

function checkifUserHasClosedGutterImageInPast() {
    if (window.currentGutterImageName == null) {
        return
    }
    var A = true;
    if (typeof(Storage) !== "undefined") {
        if (window.localStorage.getItem("closedGutterImageName") != null) {
            if (window.currentGutterImageName != window.localStorage.getItem("closedGutterImageName")) {
                A = false
            }
        } else {
            A = false
        }
    }
    return A
}

function AddImageAfterLoadEvent() {
    if (checkifUserHasClosedGutterImageInPast()) {
        $("#gutterPromotionContainer").hide();
        return
    }
    if (window.gutterPromoImageSource == null) {
        return
    }
    var B = new Image();
    B.src = window.gutterPromoImageSource;
    B.onload = function() {
        document.getElementById("hideGutterPromotion").style.display = "block"
    };
    var A = document.getElementById("gutterPromoImage");
    A.appendChild(B)
}
if (window.addEventListener) {
    window.addEventListener("load", AddImageAfterLoadEvent, false)
} else {
    if (window.attachEvent) {
        window.attachEvent("onload", AddImageAfterLoadEvent)
    } else {
        window.onload = AddImageAfterLoadEvent
    }
}
$(document).ready(function() {
    getTrackingParams();
    $("#hideGutterPromotion").click(function() {
        $("#gutterPromotionContainer").attr("style", "display:none !important");
        if (typeof(Storage) !== "undefined") {
            if ($("#gutterPromoImage img").attr("src")) {
                var B = $("#gutterPromoImage img").attr("src").split("/");
                var A = B.length;
                B = B[A - 1];
                window.localStorage.setItem("closedGutterImageName", B)
            }
        }
    })
});
var noHeaderFreeze = false;
$(window).scroll(function() {
    if (document.documentElement.scrollTop >= (window.isBrandPage ? 1 : 146) || window.pageYOffset >= (window.isBrandPage ? 1 : 146)) {
        if (Snapdeal.headerVersion == "2") {
            if ($(".ui-autocomplete").is(":visible")) {
                $(".ui-autocomplete").css({
                    position: "fixed",
                    top: 47
                })
            } else {
                $(".ui-autocomplete").css({
                    position: "absolute",
                    top: 1
                })
            }
        } else {
            if ($(".ui-autocomplete").is(":visible")) {
                $(".ui-autocomplete").css({
                    position: "fixed",
                    top: 39,
                    width: "619px !important"
                })
            } else {
                $(".ui-autocomplete").css({
                    position: "absolute",
                    top: 1
                })
            }
        } if ($(".header-fixed").size() == 0) {
            if (!window.isBrandPage) {
                $("#headerFixed").css({
                    "max-height": 0,
                    overflow: "hidden"
                });
                $("#headerTop").css("display", "none");
                $("#headerFixed").addClass("header-fixed");
                $(".allCategoriesWrapper").addClass("notVisible").delay(250).queue(function() {
                    $(".allCategoriesWrapper").removeClass("notVisible").clearQueue()
                });
                $("#headerFixed").animate({
                    "max-height": "120px"
                }, 600, function() {
                    $("#headerFixed").css({
                        overflow: "visible"
                    })
                })
            } else {
                $("#headerFixed").addClass("header-fixed")
            }
            $(".saffron_grad,.green_grad").css("height", "58");
            $(".ui-autocomplete").hide();
            $("#compareProductsOuter").addClass("fixed").css("margin-top", 57);
            if (Snapdeal.headerVersion == "2") {
                if ($("#headerFixed").hasClass("header-fixed") && $("#headerFixed").hasClass("hd-rvmp")) {
                    $(".allCategoriesWrapper label").hide();
                    $(".allCategoriesWrapper").addClass("navFixedSmall");
                    $("#leftNavNemu").addClass("leftnavFixedSmall")
                }
            }
        }
    } else {
        removeHeaderFreeze()
    }
});

function removeHeaderFreeze() {
    if ($("#headerFixed").hasClass("header-fixed")) {
        $("#headerFixed").removeClass("header-fixed");
        $(".saffron_grad,.green_grad").css("height", "58");
        if (!window.isBrandPage) {
            $("#headerTop").css("display", "block");
            $(".saffron_grad,.green_grad").css("height", "83")
        }
        $("#headerFixed").stop().css({
            "max-height": "120px",
            overflow: "visible"
        });
        $(".ui-autocomplete").hide();
        $("#compareProductsOuter").removeClass("fixed").css("margin-top", 0);
        if (Snapdeal.headerVersion == "2") {
            $(".allCategoriesWrapper").removeClass("navFixedSmall");
            $("#leftNavNemu").removeClass("leftnavFixedSmall");
            $(".allCategoriesWrapper label").show()
        }
    }
}
if (Snapdeal.headerVersion == "2") {
    $("#loggedInAccount,#loggedOutAccount,.accDetails").live("mouseover", function() {
        $(".accDetails").css({
            display: "block",
            "z-index": 999
        })
    });
    $("#loggedInAccount,#loggedOutAccount,.accDetails").live("mouseout", function() {
        $(".accDetails").css("display", "none")
    })
}

function ratingStars() {
    $(".ratingStarsSmall").each(function(A) {
        var B = Number(($(this).attr("ratings") || ""));
        if ($(this).attr("ratings") != undefined && $(this).attr("ratings") != "") {
            multiplier = imageOffsetForRating(B);
            $(this).css("background-position", "0px -" + (multiplier * 10) + "px")
        }
    })
}

function imageOffsetForRating(A) {
    if (A < 0.2) {
        return 0
    } else {
        if (A < 0.8) {
            return 1
        } else {
            if (A < 1.2) {
                return 2
            } else {
                if (A < 1.8) {
                    return 3
                } else {
                    if (A < 2.2) {
                        return 4
                    } else {
                        if (A < 2.8) {
                            return 5
                        } else {
                            if (A < 3.2) {
                                return 6
                            } else {
                                if (A < 3.8) {
                                    return 7
                                } else {
                                    if (A < 4.2) {
                                        return 8
                                    } else {
                                        if (A < 4.8) {
                                            return 9
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return 10
}

function textForRating(A) {
    if (A < 1) {
        return "Rate this product!"
    } else {
        if (A < 1.1) {
            return "Not Good"
        } else {
            if (A < 2.1) {
                return "Needs That Special Something"
            } else {
                if (A < 3.1) {
                    return "Average, Ordinary"
                } else {
                    if (A < 4.1) {
                        return "That's Good Stuff"
                    }
                }
            }
        }
    }
    return "Perfect. It doesn't get any better"
}

function textForRating1(A) {
    if (A < 1) {
        return "Rate this product!"
    } else {
        if (A < 1.1) {
            return "Not Good"
        } else {
            if (A < 2.1) {
                return "Below Average"
            } else {
                if (A < 3.1) {
                    return "Average, Ordinary"
                } else {
                    if (A < 4.1) {
                        return "That's Good Stuff"
                    }
                }
            }
        }
    }
    return "Perfect!"
}

function loadTopOffersAndPersonal() {
    if ($(".left-product-banners").size() > 0 && (selectedTab == "hp" || activeProductTab == "ph") && !isPersonalizedHP) {
        function C() {
            homePageTopOfferWidget();
            $(".leftNavPersonalizationWidget").each(function() {
                getHomePagePersonalizationWidgetProducts($(this).attr("id").split("-")[1])
            });
            if (Snapdeal.componentSwitches.recSysClosed == "false") {
                loadTrendingNowProductFeed()
            }
            lazySrc()
        }
        if (window.isLoaded) {
            C()
        } else {
            $(window).load(function() {
                C()
            })
        }
    } else {
        if (isPersonalizedHP) {
            function A() {
                try {
                    if (window.localStorage.getItem("personalizedHPWalkThru") == null) {
                        window.localStorage.setItem("personalizedHPWalkThru", true);
                        $(".pageOverlay.startPersonalisedTour").show();
                        $(".highlighter").show();
                        $(".popover").show()
                    }
                } catch (D) {}
                getPersonalizedHPRecentlyViewedWidget();
                if (hpCatWidgetsCount >= 2) {
                    homePageTopOfferWidget()
                }
                if (Snapdeal.componentSwitches.recSysClosed == "false") {
                    loadPersonalizedProductFeed()
                }
            }
            if (window.isLoaded) {
                A()
            } else {
                $(window).load(function() {
                    A()
                })
            }
        } else {
            if (Snapdeal.Cookie.get("alps") == "2") {
                function B() {
                    homePageTopOfferWidget()
                }
                if (window.isLoaded) {
                    B()
                } else {
                    $(window).load(function() {
                        B()
                    })
                }
            }
        }
    }
}

function homePageTopOfferWidget() {
    var A = Snapdeal.getAjaxResourcesPath("/getTopOffers");
    $.ajax({
        url: A,
        type: "GET",
        dataType: "json",
        success: function(B) {
            if (B != null) {
                displayHomePageTopOfferWidget(B)
            }
            lazySrc()
        },
        error: function() {}
    })
}

function displayHomePageTopOfferWidget(E) {
    var A = "";
    var C = "";
    var D = 0;
    $.each(E.topOffer, function(F, G) {
        if (G.offerHeading != "") {
            D++
        }
    });
    if (E != undefined && E != null && E != "" && E.topOffer.length != 0 && D > 0) {
        C += "<h3>Today's Top Offers <a class='viewallwidgets somn-track' hidOmnTrack='HID=TopOffers_viewAll' href='http://www.snapdeal.com/offers/best-discounts'>View All ></a></h3>";
        C += "<div class='left-outer-box'>";
        var B = "";
        $.each(E.topOffer, function(F, G) {
            if (G.offerLink != "" && G.offerHeading != "" && G.imageIcon != "" && G.offerText != "") {
                B += '<div class="topOfferWrapper somn-track" hidOmnTrack="TopOffer_' + (F + 1) + '">';
                if (G.offerLink != "null" && G.offerLink != "") {
                    B += '<a href="' + G.offerLink + '">'
                }
                B += '<div class="topOffer-img">';
                B += '<img src="' + G.imageIcon + '" alt="' + G.offerHeading + '" height="43" width="37">';
                B += "</div>";
                B += '<div class="topOffer-content">';
                B += "<div>" + G.offerHeading + "</div>";
                B += '<div class="promoText">' + G.offerText + "</div>";
                B += "</div>";
                if (G.offerLink != "null" && G.offerLink != "") {
                    B += "</a>"
                }
                B += "</div>"
            }
        });
        B += "</div>"
    }
    $("#homePageTopOffer").html(C + B);
    $(".topOffersHeaderDrop .left-outer-box").html(B);
    $(".topOffer-img img[lazySrc]").each(function() {
        if (!$(this).attr("src")) {
            $(this).attr("src", $(this).attr("lazySrc"))
        }
    })
}
$(window).load(function() {
    try {
        if (localStorage.getItem("scrollToShoppingFeed") != null) {
            localStorage.removeItem("scrollToShoppingFeed");
            $("body").scrollTo(($("#productFeedHdr").offset().top - 251), {
                duration: "slow"
            })
        }
    } catch (A) {}
});
$(".popover-content .buyBlueButton, .shoppingFeed").click(function() {
    try {
        if (selectedTab != "hp") {
            localStorage.setItem("scrollToShoppingFeed", "true");
            window.location.href = "/"
        } else {
            $("body").scrollTo(($("#productFeedHdr").offset().top - 251), {
                duration: "slow"
            });
            $(".pageOverlay.startPersonalisedTour").hide();
            $(".highlighter").hide();
            $(".popover").hide();
            getOmnitureScript({}, "viewShoppingFeed")
        }
    } catch (A) {}
});
Snapdeal.HomePage = function() {
    var A = this;
    this.init = function() {
        loadTopOffersAndPersonal()
    };
    this.loadScript = function(C) {
        var B = $("<script/>");
        B.attr("type", "text/javascript");
        B.attr("src", C);
        B.appendTo($("head"))
    };
    this.loadImage = function(E) {
        var C = $(E);
        var B = '<img src="' + C.attr("src") + '" width="' + C.attr("width") + '" alt="' + C.attr("alt") + '"/>';
        var D = $(E.parentNode);
        C.remove();
        D.append(B)
    };
    this.closeSystemMessage = function(B) {
        $("#system_message").slideToggle("fast")
    };
    this.showLoader = function(B) {
        var C = '<div type="sd.AjaxLoader" style="padding:0 20px 0 20px;text-align:center;"><img src="' + Snapdeal.getResourcePath("img/ajax-loader.gif") + '"/></div>';
        B.append(C)
    };
    this.hideLoader = function(B) {
        B.find('div.[type="sd.AjaxLoader"]').each(function(C, D) {
            $(D).remove()
        })
    };
    ratingStars()
};
var page;
var highlights;

function googleScriptLoaded() {
    highlights.scriptLoaded = true;
    highlights.googleScriptLoaded()
}
$(".home-image-hover").mouseover(function() {
    onMouseOver(this)
});
$(".home-image-hover").mouseleave(function() {
    onMouseLeave(this)
});

function onMouseOver(A) {
    $(A).find(".home-image-hover").show()
}

function onMouseLeave(A) {
    $(A).find(".home-image-hover").hide()
}

function shortenUrl(B) {
    var C = "/ext/util/generatesdsu/?lu=" + B;
    var A;
    $.ajax({
        url: C,
        type: "POST",
        dataType: "json",
        async: false,
        success: function(D) {
            if (D.status == "success") {
                A = D.items.su
            } else {
                A = B
            }
        }
    });
    return A
}
$(".home-proddeals-cont").hover(function() {
    $(".cashBackCategoryWidgets").css("text-decoration", "none")
});

function get(A) {
    if (A = (new RegExp("[?&]" + encodeURIComponent(A) + "=([^&]*)")).exec(location.search)) {
        return decodeURIComponent(A[1])
    }
}

function norm(A) {
    return (A <= 9 ? "0" + A : A)
}

function loadABtesting() {
    var D = Snapdeal.Cookie.get("alps");
    var C = abVersion;
    var B;
    if (D == null || D == undefined) {
        var A = getABStatus();
        if (A != null && A != undefined && A == "true") {
            B = getABVersion();
            if (B != null && B != undefined && B != null) {
                Snapdeal.Cookie.set("alps", B, 7);
                var E = Snapdeal.Cookie.get("AWSELB");
                if (E != null && E != undefined) {
                    document.cookie = "AWSELB=" + E + "; expires=0; path=/"
                }
            }
        }
    }
    if (C != Snapdeal.Cookie.get("alps")) {
        Snapdeal.Cookie.set("_alps_miss", "true")
    }
}

function getABStatus() {
    var A = null;
    $.ajax({
        url: "/gas",
        type: "GET",
        async: false,
        success: function(B) {
            A = B
        }
    });
    return A
}

function getABVersion() {
    var A = null;
    $.ajax({
        url: "/gav",
        type: "GET",
        async: false,
        success: function(B) {
            A = B
        }
    });
    return A
}
$(window).load(function() {
    var H = Snapdeal.Cookie.get("bw");
    if (H == null || H == undefined || H == "undefined") {
        var A = $("#logServiceUrl").val();
        if (A != "" && A != undefined) {
            var D = Snapdeal.getResourcePath("/img/snapdeal_bw.jpg") + "?n=" + Math.random();
            var E, C;
            var G = 30469;
            var B = new Image();
            B.onload = function() {
                C = (new Date()).getTime();
                F()
            };
            E = (new Date()).getTime();
            B.src = D;

            function F() {
                var L = (C - E) / 1000;
                var Q = G * 8;
                var O = (Q / L).toFixed(2);
                var N = (O / 1024).toFixed(2);
                var I = (N / 1024).toFixed(2);
                var P = $("#logServiceUrl").val();
                var J = "B/W for user " + Snapdeal.Cookie.get("u") + " is: " + N + "kbps";
                Snapdeal.Cookie.set("bw", N);
                $.ajax({
                    url: P + "/lg/gnl",
                    dataType: "jsonp",
                    cache: false,
                    data: {
                        mssg: J
                    }
                });
                if (window.selectedTab == "hp" && $.browser.webkit && window.performance && window.performance.getEntries) {
                    var M = window.performance.getEntries();
                    if (M.length >= 5 && M[4].initiatorType == "css") {
                        var K = M[4].startTime / 1000;
                        $.ajax({
                            url: $("#logServiceUrl").val() + "/lg/gnl",
                            dataType: "jsonp",
                            cache: false,
                            data: {
                                mssg: "Time before main banner load for user " + Snapdeal.Cookie.get("u") + " is: " + K + "seconds"
                            }
                        })
                    }
                }
            }
        }
    }
});
$(function() {
    initSdScroll()
});

function initSdScroll() {
    $(".sdScroll").each(function() {
        var A = parseInt(($(this).css("maxHeight") || "").split("px")[0]) || 0;
        if ($(this).height() == A) {
            $(this).slimScroll({
                height: A,
                alwaysVisible: true,
                railVisible: true,
                size: "14px",
                color: "#c4c4c4",
                opacity: 1,
                railColor: "#f4f4f4",
                railOpacity: 1,
                borderRadius: 3,
                railBorderRadius: 3
            })
        }
    })
}
$(".sellOnSnapdeal").click(function() {
    Snapdeal.omni.fire("sellOnSnapdealHeaderLink", {})
});
$("#footer-top-text a").each(function() {
    if ($(this).attr("href") != undefined) {
        var A = $(this).attr("href");
        if (A.indexOf("?") == -1) {
            $(this).attr("href", A + "?utm_source=seolinks")
        }
    }
});
$(".brandDescription a").each(function() {
    if ($(this).attr("href") != undefined) {
        var A = $(this).attr("href");
        if (A.indexOf("?") == -1) {
            $(this).attr("href", A + "?utm_source=seolinks")
        }
    }
});
$("#footer-top-text a").live("click", function() {
    Snapdeal.omni.fire("ClickOnFooterLinks", {})
});
$(".brandDescription a").live("click", function() {
    Snapdeal.omni.fire("ClickOnBrandDescLinks", {})
});

function checkIfImagePresentInDfpAd(A) {
    if ($("#" + A + " iframe").contents().find("img")[0]) {
        $("#" + A).css("z-index", 0)
    } else {}
};
if (typeof Snapdeal == "undefined") {
    Snapdeal = {}
}(function() {
    function B(F, G, D) {
        var E = $(F);
        if (E.length == 0) {
            return
        }
        var C = new Image();
        C.src = G;
        E.attr("src", C.src);
        E.addClass("lazy-effect");
        E.parent().addClass("lazy-loader");
        var H = D;
        C.onload = function() {
            if ("naturalHeight" in this) {
                if (this.naturalHeight + this.naturalWidth === 0) {
                    this.onerror();
                    return
                }
            } else {
                if (this.width + this.height == 0) {
                    this.onerror();
                    return
                }
            }
            E.parents(".lazy-loader").eq(0).removeClass("lazy-loader");
            var J = E.attr("anim") == "true";
            if (J) {
                E.appear();
                E.css("opacity", 0);

                function I() {
                    E.removeClass("lazy-effect");
                    E.animate({
                        opacity: 1
                    }, 500)
                }
                if (E.is(":appeared")) {
                    I()
                }
                E.one("appear", I)
            } else {
                E.css("opacity", 1);
                E.removeClass("lazy-effect")
            }
        };
        C.onerror = function() {
            E.css("opacity", 0);
            if (!H) {
                B(E, Snapdeal.getResourcePath("img/no-image.gif"), true)
            }
        }
    }

    function A() {
        $("img[lazySrc]").each(function() {
            var D = $(this).attr("src") || "";
            var C = $(this).attr("alt") || "";
            B($(this), $(this).attr("lazySrc"));
            $(this).removeAttr("lazySrc");
            if (!C.trim()) {
                $(this).attr("alt", $(this).attr("atl"));
                $(this).removeAttr("atl")
            }
        })
    }
    window.lazySrc = A;
    $(window).load(function() {
        $(document).trigger("loadLazyImages")
    });
    $(document).bind("loadLazyImages", function() {
        setTimeout(function() {
            A()
        }, 100)
    });
    setInterval(function() {
        $.force_appear()
    }, 1000)
})();

function isScrolledIntoView(A) {
    if (A != window && $(A).is(":visible")) {
        return $(A).is(":appeared")
    } else {
        return true
    }
}

function sanitizeProductName(B, C) {
    var A = "";
    if (B.length > C) {
        A = B.substring(0, C - 1) + "..."
    } else {
        A = B
    }
    return A
}(function() {
    window.handleTextOverFlow = function(B, A) {
        B.addClass("textOverflowWrapper");
        B.each(function() {
            var C = $(this);
            C = $(C);
            var E = C.html();
            var F = E.length - 1;
            var D = (F > A) ? E.substring(0, A) + '<br><span class="textOverflow-more">more <i class="down-whiteArrow"></i></span><span class="descriptionDetail"></span><br><span class="textOverflow-less">less <i class="up-whiteArrow"></i></span>' : E;
            var G = (F > A) ? E.substring(A, F) : "";
            C.html(D);
            C.find(".descriptionDetail,.textOverflow-less").hide();
            C.find(".descriptionDetail").html(G)
        })
    };
    $(".textOverflow-more").live("click", function() {
        var A = $(this).parents(".textOverflowWrapper").eq(0);
        A.find(".descriptionDetail,.textOverflow-less").show();
        A.find(".textOverflow-more").hide()
    });
    $(".textOverflow-less").live("click", function() {
        var A = $(this).parents(".textOverflowWrapper").eq(0);
        A.find(".descriptionDetail,.textOverflow-less").hide();
        A.find(".textOverflow-more").show()
    })
})();
var eventContinueShopping = "continueShoppingNew";
var eventCartCrossed = "cartCrossedNew";
Snapdeal.Cart = function() {
    var A = this,
        F, C, D, B;
    var E = true;
    this.init = function(G) {
        F = G
    };
    this.getCartJson = function() {
        return F
    };
    this.reloadCart = function(G) {
        E = true;
        F = G
    };
    this.hideCartMessage = function() {
        E = false
    };
    this.toCartHtml = function() {};
    this.createOneDayTooltip = function(Q, N) {
        var P = "",
            H = false,
            V = false,
            U = false,
            K = false,
            O = false;
        if (!N.expressDeliveryValidated) {
            H = true;
            if (N.sameDayDelivery && isSameDayFlag) {
                K = true
            }
            if (N.nextDayDelivery && isNextDayFlag) {
                O = true
            }
        }
        if (N.sameDayDelivery && N.sameDayDelivery.deliveryDate && isSameDayFlag) {
            U = true
        }
        if (N.nextDayDelivery && N.nextDayDelivery.deliveryDate && isNextDayFlag) {
            V = true
        }
        P += '<div class="oneday-outer-tooltip" data-index=' + Q + ">";
        P += '<div class="total-discout-show"><div class="oneday-arrow-top"></div><div class="oneday-arrow-top oneday-arrow-top-white"></div>';
        if (H) {
            if (K) {
                P += '<div class="sameday-logo"><div class="oneday-logo-aside oneday-maybe"><div class="logo-aside-static"><span class="maybe-width">Order before ' + A.convertIntoTwelveFormat(sameDayCutoffTime) + '</span><span class="oneday-charges">+Rs.' + N.sameDayDelivery.deliveryCharges + '</span><span class="oneday-rightarrow"></span><span class="oneday-clock"></span></div></div></div>'
            }
            if (O) {
                P += '<div class="nextday-logo"><div class="oneday-logo-aside oneday-maybe"><div class="logo-aside-static"><span class="maybe-width">Order before ' + A.convertIntoTwelveFormat(nextDayCutoffTime) + '</span><span class="oneday-charges">+Rs.' + N.nextDayDelivery.deliveryCharges + '</span><span class="oneday-rightarrow"></span><span class="oneday-clock"></span></div></div></div>'
            }
            P += '<div class="oneday-askaddress">Please enter your address to check availability</div>'
        } else {
            if (U) {
                P += '<div class="sameday-logo"><div class="oneday-logo-aside"><div><span>Delivered by <strong>' + A.getCustomDate(N.sameDayDelivery.deliveryDate) + '</strong></span><span class="oneday-charges">+Rs.' + N.sameDayDelivery.deliveryCharges + '</span></div><div class="logo-aside-static">Order by ' + A.convertIntoTwelveFormat(sameDayCutoffTime) + '</div><span class="oneday-rightarrow"></span><span class="oneday-clock"></span></div></div>'
            }
            if (V) {
                P += '<div class="nextday-logo"><div class="oneday-logo-aside"><div><span>Delivered by <strong>' + A.getCustomDate(N.nextDayDelivery.deliveryDate) + '</strong></span><span class="oneday-charges">+Rs.' + N.nextDayDelivery.deliveryCharges + "</span></div>";
                var S = new Date(),
                    G = false;
                var T = S.getHours();
                var M = S.getMinutes();
                var R = this.getCustomDate(S.setDate(S.getDate() + 1));
                var I = nextDayCutoffTime.split(":");
                var L = parseInt(I[0]);
                var J = parseInt(I[1]);
                if (T > L || (T == L && M > J)) {
                    G = true
                } else {
                    G = false
                } if (G) {
                    P += '<div class="logo-aside-static">Order by ' + R + ", " + A.convertIntoTwelveFormat(nextDayCutoffTime) + '</div><span class="oneday-rightarrow"></span><span class="oneday-clock"></span></div></div>'
                } else {
                    P += '<div class="logo-aside-static">Order by ' + A.convertIntoTwelveFormat(nextDayCutoffTime) + '</div><span class="oneday-rightarrow"></span><span class="oneday-clock"></span></div></div>'
                }
            }
        }
        P += "</div></div>";
        return P
    };
    this.convertIntoTwelveFormat = function(H) {
        var I = H.split(":"),
            G = 0,
            J = "";
        hour_12 = I[0] - 12;
        if (hour_12 <= 0) {
            J = H + " AM"
        } else {
            I[0] = hour_12;
            J = I.join(":") + " PM"
        }
        return J
    };
    this.getCustomDate = function(G) {
        var J = new Date(G),
            I = "";
        var H = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        I = J.getDate() + " " + H[J.getMonth()];
        return I
    };
    this.cartItemHtml = function(a, f, M) {
        var P = "";
        var S = a.catalogDTO.sellingPrice * a.quantity;
        var T = 0;
        if (a.subCartItems != undefined) {
            T = A.getPromoCodeValue(Snapdeal.Utils.fixArray(a.subCartItems))
        }
        var R = S - T - (a.internalCashback + a.externalCashback) * a.quantity;
        C += R;
        if (a.catalogDTO.catalogType == "product") {
            D += S - (a.internalCashback + a.externalCashback) * a.quantity;
            B += a.shippingCharges * a.quantity
        }
        var d = a.catalogDTO.maxUnits;
        if (d == -1 || d > 40) {
            d = 40
        }
        var e = '<div class="select-box-wrap-cart">';
        var I = 0,
            V = 0,
            Q = "",
            c = "";
        for (var Y = 1; Y <= d; Y++) {
            if (a.quantity == Y) {
                e += '<input type="text" onkeypress="return Snapdeal.CommonUtils.validateNumeric(event);" onblur="return Snapdeal.UpdateCartItem(this, \'' + a.catalogDTO.catalogIdL + "', this.value, '" + f + "','" + a.vendorCode + "', '" + a.supc + "'," + d + ", 'blur');\" value=\"" + Y + '" class="qtynumber" maxlength=2 />';
                I = Y + 1;
                V = Y - 1;
                if (I > d) {
                    Q = "opc5"
                }
                if (V == 0) {
                    c = "opc5"
                }
            }
        }
        e += '<a class="upArrowQtyCart ' + Q + '" onClick="return Snapdeal.UpdateCartItem(this, \'' + a.catalogDTO.catalogIdL + "', " + I + ", '" + f + "','" + a.vendorCode + "', '" + a.supc + "'," + d + ', \'arrow\');" title="increase quantity by one" ><i>Increase Quantity</i></a>';
        e += '<a class="downArrowQtyCart ' + c + '" onClick="return Snapdeal.UpdateCartItem(this, \'' + a.catalogDTO.catalogIdL + "', " + V + ", '" + f + "','" + a.vendorCode + "','" + a.supc + "'," + d + ', \'arrow\');" title="decrease quantity by one" ><i>Decrease Quantity</i></a>';
        e += "</div>";
        var K = "";
        if (d == 0 || !a.catalogDTO.live) {
            K = "cart-disable"
        }
        var X = a.catalogDTO;
        var Z = "";
        var J = Snapdeal.Utils.fixArray(X.freebie);
        var T = 0;
        if (a.subCartItems != undefined) {
            T = this.getPromoCodeValue(Snapdeal.Utils.fixArray(a.subCartItems))
        }
        if (X.freebie != undefined) {
            var U = J.length;
            if (U == undefined) {
                U = 1;
                J[0] = J
            }
            Z = "<div class='product-freebie'>";
            Z += "<span>Freebie:</span> ";
            for (var O = 0; O < U; O++) {
                if (O != U - 1) {
                    Z += J[O] + " + "
                } else {
                    Z += J[O]
                }
            }
            Z += "</div>"
        }
        var j = "";
        var N = Snapdeal.Utils.fixArray(a.catalogDTO.attributeDTOs);
        if (typeof(a.catalogDTO.attributeDTOs) != "undefined" && N.length > 0) {
            var j = '<div class="cart_attr" style="line-height:18px; font-size:13px; color:#999999;">';
            for (var Y = 0; Y < N.length; Y++) {
                j += N[Y].name + ": " + N[Y].value + " &nbsp; | &nbsp; "
            }
            if (j.length > 0) {
                j = j.substring(0, j.lastIndexOf("|"));
                j += "</div>"
            }
        }
        var H = 0;
        if (T == undefined) {
            T = 0
        }
        H += ((a.internalCashback * a.quantity) + (a.externalCashback * a.quantity) + T);
        P += '<div class="positionRelative">';
        P += '<div class="sold-tag sold-' + K + '"></div><div class="cart-item-cont ' + K + '"><div class="cart-items cart-img-thumb"><img src="' + Snapdeal.getResourcePath(a.catalogDTO.image) + '" width="88" border="0" class="items-image"/></div>';
        P += '<div class="cart-description1 des-' + K + '"><div class="product-title-cart">' + a.catalogDTO.tagline + "</div>" + j + Z + this.getPromoCodeHtml(Snapdeal.Utils.fixArray(a.subCartItems));
        if (a.buyBack) {
            var g = a.productName + " exchanged for " + a.catalogDTO.tagline;
            P += '<div class="cart-buyback"><div class="cart-buyback-amt"><span class="buyback-title">BUYBACK AMOUNT: </span><strong>Rs ' + a.quotedAmount + ' </strong><span class="cart-buyback-help">[?]</span></div><div class="buyback-model"><div title="' + g + '">' + g + "</div></div></div>"
        }
        P += '</div> <div class="cart-quantity">' + e + ' </div> <div class="cart-price" style="color:#2e2e2e;">Rs ' + a.catalogDTO.sellingPrice + ' </div> <div class="cart-delivery-info"> Standard delivery: ';
        if (a.shippingCharges > 0) {
            P += '<span class="cart-dark"><strong> +' + a.shippingCharges * a.quantity + "</strong></span>"
        } else {
            P += '<span class="cart-standard-free"> FREE</span>'
        }
        var L = false,
            G = false,
            h = false,
            b = false,
            W = false;
        if (a.sameDayDelivery && a.sameDayDelivery.deliveryDate && isSameDayFlag) {
            L = true
        }
        if (a.nextDayDelivery && a.nextDayDelivery.deliveryDate && isNextDayFlag) {
            G = true
        }
        if (!a.expressDeliveryValidated) {
            h = true;
            if (a.sameDayDelivery && isSameDayFlag) {
                b = true
            }
            if (a.nextDayDelivery && isNextDayFlag) {
                W = true
            }
        }
        if (h) {
            if (b && W) {
                P += '<div class="cart-oneday"><strong>Same Day </strong><span class="cart-light"> & </span><strong> Next Day </strong><br /><span class="cart-light"> May be Available </span> <span class="cart-light oneday-delivery-help" data-index=' + M + ">[?]</span></div>"
            } else {
                if (b) {
                    P += '<div class="cart-oneday"><strong>Same Day </strong><br /><span class="cart-light"> May be Available </span> <span class="cart-light oneday-delivery-help" data-index=' + M + ">[?]</span></div>"
                } else {
                    if (W) {
                        P += '<div class="cart-oneday"><strong>Next Day </strong><br /><span class="cart-light"> May be Available </span> <span class="cart-light oneday-delivery-help" data-index=' + M + ">[?]</span></div>"
                    }
                }
            }
        } else {
            if (L && G) {
                P += '<div class="cart-oneday"><strong>Same Day </strong><span class="cart-light"> & </span><strong> Next Day </strong><br /><span class="cart-light"> Delivery Available </span> <span class="cart-light oneday-delivery-help" data-index=' + M + ">[?]</span></div>"
            } else {
                if (L) {
                    P += '<div class="cart-oneday"><strong>Same Day </strong><br /><span class="cart-light"> Delivery Available </span> <span class="cart-light oneday-delivery-help" data-index=' + M + ">[?]</span></div>"
                } else {
                    if (G) {
                        P += '<div class="cart-oneday"><strong>Next Day </strong><br /><span class="cart-light"> Delivery Available </span> <span class="cart-light oneday-delivery-help" data-index=' + M + ">[?]</span></div>"
                    }
                }
            }
        }
        P += '</div> <div class="cart-total1"> <strong>Rs ' + R + "</strong>";
        if (H > 0) {
            P += '<div class="total-discout-wrap"><i>You Saved:</i> <div class="discounted-price"><span>Rs ' + H + "</span></div></div>"
        }
        P += '<div class="total-discout-outer">';
        P += '<div class="total-discout-show">';
        P += '<div class="discount-arrow-top"></div>';
        P += this.getCashbackHtmlForCartitem(a);
        if (T > 0) {
            P += '<div style="line-height:12px;font-size:13px;"> Promo Applied: (-) Rs ' + T + "</div>"
        }
        P += '</div></div></div><div class="cart-remove"> <span id="cart-item-remove-' + a.catalogDTO.catalogIdL + '"><span class="remove-cart" title="remove" onClick="return Snapdeal.RemoveCartItem(\'' + a.catalogDTO.catalogIdL + "','" + a.catalogDTO.live + "'," + f + ",'" + a.vendorCode + "','" + a.supc + "');\"></span> </span></div> </div>";
        P += "</div>";
        return P
    };
    this.cartQty = function() {
        var G;
        if (F.cart.cartItems === undefined) {
            G = []
        } else {
            G = Snapdeal.Utils.fixArray(F.cart.cartItems);
            G = Snapdeal.Utils.filterUndef(G)
        }
        return G.length
    };
    this.getCashbackHtmlForCartitem = function(G) {
        var H = "";
        if (G.internalCashback > 0) {
            H += '<div class="fnt12" style="line-height:12px;">Offer Discount: (-) Rs ' + G.internalCashback * G.quantity + "</div>"
        }
        if (G.externalCashback > 0) {
            H += '<div class="fnt12" style="line-height:12px;">Insta Cashback: (-) Rs ' + G.externalCashback * G.quantity + "</div>"
        }
        return H
    };
    this.getCashbackHtmlForCartitemMerge = function(G) {
        var H = "";
        if (G.internalCashback > 0) {
            H += '<div class="fnt10" style="line-height:12px;">Offer Discount: (-) Rs ' + G.internalCashback * G.quantity + "</div>"
        }
        if (G.externalCashback > 0) {
            H += '<div class="fnt10" style="line-height:12px;">Insta Cashback: (-) Rs ' + G.externalCashback * G.quantity + "</div>"
        }
        return H
    };
    this.getPromoCodeHtml = function(H) {
        var I = "";
        var G = [];
        if (H) {
            $.each(H, function(J, K) {
                if (K && K.promoCode) {
                    if ($.inArray(K.promoCode, G) == -1) {
                        I += '<div class="cartPromoWrapper ' + (J == 0 ? "topBorNon" : "") + '"><span>Promo Applied:</span> [' + K.promoCode + "] " + K.promoName + " - Coupon Valid till-" + getDate(K.expDate) + "</div>"
                    }
                    G.push(K.promoCode)
                }
            })
        }
        return I
    };
    this.getPromoCodeValue = function(G) {
        if (G == undefined || G[0] == undefined) {
            return 0
        }
        var H = 0;
        $.each(G, function(I, J) {
            H = H + J.promoValue
        });
        if (H == undefined || isNaN(H)) {
            H = 0
        }
        return H
    };
    this.toHtml = function(M) {
        if (F.cart.unresolvedCart === undefined) {
            var L = "";
            var N = 0;
            var G = 0;
            C = 0;
            D = 0;
            B = 0;
            var J;
            if (F.cart.cartItems === undefined) {
                J = []
            } else {
                J = Snapdeal.Utils.fixArray(F.cart.cartItems);
                J = Snapdeal.Utils.filterUndef(J)
            } if (F.cart.containsFaultyItem && F.cart.message == "") {
                F.cart.message = "Some or more items are no more available. Please remove them before checkout.";
                F.cart.messageType = "cart-info"
            }

            function S() {
                var T = Snapdeal.getAjaxResourcesPath("/getTopOffers");
                $.ajax({
                    url: T,
                    type: "GET",
                    dataType: "json",
                    success: function(U) {
                        if (U != null) {
                            R(U)
                        }
                    },
                    error: function() {}
                })
            }

            function R(W) {
                var T = "";
                var U = "";
                var V = 0;
                $.each(W.topOffer, function(X, Y) {
                    if (Y.offerHeading != "") {
                        V++
                    }
                });
                if (W != undefined && W != null && W != "" && W.topOffer.length != 0 && V > 0) {
                    U += '<div class="heading-top-offers-empty-cart">Today\'s Top Offers</div>';
                    U += "<div class='top-offers-widget-empty-cart'>";
                    $.each(W.topOffer, function(X, Y) {
                        if (Y.offerLink != "" && Y.offerHeading != "" && Y.imageIcon != "" && Y.offerText != "") {
                            U = U + '<div class="top-offer-wrap somn-track" hidOmnTrack="TopOffer_' + (X + 1) + '">';
                            if (Y.offerLink != "null" && Y.offerLink != "") {
                                U += '<a href="' + Y.offerLink + '">'
                            }
                            U += '<div class="lfloat">';
                            U += '<img src="' + Y.imageIcon + '" alt="' + Y.offerHeading + '" height="43" width="37">';
                            U += "</div>";
                            U += '<div class="top-offer-content lfloat">' + Y.offerHeading;
                            U += "<span>" + Y.offerText + "</span>";
                            U += "</div>";
                            if (Y.offerLink != "null" && Y.offerLink != "") {
                                U += "</a>"
                            }
                            U += "</div>"
                        }
                    });
                    U += "</div>";
                    U += "</div>"
                }
                $("#cartTopOffer").html(U)
            }
            $(function() {
                S()
            });
            if (J.length == 0) {
                L = '<div class="cart-cont-outer qv_pane_outer"><div class="cart-scroll qv_pane">';
                L += '	<div class="cart-cont" style="padding-bottom:9px">';
                L += '	<div class="cart-skip closeWrapperNew" onClick="return Snapdeal.CloseCart(eventCartCrossed);"></div>';
                L += ' <div class="mycart-outer">';
                L += '	<div class="lfloat"><span class="fill-cart">0</span><span class="cart-text">My Cart</span></div>';
                L += ' 	<div class="lfloat" style="width:708px;"><span class="heading-cart">Your Shopping Cart is empty!</span></div>';
                L += " </div>";
                L += '		<div class="empty-shopping-cart">';
                L += '			<div class="categories-text">Browse Categories</div>';
                L += '			<div class="productCategoriesOuter overhid"></div>';
                L += '<div class="today-offers-cart overhid" id="cartTopOffer"></div>';
                if (M == false) {
                    L += '<div class="start-shoping-button-outer proceed-button" onclick="allProducts(\'' + Snapdeal.getStaticPath("/") + "')\"><span>Start Shopping Now</span></div>"
                } else {
                    L += '<div class="start-shoping-button-outer proceed-button" onClick="return Snapdeal.CloseCart();"><span>Start Shopping Now</span></div>'
                }
                L += "		</div>";
                L += "	</div>";
                L += "</div></div>"
            } else {
                if (F.cart.messageType == "") {
                    E = false
                }
                var Q = "";
                var P = "",
                    O = "",
                    H = "";
                var I = "";
                for (var K = 0; K < J.length; K++) {
                    P += A.cartItemHtml(J[K], M, K);
                    O += A.createOneDayTooltip(K, J[K])
                }
                L = '<div class="cart-cont-outer qv_pane_outer">';
                L += '<div class="cart-scroll qv_pane"><div class="cart-cont">';
                L += '		<div class="cart-skip closeWrapperNew" onClick="return Snapdeal.CloseCart(eventCartCrossed);"></div>';
                L += '<div class="mycart-outer">';
                L += '	<div class="lfloat"><span class="fill-cart">' + J.length + '</span><span class="cart-text">My Cart</span></div>';
                L += '<div class="lfloat">';
                if (E || F.cart.containsFaultyItem) {
                    L += Q;
                    L += '<div id="cart-message"><div class="cart-msj ' + F.cart.messageType + '">';
                    L += Q;
                    L += '<span class="cart_success_msj_heading">' + F.cart.message + "</span>You have now " + J.length + " Product(s) in your cart.";
                    L += Q;
                    L += "</div></div>"
                }
                L += "</div>";
                L += "</div>";
                L += Q;
                L += '		<div class="cart-item-outer cart-item-wbor">';
                L += '			<div class="cart-item-bg">';
                L += '				<div class="cart-item-heading-bg">';
                L += '					<div class="cart-items">Product(s)</div>';
                L += '					<div class="cart-description1">Description</div>';
                L += '					<div class="cart-quantity">Quantity</div>';
                L += '					<div class="cart-price" style="color: #565656;font-size: 12px;">Unit Price</div>';
                L += '					<div class="cart-delivery-info" style="color: #565656;font-size: 12px;">Delivery Info</div>';
                L += '					<div class="cart-total1" style="font-size:12px; color:#565656;">Sub Total</div><div class="cart-remove"></div>';
                L += "			</div>";
                L += O;
                L += '<div class="buyback-outer-tooltip"><div class="total-discout-show"><div class="buyback-arrow-top"></div><div class="buyback-arrow-top buyback-arrow-top-white"></div><div class="buyback-content">The quoted value is based on the condition of the gadget mentioned by you. Please refer to T&C for detailed information.</div></div></div>';
                L += '			<div class="cart-item-cont-bg">' + P + "</div>";
                L += "		</div>";
                L += "	</div>";
                L += "</div>";
                L += '<div class="cart-total-outer"><div class="cart-total-summary"><span class="promo-text">(You can use your Promo Codes and SD Cash during payment)</span><span class="cart-text">' + J.length + " product(s) in your cart</span></div>";
                L += '<div class="cart-total-payment-text" align="right" style="width:497px;">';
                if (D < F.cart.productPriceThresholdForShippingCharge && D > 0) {
                    G = F.cart.productPriceThresholdForShippingCharge - D;
                    L += '		<div class="add-cart-ship" style="display:none;">';
                    L += '			<div class="add-cart-ship-arrow-right"></div>';
                    L += "			<div>Add a product of <strong>Rs " + G + "</strong> more</div>";
                    L += '			<div class="add-free-ship">&amp; Get Free Shipping</div>';
                    L += "		</div>"
                }
                if (D != 0) {
                    C += B;
                    if (B > 0) {
                        L += '<div class="shipping-charges fnt12"> Delivery Charges: <span class="total-text-shipping">Rs ' + B + "</span></div>"
                    } else {
                        L += '<div class="shipping-charges fnt12">Delivery Charges: <span class="total-text free-text"> Free</span></div>'
                    }
                }
                L += '<div class="payable-amount-cart">Payable Amount: <span class="total-text">Rs ' + C + "</span></div></div></div>";
                L += '<div id="checkout-cart" class="cart-payment-outer">';
                if (window.isAuthenticated) {
                    L += '<div><div class="proceed-button"><span><a href="' + Snapdeal.getAbsoluteStaticPath("/checkout?cartId=" + F.cart.cartId) + '">Proceed to Payment</a></span></div>'
                } else {
                    L += '<div class="payment-button-outer"><div class="proceed-button"><span><a href="' + Snapdeal.getAbsoluteStaticPath("/checkout?cartId=" + F.cart.cartId) + '">Proceed to Payment</a></span></div>'
                } if (window.isAuthenticated) {
                    L += '<div class="proceed-button-quickpay sdTitleTip" sdTitle="Click to buy all items in one step"><a href="' + Snapdeal.getAbsoluteStaticPath("/quickCheckout?cartId=" + F.cart.cartId) + '"><span></span></a></div>'
                }
                if (M == false) {
                    L += '<div class="continue-button" onclick="allProducts(\'' + Snapdeal.getStaticPath("/") + "')\"><span>Continue Shopping</span></div>"
                } else {
                    if (window.isAuthenticated) {
                        L += '<div class="continue-button" style="float: left" onclick="return Snapdeal.CloseCart(eventContinueShopping);"><span>Continue Shopping</span></div>'
                    } else {
                        L += '<div class="continue-button" onclick="return Snapdeal.CloseCart(eventContinueShopping);"><span>Continue Shopping</span></div>'
                    }
                }
                L += '</div><div class="clear"></div></div>';
                L += '<div id ="cross-selling-carousel"></div>'
            }
            return L
        } else {
            return resolveCartHtml()
        }
    };
    resolveCartHtml = function() {
        var P = '<div class="cart-cont-outer qv_pane_outer"><div class="cart-scroll qv_pane"><div class="cart-cont">';
        P += '<div class="cart-skip closeWrapperNew" onClick="return Snapdeal.CloseCart(eventCartCrossed);"></div>';
        P += '<div class="mycart-outer">';
        P += '	<div class="lfloat"><span class="fill-cart"></span><span class="cart-text">My Cart</span></div>';
        P += '	<div class="lfloat" id="cart-message" style="margin-left:10px;"><div class="cart-msj cart-merge"><span class="cart_success_msj_heading">You have 2 carts on this machine. Do you want to merge the carts?</span>You can delete or update item(s) in the merged cart.</div></div>';
        P += "</div>";
        P += '<div class="cart-merge-cont-outer">';
        P += '<div class="cart-item-outer cart-merge-cont-left"><div class="cart-item-bg">';
        var J = Snapdeal.Utils.fixArray(F.cart.cartItems);
        J = Snapdeal.Utils.filterUndef(J);
        var T = 0,
            W = 0;
        P += '<div class="cart-item-heading-bg cart-item-cont-merge" style="border-bottom:2px solid #ccc;"><div class="cart-description cart-merge-des">Product(s)</div><div class="cart-price cart-price-merge">Unit Price</div><div class="cart-total cart-merge-price">Sub Total</div></div>';
        for (var Z = 0; Z < J.length; Z++) {
            var Y = J[Z].catalogDTO;
            var a = "";
            var H = Snapdeal.Utils.fixArray(Y.freebie);
            var U = 0;
            if (J[Z].subCartItems != undefined) {
                U = A.getPromoCodeValue(Snapdeal.Utils.fixArray(J[Z].subCartItems))
            }
            if (Y.freebie != undefined) {
                var V = H.length;
                if (V == undefined) {
                    V = 1;
                    H[0] = H
                }
                a = "<div class='catalog-freebie'>";
                a += '<span style="color:#597713;">Freebie: </span>';
                for (var O = 0; O < V; O++) {
                    if (O != V - 1) {
                        a += H[O] + " + "
                    } else {
                        a += H[O]
                    }
                }
                a += "</div>"
            }
            var b = "";
            var K = Snapdeal.Utils.fixArray(J[Z].catalogDTO.attributeDTOs);
            if (typeof(J[Z].catalogDTO.attributeDTOs) != "undefined" && K.length > 0) {
                var b = '<div class="cart_attr" style="line-height:18px;font-size:13px; font-family: Calibri,Regular; color:#999999;">';
                for (var N = 0; N < K.length; N++) {
                    b += K[N].name + ": " + K[N].value + " &nbsp; | &nbsp; "
                }
                if (b.length > 0) {
                    b = b.substring(0, b.lastIndexOf("|"));
                    b += "</div>"
                }
            }
            var R = J[Z].catalogDTO.sellingPrice * J[Z].quantity;
            var I = 0;
            if (J[Z].subCartItems != undefined) {
                I = A.getPromoCodeValue(Snapdeal.Utils.fixArray(J[Z].subCartItems))
            }
            var Q = R - I - (J[Z].internalCashback + J[Z].externalCashback) * J[Z].quantity;
            if (J[Z].shippingCharges > 0) {
                Q += J[Z].shippingCharges * J[Z].quantity
            }
            T += Q;
            if (J[Z].catalogDTO.catalogType == "product") {
                D += R - (J[Z].internalCashback + J[Z].externalCashback) * J[Z].quantity
            }
            var M = "";
            P += '<div class="cart-item-cont cart-item-cont-merge" style="border-right:1px solid #ccc; border-left:1px solid #ccc; color:#2e2e2e;"><div class="cart-description cart-merge-des"><div class="product-title-merge-cart">' + J[Z].catalogDTO.tagline + "</div>" + b + a + M + A.getPromoCodeHtml(Snapdeal.Utils.fixArray(J[Z].subCartItems)) + '</div> <div class="cart-unit-price cart-price-merge">Rs ' + J[Z].catalogDTO.sellingPrice + '</div> <div class="cart-total cart-merge-price" style="font-size:14px;"> <strong>Rs ' + Q + "</strong>";
            if (J[Z].shippingCharges > 0) {
                W += J[Z].shippingCharges * J[Z].quantity;
                P += '<div class="fnt10" style="line-height:12px;"> Delivery Charge: (+) Rs ' + J[Z].shippingCharges * J[Z].quantity + "</div>"
            }
            if (U > 0) {
                P += '<div class="fnt10" style="line-height:12px;"> Promo Applied: (-) Rs ' + U + "</div>"
            }
            P += A.getCashbackHtmlForCartitemMerge(J[Z]) + "</div> </div>"
        }
        P += "</div></div>";
        var L = Snapdeal.Utils.fixArray(F.cart.unresolvedCart.cartItems);
        var X = 0,
            G = 0;
        P += '<div class="cart-item-outer cart-merge-cont-right"><div class="cart-item-bg">';
        P += '<div class="cart-item-heading-bg cart-item-cont-merge" style="border-bottom:2px solid #ccc;"><div class="cart-description cart-merge-des">Product(s)</div><div class="cart-unit-price-hd cart-price-merge">Unit Price</div><div class="cart-total cart-merge-price">Sub Total</div></div>';
        for (var Z = 0; Z < L.length; Z++) {
            var Y = L[Z].catalogDTO;
            var a = "";
            var H = Snapdeal.Utils.fixArray(Y.freebie);
            var U = 0;
            if (L[Z].subCartItems != undefined) {
                U = A.getPromoCodeValue(Snapdeal.Utils.fixArray(L[Z].subCartItems))
            }
            if (Y.freebie != undefined) {
                var V = H.length;
                if (V == undefined) {
                    V = 1;
                    H[0] = H
                }
                a = "<div class='catalog-freebie'>";
                a += "Freebie: ";
                for (var O = 0; O < V; O++) {
                    if (O != V - 1) {
                        a += H[O] + " + "
                    } else {
                        a += H[O]
                    }
                }
                a += "</div>"
            }
            var b = "";
            var K = Snapdeal.Utils.fixArray(L[Z].catalogDTO.attributeDTOs);
            if (typeof(L[Z].catalogDTO.attributeDTOs) != "undefined" && K.length > 0) {
                var b = '<div class="cart_attr" style="line-height:18px; font-size:11px; color:#999999;">';
                for (var N = 0; N < K.length; N++) {
                    b += K[N].name + ": " + K[N].value + " &nbsp; | &nbsp; "
                }
                if (b.length > 0) {
                    b = b.substring(0, b.lastIndexOf("|"));
                    b += "</div>"
                }
            }
            var R = L[Z].catalogDTO.sellingPrice * L[Z].quantity;
            var I = 0;
            if (L[Z].subCartItems != undefined) {
                I = A.getPromoCodeValue(Snapdeal.Utils.fixArray(L[Z].subCartItems))
            }
            var Q = R - I - (L[Z].internalCashback + L[Z].externalCashback) * L[Z].quantity;
            if (L[Z].shippingCharges > 0) {
                Q += L[Z].shippingCharges * L[Z].quantity
            }
            X += Q;
            if (L[Z].catalogDTO.catalogType == "product") {
                D += R - (L[Z].internalCashback + L[Z].externalCashback) * L[Z].quantity
            }
            var S = "";
            if (L[Z].vendorName != undefined && false) {
                S += '<div><span style="font-weight:bold;">Seller</span>: ' + L[Z].vendorName + "</div>"
            }
            P += '<div class="cart-item-cont cart-item-cont-merge" style="border-left:1px solid #ccc; border-right:1px solid #ccc; color:#2e2e2e;"><div class="cart-description cart-merge-des"><div class="product-title-merge-cart">' + L[Z].catalogDTO.tagline + "</div>" + b + a + S + A.getPromoCodeHtml(Snapdeal.Utils.fixArray(L[Z].subCartItems)) + '</div> <div class="cart-unit-price cart-price-merge">Rs ' + L[Z].catalogDTO.sellingPrice + '</div> <div class="cart-total cart-merge-price" style="font-size:14px;"> <strong>Rs ' + Q + "</strong>";
            if (L[Z].shippingCharges > 0) {
                G += L[Z].shippingCharges * L[Z].quantity;
                P += '<div class="fnt10" style="line-height:12px;"> Delivery Charge: (+) Rs ' + L[Z].shippingCharges * L[Z].quantity + "</div>"
            }
            if (U > 0) {
                P += '<div class="fnt10" style="line-height:12px;"> Promo Applied: (-) ' + U + "</div>"
            }
            P += A.getCashbackHtmlForCartitemMerge(L[Z]) + "</div> </div>"
        }
        P += "</div></div>";
        P += "</div></div>";
        P += '<div class="cart-total-outer cart-total-merge"><div class="cart-total-summary" style="width:382px;"><div class="lfloat" style="padding:2px 0 0 8px; font-size:12px;"><strong>Cart 1</strong> (<span class="cart-text">' + J.length + ' Item(s)</span>)</div> <div class="rfloat cart-total-merge-summary">Payable Amount : <strong>Rs ' + T + "</strong></div></div>";
        P += '<div class="cart-total-payment-text cart-merge-total-payment-text" style="width:402px;"><div class="lfloat" style="padding-top:2px;"><strong>Cart 2</strong> (<span class="cart-text">' + L.length + ' Item(s)</span>)</div> <div class="rfloat cart-total-merge-summary" style="padding-right:30px;">Payable Amount : <strong>Rs ' + X + "</strong></div></div></div>";
        P += '<div class="cart-payment-outer overhid" style="padding-top:12px;"><div class="payment-button-outer proceed-button-merge-outer"><div class="proceed-merge-button" onClick="return Snapdeal.MergeCart()"><span>Merge carts</span></div><div class="cart-button-merge-sepr">OR</div><div class="proceed-merge-button" onClick="return Snapdeal.SetPreferredCart(\'' + F.cart.cartId + '\');"><span>Proceed with cart 1</span></div><div class="cart-button-merge-sepr">OR</div><div class="proceed-merge-button" onClick="return Snapdeal.SetPreferredCart(\'' + F.cart.unresolvedCart.cartId + "');\"><span>Proceed with cart 2</span></div></div></div></div>";
        return P
    }
};
Snapdeal.CartService = function() {
    var D = this,
        F = new Snapdeal.Cart(),
        A = '<div class="ajax-loader-outer">Loading...</div>';
    var E = true;
    this.init = function() {};
    this.showCart = function(G) {
        if (F.getCartJson() === undefined) {
            this.getCartDetails(G)
        } else {
            if (buyButtonTransitionFlag == "true" && cartFlag == true) {
                $("#cart-layout-div").html(F.toHtml(G)).hide()
            } else {
                $("#cart-layout-div").html(F.toHtml(G)).show()
            }
            $("#cartItemQtyId").textContent = D.cartQty();
            var H = F.getCartJson();
            if (H.cart.cartItems === undefined) {
                cartItems = []
            } else {
                cartItems = Snapdeal.Utils.fixArray(H.cart.cartItems);
                cartItems = Snapdeal.Utils.filterUndef(cartItems);
                if ($("#crossSellingEnabled").val() == "true") {
                    getCrossSellingProducts(cartItems)
                }
            } if ($("#cart-layout-div").find(".cart-cont-outer.qv_pane_outer").length > 0 && E == false) {
                $(".cart-cont-outer.qv_pane_outer").addClass("activeTransModal")
            }
            $(".oneday-delivery-help").hover(function() {
                var I = $(this).offset().top - 35 - $(window).scrollTop();
                var J = $(this).attr("data-index");
                $(".oneday-outer-tooltip[data-index=" + J + "]").css("top", I).show()
            }, function() {
                $(".oneday-outer-tooltip").hide()
            });
            $(".cart-buyback-help").hover(function() {
                var K = $(this);
                var L = $(".buyback-outer-tooltip");
                var J = parseInt(L.css("left"), 10);
                var I = K.offset().top - 35 - $(window).scrollTop();
                var M = J + K.offset().left - K.parent().find("strong").offset().left;
                L.css("top", I).css("left", M).show()
            }, function() {
                $(".buyback-outer-tooltip").css("left", "116px").hide()
            });
            $(".cart-free-shipping-tooltip").mouseover(function() {
                $(".add-cart-ship").show()
            }).mouseout(function() {
                $(".add-cart-ship").hide()
            });
            $(".discounted-price").mouseover(function(I) {
                $(this).parent().next().show()
            }).mouseout(function(I) {
                $(this).parent().next().hide()
            });
            $(".product-title-cart").text().substr(0, 160) + "...";
            if (D.cartQty() > 0) {
                $("#cartItemQtyId").removeClass("empty-cart")
            } else {
                $("#cartItemQtyId").addClass("empty-cart");
                productCategories()
            }
        }
        setTimeout(function() {
            if (E) {
                if (buyButtonTransitionFlag == "true" && cartFlag == true) {
                    $("#cart-layout-div.activeTransModalOuter").css("display", "none");
                    $(".cart-cont-outer.qv_pane_outer").css("display", "none");
                    cartFlag = false
                } else {
                    $("#cart-layout-div.activeTransModalOuter").css("display", "block")
                }
                $(".cart-cont-outer.qv_pane_outer").addClass("activeTransModal")
            } else {
                $(".cart-scroll.qv_pane").css({
                    visibility: "visible",
                    transform: "inherit",
                    opacity: "inherit",
                    transition: "inherit"
                });
                E = true
            }
        })
    };
    this.freezeScreen = function() {
        $("#cart-layout-div").html(A).show()
    };

    function C(G) {
        if (G) {
            $("#cart-layout-div").css("display", "block").append('<div class="buyLoader">LOADING</div>')
        } else {
            $("#cart-layout-div").css("display", "none").children().remove()
        }
    }
    this.addToCart = function(J, K, G) {
        var R = $("#defaultSupc").html();
        var O = window.sessionStorage.getItem("buyBack");
        if (O != null) {
            var M = O.split("|")
        }
        var P = "";
        var Q = "";
        var I = "";
        if (M) {
            for (var L = 0; L < M.length; L++) {
                var H = M[L].split("~");
                if (O != null) {
                    if (H[0] === R) {
                        P = (H[1].split("^"))[0];
                        Q = (H[1].split("^"))[1];
                        I = (H[1].split("^"))[2]
                    }
                }
            }
        }
        var N = "/json/cart/operation/add?catalogId=" + J + "&qty=1&vendorCode=" + K + "&supc=" + G + "&buyBackQuoteValue=" + P + "&buyBackQuoteId=" + encodeURIComponent(Q) + "&buyBackQuoteName=" + I;
        return $.ajax({
            url: N,
            type: "GET",
            dataType: "json",
            cache: false,
            beforeSend: function() {},
            success: function(S) {
                C(false);
                $(".no-thanks-Combo").hide();
                D.processCart(S);
                if (window.goWithoutCombo == true) {
                    D.getCartWithSlide()
                }
            }
        })
    };
    this.addProductsToCart = function(G) {
        var H = "/operation/addProductsToCart";
        $.ajax({
            url: H,
            type: "POST",
            dataType: "json",
            data: JSON.stringify(G),
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            cache: false,
            beforeSend: function() {},
            success: function(J) {
                var I = {
                    cart: J
                };
                D.processCart(I);
                D.getCartWithSlide()
            }
        })
    };
    this.getCartWithSlide = function() {
        $("#frequently-bought-div").parent().removeClass("pageOverlay");
        $("#frequently-bought-div").removeClass("buyflow-combo-popup");
        if (window.combo && window.combo.closeAttrBox) {
            window.combo.closeAttrBox()
        }
        return false
    };
    this.addCrossSellingToCart = function(N, H, G) {
        var J = new Date();
        var M = J.getTime();
        M += 1200 * 1000;
        J.setTime(M);
        var I = B("Cross_Selling_Product");
        window.cartService.fireOmnitureScript("cartCrossSellNew", N);
        document.cookie = "Cross_Selling_Product=" + escape(I + "," + N) + "; path =/ ; expires=" + J.toGMTString();
        var K = "/json/cart/operation/add";
        var L = {
            catalogId: N,
            vendorCode: H,
            supc: G,
            qty: 1
        };
        $.ajax({
            url: K,
            type: "GET",
            data: L,
            dataType: "json",
            cache: false,
            success: function(O) {
                D.processCart(O)
            }
        });
        window.cartService.fireOmnitureScript("addToCart", N, true)
    };

    function B(H) {
        var G = "";
        var J = 0;
        var I = document.cookie.split(";");
        for (i = 0; i < I.length; i++) {
            x = I[i].substr(0, I[i].indexOf("="));
            y = I[i].substr(I[i].indexOf("=") + 1);
            x = x.replace(/^\s+|\s+$/g, "");
            if (x == H) {
                G = unescape(y)
            }
        }
        return G
    }
    this.fireOmnitureScript = function(G, I, H, L, J, K, M, O, N) {
        N = N || "normal";
        var P = getOmnitureScript({
            catalogId: I,
            prebook: L,
            live: H,
            user: getCookieByName("uid"),
            vendorCode: J,
            vendorScore: K,
            buyBtnOrder: M,
            shippingCharges: O,
            buyType: N
        }, G);
        P.complete(function() {
            $(".ajax-loader-icon").hide()
        });
        this.fireLogToServer(G, I, H)
    };
    this.processCart = function(I, G) {
        F.reloadCart(I);
        var H = F.cartQty();
        Snapdeal.Cookie.set("cartQty", H, 1000);
        $("#cartItemQtyId").text(H);
        this.showCart(G);
        setTimeout("cartMsgFade()", 7000)
    };
    this.cartQty = function() {
        var G = Snapdeal.Cookie.get("cartQty");
        if (G) {
            return G
        }
        return 0
    };
    this.getCartDetails = function(G) {
        this.freezeScreen();
        var H = "/json/cart/operation/getInfo?mergeCart=" + G;
        $.ajax({
            url: H,
            type: "GET",
            dataType: "json",
            cache: false,
            success: function(I) {
                D.processCart(I, G)
            }
        })
    };
    this.removeCartItem = function(M, J, G, I) {
        var H = Snapdeal.Cookie.get("spin"),
            L = "";
        if (H) {
            L = H.substr(0, 6)
        }
        var K = "/json/cart/operation/remove?catalogId=" + M + "&vendorCode=" + G + "&supc=" + I + "&pincode=" + L;
        $.ajax({
            url: K,
            type: "GET",
            dataType: "json",
            cache: false,
            success: function(N) {
                E = false;
                D.processCart(N, J)
            }
        })
    };
    this.updateCartItem = function(L, I, Q, N, K, H, J, G) {
        var O = Snapdeal.Cookie.get("spin"),
            P = "";
        if (O) {
            P = O.substr(0, 6)
        }
        if (G == "blur" && (parseInt(Q) > J || isNaN(Q) || Q == 0)) {
            $(L).val(J).focus();
            Q = J
        }
        if (G == "arrow" && (Q < 1 || Q > J)) {
            return
        }
        var M = "/json/cart/operation/update?catalogId=" + I + "&qty=" + Q + "&vendorCode=" + K + "&supc=" + H + "&pincode=" + P;
        $.ajax({
            url: M,
            type: "GET",
            dataType: "json",
            cache: false,
            beforeSend: function() {
                $(".cartOverlaySmall").css("display", "block")
            },
            success: function(R) {
                E = false;
                D.processCart(R, N);
                $(".cartOverlaySmall").css("display", "none")
            }
        })
    };
    this.hideCartMessage = function() {
        F.hideCartMessage()
    };
    this.mergeCart = function() {
        var G = "/json/cart/operation/merge";
        $.ajax({
            url: G,
            type: "GET",
            dataType: "json",
            cache: false,
            success: function(H) {
                D.processCart(H)
            }
        })
    };
    this.setPreferredCart = function(H) {
        var G = "/json/cart/operation/setpreference?prefCartID=" + H;
        $.ajax({
            url: G,
            type: "GET",
            dataType: "json",
            success: function(I) {
                D.processCart(I)
            }
        })
    };
    this.fireLogToServer = function(H, J, I) {
        var G = $("#logServiceUrl").val();
        if (G != "" && G != undefined) {
            $.ajax({
                url: G + "/lg/ac",
                dataType: "jsonp",
                cache: false,
                data: {
                    u: Snapdeal.Cookie.get("u"),
                    et: H,
                    c: J
                }
            })
        }
    }
};
Snapdeal.CloseCart = function closeCart(C) {
    $(".cart-scroll.qv_pane").css({
        visibility: "",
        transform: "",
        opacity: "",
        transition: ""
    });
    window.cartService.hideCartMessage();
    $(".cart-cont-outer.qv_pane_outer").removeClass("activeTransModal");

    function A() {
        $("#cart-layout-div").html("").hide();
        $("#cart-layout-div.activeTransModalOuter").fadeOut(300)
    }
    if (detectCSSFeature("transition")) {
        $(".cart-cont-outer.qv_pane_outer").one("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd", function() {
            A()
        })
    } else {
        A()
    }
    $("#beforecartmsgg, .icon-close").hide();
    goWithoutCombo = false;
    $(".dicount-show-div .prodbuy-button").addClass("proceed-btn");
    if ($(".no-item-added").is(":visible")) {
        $(".dicount-show-div .prodbuy-button").addClass("grray-out-combo").removeClass("proceed-btn")
    }
    $(".more-similar-links-cmb").show();
    var B = window.location.href;
    if (B.indexOf("/info/") != -1) {
        window.location.href = "/"
    }
    if (C) {
        window.cartService.fireOmnitureScript(C)
    }
    $("#frequently-bought-div").css("left", "auto").css("top", "auto").css("width", 732).css("height", 498);
    return false
};
Snapdeal.RemoveCartItem = function(G, E, D, B, C) {
    var A = '<img src="' + Snapdeal.getResourcePath("img/ajax-loader.gif") + '"/>';
    var F = "#cart-item-remove-" + G;
    $(F).html(A);
    window.cartService.removeCartItem(G, D, B, C);
    window.cartService.fireOmnitureScript("removeFromCartNew", G, E)
};
Snapdeal.UpdateCartItem = function(F, H, G, D, A, C, B, E) {
    window.cartService.updateCartItem(F, H, G, D, A, C, B, E)
};
Snapdeal.MergeCart = function() {
    window.cartService.mergeCart();
    return false
};
Snapdeal.SetPreferredCart = function(A) {
    window.cartService.setPreferredCart(A);
    return false
};

function getCookieByName(A) {
    var B = new RegExp(A + "=[^;]+", "i");
    if (document.cookie.match(B)) {
        return document.cookie.match(B)[0].split("=")[1]
    }
    return null
}

function allProducts(A) {
    window.location.href = A
}

function cartMsgFade() {
    $("#cart-message").fadeOut(1000)
}

function productCategories() {
    var C = Math.ceil($(".headerProductCategories").length / 4);
    var A = "";
    var B = "";
    A += '<div class="productCategoriesBox lfloat">';
    $(".headerProductCategories").each(function(D) {
        if ((D % C) == 0 && D != 0) {
            A += '</div><div class="productCategoriesBox lfloat">'
        }
        A += '<div class="productCatList"><a href="' + Snapdeal.getAbsoluteStaticPath("/products/" + $(this).attr("catUrl")) + '">' + $(this).attr("name") + "</a></div>"
    });
    $(".productCategoriesOuter").html(A);
    B = $(".productCategoriesOuter").height();
    $(".productCategoriesBox").each(function() {
        $(this).css("height", B)
    })
}

function getCrossSellingProducts(A) {
    Snapdeal.Utils.fixArray(A);
    var D = "";
    var E = "[";
    var C = new Array();
    for (var B = 0; B < A.length; B++) {
        if (B == 0) {
            E = E + '{"csr":{'
        } else {
            E = E + ',{"csr":{'
        } if (A[B].catalogDTO.catalogType == "product") {
            E = E + '"sp":"' + A[B].catalogDTO.sellingPrice + '",';
            E = E + '"supc":"' + A[B].supc + '"'
        }
        E = E + "}}"
    }
    E = E + "]";
    if (E != "[]") {
        var F = "/json/cart/crossSellingProducts";
        $.ajax({
            url: F,
            type: "POST",
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function(G) {
                $("#cross-selling-carousel").append(cartCarousel(G));
                $("#cart-crousel-wrapper").bxSlider({
                    auto: false,
                    pager: false,
                    infiniteLoop: false,
                    hideControlOnEnd: true
                });
                $("a.bx-prev").html("");
                $("a.bx-next").html("")
            }
        })
    }
}

function cartCarousel(E) {
    var D = Snapdeal.Utils.fixArray(E);
    if (D != 0 && D != undefined) {
        var C = D.length;
        if (C == undefined) {
            C = 1;
            D[0] = D
        }
        if (C > 0) {
            var B = "";
            B += '<div class="cart-crousel-head">Frequently bought together with products in cart</div>';
            B += '<div class="overhid" id="cart-crousel-wrapper">';
            B += '<div class="overhid" style="width:812px">';
            for (var A = 0; A < D.length; A++) {
                if ((A % 3 == 0) && A != 0) {
                    B += "</div><div>"
                }
                B += '<div class="lfloat overhid cart-crousel-box">';
                B += '<div class="crousel-img lfloat">';
                B += '<img src="' + Snapdeal.getResourcePath(D[A].crossSellingProductSRO.imageUrl) + '" width="35"  />';
                B += "</div>";
                B += '<div class="lfloat carousal-data">';
                B += '<div class="crousel-prod-title">' + D[A].crossSellingProductSRO.name + "</div>";
                B += '<div class="crousel-prod-price lfloat">';
                if (D[A].crossSellingProductSRO.voucherPrice != undefined) {
                    B += "Rs " + D[A].crossSellingProductSRO.voucherPrice
                } else {
                    B += ""
                }
                B += "</div>";
                B += '<div class = "carousel-prod-addtocart lfloat"><label class ="carousel-checkbox"><input class ="carousel-check" type="checkbox" name="Add to Cart" value="Add to Cart" onclick="window.cartService.addCrossSellingToCart(' + D[A].crossSellingProductSRO.catalogId + ", '" + D[A].crossSellingProductSRO.vendorCode + "', '" + D[A].crossSellingProductSRO.supcCode + "')\"></label>Add to cart</div>";
                B += "</div>";
                B += "</div>"
            }
            B += "</div></div>";
            return B
        }
    } else {
        var B = "";
        return B
    }
}

function showMyCart() {
    window.cartService.showCart(false);
    window.cartService.fireOmnitureScript("showCart");
    $("#checkout-cart").show();
    return false
}
$(function() {
    window.cartService = new Snapdeal.CartService();
    window.cartService.init();
    var A = window.cartService.cartQty();
    $("#cartItemQtyId").text(A);
    if (A > 0) {
        $("#cartItemQtyId").removeClass("empty-cart")
    }
    if ($("#initShowCart").html() == "true") {
        window.cartService.showCart()
    }
    $("#navCartWrapper").click(function() {
        window.cartService.showCart();
        window.cartService.fireOmnitureScript("showCart");
        return false
    })
});

function showCheckoutMyCart() {
    window.cartService.showCart(false);
    $("#delPay").hide();
    window.cartService.fireOmnitureScript("showCart");
    return false
}
$(".addToCart").live("click", function() {
    if ($(this).attr("catalog") == "undefined") {
        $("#attribute-error-message").show();
        return false
    }
    var A = false;
    if ($(this).attr("prebook") == "true") {
        A = true
    }
    $(".menucard-overlay").hide();
    window.cartService.addToCart($(this).attr("catalog"), $(this).attr("vendorCode"), $(this).attr("supc"));
    window.cartService.fireOmnitureScript("addToCart", $(this).attr("catalog"), true, A, undefined, undefined, undefined, $(this).attr("shippingCharges"));
    return false
});
var loadLeftNavInWindowLoad = false;
if (typeof isIframe == "undefined") {
    $(document).ready(function() {
        try {
            if (window.isNewLeftNav === "true") {
                $(".right-product-banners").addClass("revampLeft-right-product-banners");
                $(".somn-track.seeFullSiteLink").attr("hidomntrack", "HID=newLeftnav_SeeAllCategory");
                if (!window.localStorage || window.localStorage.getItem("newLeftNavStorage") == null) {
                    if ("hp" == window.selectedTab) {
                        newLeftNavAjaxRequest(false);
                        window.localStorage.removeItem("leftNavHtmlRevamp")
                    } else {
                        loadLeftNavInWindowLoad = true
                    }
                } else {
                    loadLeftNavInWindowLoad = true;
                    var C = window.localStorage.getItem("newLeftNavStorage");
                    $(".leftNavigationContainer").html(C);
                    autoPositionForLeftNav();
                    revampMoveWithCone();
                    previousDocReady()
                }
            } else {
                if (!window.localStorage || window.localStorage.getItem("leftNavHtmlRevamp") == null) {
                    if ("hp" == window.selectedTab) {
                        oldLeftNavAjaxRequest(false)
                    } else {
                        loadLeftNavInWindowLoad = true
                    }
                } else {
                    loadLeftNavInWindowLoad = true;
                    var C = window.localStorage.getItem("leftNavHtmlRevamp");
                    $(".leftNavigationContainer").html(C);
                    autoPositionForLeftNav();
                    moveWithCone();
                    previousDocReady()
                }
            }
        } catch (B) {
            var A = Snapdeal.Cookie.get("cityPageUrl");
            if (window.isNewLeftNav === "true") {
                $(".leftNavigationContainer").load(Snapdeal.getAjaxResourcesPath("/getRevampedLeftNav"));
                autoPositionForLeftNav();
                revampMoveWithCone();
                previousDocReady()
            } else {
                $(".leftNavigationContainer").load(Snapdeal.getAjaxResourcesPath("/getLeftNav?sT=" + selectedTab + "&aT=" + activeProductTab + "&v=2"));
                autoPositionForLeftNav();
                moveWithCone();
                previousDocReady()
            }
        }
        setEvents()
    })
}
if (typeof isIframe == "undefined") {
    $(window).load(function() {
        if (loadLeftNavInWindowLoad) {
            if (window.isNewLeftNav === "true") {
                $(".right-product-banners").addClass("revampLeft-right-product-banners");
                $(".somn-track.seeFullSiteLink").attr("hidomntrack", "HID=newLeftnav_SeeAllCategory");
                newLeftNavAjaxRequest(true)
            } else {
                oldLeftNavAjaxRequest(true)
            }
        }
    })
}

function setEvents() {
    $(".left-categories-head-down").mouseenter(function() {
        leftCategoriesHeadHover("left-categories-head-down")
    }).mouseleave(function() {
        leftCategoriesHeadOut("left-categories-head-down")
    });
    $(".left-categories-cont-inactive").mouseenter(function() {
        leftCategoriesContHover("left-categories-cont-inactive")
    }).mouseleave(function() {
        leftCategoriesContOut("left-categories-cont-inactive")
    })
}

function leftCategoriesHeadHover(A) {
    $(".left-categories-cont-inactive").show();
    $("." + A).addClass("left-categories-head-up");
    $("." + A).parent().removeClass("left-categories-head-inner")
}

function leftCategoriesHeadOut(A) {
    $(".left-categories-cont-inactive").hide();
    $("." + A).removeClass("left-categories-head-up");
    $("." + A).parent().addClass("left-categories-head-inner")
}

function leftCategoriesContHover(A) {
    $("." + A).show();
    $(".left-categories-head-down").addClass("left-categories-head-up");
    $(".left-categories-head-down").parent().removeClass("left-categories-head-inner")
}

function leftCategoriesContOut(A) {
    $("." + A).hide();
    $(".left-categories-head-down").removeClass("left-categories-head-up");
    $(".left-categories-head-down").parent().addClass("left-categories-head-inner")
}
var loadLeftNavInWindowLoad = false;

function autoPositionForLeftNav() {
    $(".navlink").each(function(B, A) {
        if (B > 0) {
            $(this).mouseover(function(D) {
                if ($.browser.version == "8.0" && $.browser.msie == true) {
                    var E = 0;
                    if ($("#leftNavNemu").hasClass("left-categories-cont-inactive")) {
                        var E = -200
                    }
                    var C = E - $(this).position().top + ($(window).height() - parseInt($(this).children(".subnavCont").height())) - $(".navlink").eq(1).position().top + 40 + $("html").scrollTop();
                    if (C > 0) {
                        C = -1
                    }
                    if (parseInt($(this).children(".subnavCont").height()) > ($(window).height() - $(this).position().top)) {
                        $(this).children(".subnavCont").attr("topPos", C);
                        $(this).children(".subnavCont").css("top", C)
                    }
                    if ($("#leftNavNemu").hasClass("left-categories-cont-inactive")) {
                        $(this).children(".subnavCont").css("top", C);
                        $(this).children(".subnavCont").attr("topPos", C)
                    }
                } else {
                    if (window.pageYOffset < 145) {
                        var C = (0 + $(this).position().top - $(".navlink").eq(0).position().top) * (-1);
                        if ($("#leftNavNemu").hasClass("left-categories-cont-inactive")) {
                            C += 1
                        }
                        $(this).children(".subnavCont").css("top", C)
                    } else {
                        var C = (0 + $(this).position().top - window.pageYOffset) * (-1);
                        if ($("#leftNavNemu").hasClass("left-categories-cont-inactive")) {
                            C += -99
                        }
                        $(this).children(".subnavCont").css("top", C)
                    }
                }
            })
        }
    });
    if ($(".wrapper-z > a > .brandimgSize").size() > 0) {
        $(".wrapper-z > a > .brandimgSize").mouseover(function() {
            $(this).attr("src", Snapdeal.getResourcePath($(this).attr("imgSrcCL")))
        }).mouseout(function() {
            $(this).attr("src", Snapdeal.getResourcePath($(this).attr("imgSrcBW")))
        })
    }
    if ($(".productSliderLeftNav").size() > 0) {
        if (!($.browser.version == 8 && $.browser.msie == true)) {
            $(".left-categories-cont-inactive").show();
            $(".subnavCont").show()
        }
        $(".productSliderLeftNav").each(function(B, A) {
            if ($(this).find(".leftNavProdWrapper").size() > 0) {
                $("#" + $(this).attr("id")).bxSlider({
                    pager: false,
                    auto: false,
                    easing: "swing",
                    prevText: "",
                    nextText: "",
                    infiniteLoop: true,
                    hideControlOnEnd: true
                })
            }
        });
        if (!($.browser.version == 8 && $.browser.msie == true)) {
            $(".subnavCont").hide();
            $(".left-categories-cont-inactive").hide()
        }
        $(".productSliderLeftNav > .pager").css("width", "auto")
    }
}

function moveWithCone() {
    $("ul.nav").menuAim({
        activate: function(B) {
            var A = $(B).index();
            $(B).children(".subnavCont").show()
        },
        deactivate: function(B) {
            var A = $(B).index();
            $(B).children(".subnavCont").hide()
        },
        exitMenu: function(A) {
            return true
        }
    });
    $(".subnavCont").each(function() {
        $(this).mouseover();
        $(this).mouseleave()
    })
}

function openFlow(A) {
    $(A).find(".submenu1").find(".mainContentSubmenu").hide();
    $(A).children(".mainContentSubmenu").hide();
    $(A).children(".subMenuContainer").children("ul").find(".submenu1li.navlink").find(".semiRightArrow1").removeClass("firstLiArrow");
    $(A).children(".subMenuContainer").children("ul").find(".submenu1li.navlink").children("a").removeClass("addClassHoverLi");
    $(A).children(".subMenuContainer").children("ul").find(".submenu1li.navlink").eq(0).find(".semiRightArrow1").addClass("firstLiArrow");
    $(A).children(".subMenuContainer").show();
    $(A).find(".mainContentSubmenu").eq(0).show();
    $(A).find(".mainContentSubmenu").eq(0).parent().find("a").eq(0).addClass("addClassHoverLi");
    $(A).find(".mainContentSubmenu").eq(0).find(".mainContentSubmenu1").show()
}

function revampMoveWithCone() {
    $("ul.nav").menuAim({
        activate: function(F) {
            var C = $(F).index();
            if (window.openFlowFlag == "true") {
                openFlow(F)
            }
            $(F).children(".subMenuContainer").show();
            if ($(F).index() > 5) {
                $($(F).children().eq(1).children()).removeClass("submenuChild4");
                $($(F).children().eq(1).children()).addClass("submenuChild6")
            } else {
                $($(F).children().eq(1).children()).removeClass("submenuChild6");
                $($(F).children().eq(1).children()).addClass("submenuChild4")
            }
            var E = $(F).children(".subMenuContainer").children(".submenuChild6");
            if (E.height() > 180) {
                var D = -224 - (180 - (E.height())) + "px";
                $("#leftNavNemu .nav li.trial1 .subMenuContainer .submenuChild6 .mainContentSubmenu").css("top", D)
            } else {
                $("#leftNavNemu .nav li.trial1 .subMenuContainer .submenuChild6 .mainContentSubmenu").css("top", "-224px")
            }
            $(F).find("ul").eq(0).menuAim({
                activate: B,
                deactivate: A,
                exitMenu: function() {
                    return true
                }
            })
        },
        deactivate: function(D) {
            var C = $(D).index();
            $(D).children(".subMenuContainer").hide()
        },
        exitMenu: function(C) {
            return true
        }
    });
    $(".subMenuContainer").each(function() {
        $(this).mouseover();
        $(this).mouseleave()
    });

    function B(C) {
        if (window.openFlowFlag == "true") {
            $(C).parent().find("li").find(".semiRightArrow1").removeClass("firstLiArrow")
        }
        $(C).find(".mainContentSubmenu").show();
        $(C).children("a").addClass("addClassHoverLi");
        $(C).siblings().children("a").removeClass("addClassHoverLi");
        $(C).parent(".submenu1").find(".mainContentSubmenu").hide();
        $(C).find(".mainContentSubmenu").show();
        $(C).find(".mainContentSubmenu1").show()
    }

    function A(C) {
        if (window.openFlowFlag == "false") {
            $(C).find(".mainContentSubmenu").hide();
            $(C).children("a").removeClass("addClassHoverLi");
            $(C).siblings().children("a").removeClass("addClassHoverLi");
            $(C).parent(".submenu1").find(".mainContentSubmenu").hide()
        }
        if (window.openFlowFlag == "true") {
            $(C).parent().find("li").eq(0).find(".semiRightArrow1").removeClass("firstLiArrow");
            $(C).parent().children("li").children("a.addClassHoverLi").find(".semiRightArrow1").addClass("firstLiArrow");
            if ($(C).parent().children("li").children("a.addClassHoverLi").hasClass("addClassHoverLi")) {}
        }
    }
}
var alreadyInitialized = false;
var selectedTab = window.selectedTab,
    activeProductTab = window.activeProductTab;

function previousDocReady() {
    if (alreadyInitialized) {
        if (!isPersonalizedHP) {}
        return false
    }
    alreadyInitialized = true;
    page = new Snapdeal.HomePage();
    page.init();
    if (!(window.location.href.indexOf("email=") == -1)) {
        var A = window.location.href.split("?")[1].split("&");
        $.each(A, function(D, B) {
            if (B.indexOf("email=") != -1) {
                var C = (B.split("=")[1])
            }
        })
    }
}

function SecondMenuData(T, Y) {
    var R = "",
        K = "";
    var N = "",
        W = "";
    var H;
    if (T.secondMenuUrl != "") {
        H = T.secondMenuUrl
    } else {
        H = "javascript:void(0);"
    } if (T.secondMenuUrl == undefined) {
        H = "javascript:void(0);"
    }
    if (T.TAGName) {
        R = "<span class='red-offers'>" + T.TAGName + "</span>";
        N = "<a href='" + H + "' class='L2AncorMenu'><span class='sub-menu-text'>" + T.Text + "</span><span class='semiRightArrow1 floatRight'></span>" + R + "</a>";
        W = $("<li class='submenu1li navlink'>" + N + "</li>")
    } else {
        var I = "<a  href='" + H + "' class='L2AncorMenu '><span class='sub-menu-text'>" + T.Text + "</span><span class='semiRightArrow1 floatRight'></span></a>";
        W = $("<li class='submenu1li navlink'>" + I + "</li>")
    }
    var E = "",
        X = "";
    for (var g = 0; g < T.thirdLevelLeftData.length; g++) {
        var b = T.thirdLevelLeftData[g].linkOrHeadingClass;
        var S = "";
        var k = T.thirdLevelLeftData[g].logoimage;
        if (b === "leftNavHead") {
            S = "leftNavHead z-in11  "
        } else {
            S = "leftNavp marB8 z-in11 "
        } if (k) {
            var l;
            var e = "";
            if (T.thirdLevelLeftData[g].logoUrl != "") {
                l = T.thirdLevelLeftData[g].logoUrl
            } else {
                l = "javascript:void(0);"
            } if (T.thirdLevelLeftData[g].logoimage) {
                e = "lazySrc= " + k
            }
            E += "<p class='PformPortLeft logoPortLeft'><a href='" + l + "' class='logoBorder z-in11'><img Dalt='" + T.thirdLevelRightData[g].atl + "' " + e + " width='70px' height='50px' class='logoImg lazyBg'></a></p>"
        }
        if (!k && T.thirdLevelLeftData[g].showData) {
            var Z;
            if (T.thirdLevelLeftData[g].gridUrl != "") {
                Z = T.thirdLevelLeftData[g].gridUrl
            } else {
                Z = "javascript:void(0);"
            } if (T.thirdLevelLeftData[g].tagSecFlag) {
                K = "<span class='red-offers'>" + T.thirdLevelLeftData[g].TAGSecName + " </span>";
                E += "<p class='PformPortLeft'><a href='" + Z + "' class='" + S + "'>" + T.thirdLevelLeftData[g].data + "</a>" + K + "</p>"
            } else {
                E += "<p class='PformPortLeft'><a href='" + Z + "' class='" + S + "'>" + T.thirdLevelLeftData[g].data + "</a></p>"
            }
        }
    }
    for (var g = 0; g < T.thirdLevelRightData.length; g++) {
        var b = T.thirdLevelRightData[g].linkOrHeadingClass;
        var S = "";
        var k = T.thirdLevelRightData[g].logoimage;
        if (b === "leftNavHead") {
            S = "leftNavHead z-in11"
        } else {
            S = "leftNavp marB8 z-in11"
        } if (k) {
            var a;
            var e = "";
            if (T.thirdLevelRightData[g].logoUrl != "") {
                a = T.thirdLevelRightData[g].logoUrl
            } else {
                a = "javascript:void(0);"
            } if (T.thirdLevelRightData[g].logoimage) {
                e = "lazySrc= " + k
            }
            var F = "<a href><img lazySrc='" + k + "'></a>";
            X += "<p class='PformPortLeft logoPortLeft'><a class='logoBorder z-in11' href='" + a + "' ><img Dalt='" + T.thirdLevelRightData[g].atl + "' " + e + " width='70px' height='50px' class='logoImg lazyBg'></a></p>"
        }
        if (!k && T.thirdLevelRightData[g].showData) {
            var D;
            if (T.thirdLevelRightData[g].gridUrl != "") {
                D = T.thirdLevelRightData[g].gridUrl
            } else {
                D = "javascript:void(0);"
            } if (T.thirdLevelRightData[g].tagSecFlag) {
                var B = "<span class='red-offers'>" + T.thirdLevelRightData[g].TAGSecName + " </span>";
                X += "<p class='PformPortLeft'><a href='" + D + "' class='" + S + "'>" + T.thirdLevelRightData[g].data + "</a>" + B + "</p>"
            } else {
                X += "<p class='PformPortLeft'><a href='" + D + "' class='" + S + "'>" + T.thirdLevelRightData[g].data + "</a></p>"
            }
        }
    }
    var V = T.background[0].img || "";
    var L = T.background[0].posX,
        J = T.background[0].posY,
        A = T.background[0].width,
        C = T.background[0].height;
    var U = "<div style='width:" + A + "px; height:" + C + "px; background-image:" + V + " ;background-position:" + L + "px " + J + "px';  Dalt='" + T.background[0].bckAtl + "' class='leftNavChildWrapper lazyBg'>";
    var m = "";
    for (var g = 0; g < T.coordinates.length; g++) {
        var d;
        if (T.coordinates[g].hrefUrl != "") {
            d = T.coordinates[g].hrefUrl
        } else {
            d = "javascript:void(0);"
        }
        m += "<area shape='rect' coords='" + T.coordinates[g].coordinate + "' href='" + d + "' target='blank'/>"
    }
    var O = "";
    var c = "";
    var M = "";
    var j = "";
    var f = "";
    if (T.children[0].children.length > 0) {
        if (T.children[0].children[0].offer) {
            O = "lazySrc= " + T.children[0].children[0].offer
        }
        if (T.children[0].children[0].secndOffer) {
            j = " ;display: none; ";
            c = "lazySrc= " + T.children[0].children[0].secndOffer
        }
        if (j.length == 0) {
            f = " ;display: none; "
        }
        var G, P;
        if (T.children[0].children[0].offerUrl != "") {
            G = T.children[0].children[0].offerUrl
        } else {
            G = "javascript:void(0);"
        } if (T.children[0].children[0].secndOfferUrl != "") {
            P = T.children[0].children[0].secndOfferUrl
        } else {
            P = "javascript:void(0);"
        }
        M += "<div class='mainContentSubmenu1'><div class='leftSubcatOuter1'><ul class='leafMenu'><li class='submenu1li level4Pos'><div class='leftNavChildWrapperOffer'><div class='offerFormFinal'><a class='txtCenter ' href='" + G + "'><img class='logoImg lazyBg' Dalt='" + T.children[0].children[0].ofrOneAlt + "' style='width: 194px; height: 385px;" + j + "'  " + O + "></a><a class='txtCenter' href='" + G + "'><img class='logoImg lazyBg' Dalt='" + T.children[0].children[0].ofrOneAlt + "' style='width:194px; height: 184px;" + f + "' " + O + "></a><a class='txtCenter ' href='" + P + "'><img class='logoImg lazyBg' Dalt='" + T.children[0].children[0].ofrTwoAlt + "' style='width:194px; height:184px; margin-top:16px;' " + c + "></a><div class='clear marB5'></div></div></div></li></ul></div></div>"
    }
    var Q = "map" + Math.floor(Math.random() * 100000);
    var h = $("<div class='mainContentSubmenu'><div class='leftSubcatOuter'  style='width:" + A + "px; height:" + C + "px;'><ul class='submenu1'><li class='submenu1li' style='padding-top:0;'>" + U + "<img lazySrc='" + Snapdeal.getResourcePath("/img/leftNav/transi.png") + "' style='position:absolute; width:558px; z-index:10;height:423px;' usemap='#" + Q + "'></img><map name='" + Q + "' id='" + Q + "' track='omn'>" + m + "</map><div class='leftNavL3Block clearfix thirdLevelFormFinal'><form class='myForm' method='get'><div class='formPortLeft' id='' style='z-index: initial;'>" + E + "</div><div class='formPortRight' id='' style='z-index: initial;'>" + X + "</div><div class='clear marB5'></div></form></div></div>" + M + "</li></ul></div></div>");
    h.appendTo(W);
    W.appendTo(Y)
}

function MainMenuData(E, D) {
    var F;
    if (E.mainMenuUrl != "") {
        F = E.mainMenuUrl
    } else {
        F = "javascript:void(0);"
    }
    var B = "<a href='" + F + "' class='leftCategoriesProduct'>";
    var A = $("<li class='navlink  trial1'>" + B + "<span class='menuWidth1'>" + E.Text + "</span><span class='semiRightArrow'></span></a></li>");
    var G = $("<div style='width:158px;z-index:0;' class='subMenuContainer'></div>");
    var C = $("<ul class='submenu1 BGF6 borderTGrey '></ul>");
    G.appendTo(A);
    $.map(E.children, function(H) {
        if (H.Text !== "Add") {
            C.appendTo(G);
            SecondMenuData(H, C)
        }
    });
    A.appendTo(D)
}

function getRevampedHTML(B) {
    if (B.jsonString.length === 0) {
        B = serverData2
    } else {
        B = JSON.parse(B.jsonString)
    }
    var A = $("<ul class='nav'></ul>");
    $.map(B, function(C) {
        if (C.Text !== "See All Categories") {
            MainMenuData(C, A)
        }
    });
    $(".leftNavigationContainer").html(A);
    return ("<ul class='nav'>" + $(A).html() + "</ul>")
}
$(document).ready(function() {
    if (window.isNewLeftNav === "true") {
        $("<div class='greyBg' style='display:none;'></div>").appendTo("#navBarWrapper");
        $(document).on("mouseenter", "ul.nav", function() {
            $(".greyBg").show();
            if (window.homePageBannerSlider && homePageBannerSlider.stopAuto !== undefined) {
                homePageBannerSlider.stopAuto()
            }
        });
        $(document).on("mouseleave", "ul.nav", function(A) {
            $(".greyBg").hide();
            if (window.homePageBannerSlider && homePageBannerSlider.startAuto !== undefined) {
                homePageBannerSlider.startAuto()
            }
        });
        $(".seeFullSiteDiv").css({
            "font-size": "14px"
        });
        $(".seeFullSiteDiv").hover(function() {
            $(this).css({
                background: "#e7e7e7"
            })
        }, function() {
            $(this).css({
                background: "#fff"
            })
        });
        $(document).on("mouseenter", ".thirdLevelFormFinal div a.leftNavp, .thirdLevelFormFinal div a.leftNavHead", function() {
            var A = $(this).attr("href");
            if (A.indexOf("void") != -1 && A.indexOf("javascript:") != -1) {
                $(this).css({
                    textDecoration: "none",
                    cursor: "default"
                })
            } else {
                $(this).css({
                    textDecoration: "underline",
                    cursor: "pointer"
                })
            }
        });
        $(document).on("mouseleave", ".thirdLevelFormFinal div a.leftNavp, .thirdLevelFormFinal div a.leftNavHead", function() {
            var A = $(this).attr("href");
            $(this).css({
                textDecoration: "none",
                cursor: "default"
            })
        })
    }
});

function newLeftNavAjaxRequest(A) {
    var B = Snapdeal.getAjaxResourcesPath("/getRevampedLeftNav");
    $.ajax({
        url: B,
        type: "GET",
        dataType: "json",
        success: function(C) {
            if (C) {
                var E = getRevampedHTML(C);
                autoPositionForLeftNav();
                revampMoveWithCone();
                previousDocReady();
                try {
                    window.localStorage.setItem("newLeftNavStorage", E)
                } catch (D) {}
                if (A) {
                    lazySrc();
                    $(".lazyBg").removeClass("lazyBg")
                } else {
                    if (window.isLoaded) {
                        lazySrc();
                        $(".lazyBg").removeClass("lazyBg")
                    }
                }
            }
        },
        error: function() {
            previousDocReady()
        }
    })
}

function oldLeftNavAjaxRequest(A) {
    $.ajax({
        url: Snapdeal.getAjaxResourcesPath("/getLeftNav?sT=" + selectedTab + "&aT=" + activeProductTab + "&v=2"),
        type: "GET",
        data: "",
        dataType: "html",
        success: function(B) {
            $(".leftNavigationContainer").html(B);
            autoPositionForLeftNav();
            moveWithCone();
            previousDocReady();
            try {
                window.localStorage.setItem("leftNavHtmlRevamp", B)
            } catch (C) {}
            if (A) {
                lazySrc();
                $(".lazyBg").removeClass("lazyBg")
            }
        },
        error: function() {
            previousDocReady()
        }
    })
}
$(".leftCategoriesProduct").live("click", function(F) {
    var D = $(this);
    if (D.attr("href") != "javascript:void(0);") {
        var C = D.children("span").eq(0).html();
        var B = D.attr("href");
        var A = $(".navlink.trial1:hover").index() + 1;
        var E = (window.openFlowFlag == "true") ? "newLeftnavOpen" : "newLeftnavClose";
        Snapdeal.Cookie.set("track", "HID=" + E + "_" + C + "_level1_" + A + "_" + B);
        logTrack("track", "HID=" + E + "_" + C + "_level1_" + A + "_" + B)
    }
});
$(".L2AncorMenu").live("click", function(G) {
    var E = $(this);
    if (E.attr("href") != "javascript:void(0);") {
        var D = E.parents(".subMenuContainer").siblings(".leftCategoriesProduct").children()[0].innerHTML;
        var A = E.children("span").eq(0).html();
        var C = $(".submenu1li.navlink:hover").index() + 1;
        var B = E.attr("href");
        var F = (window.openFlowFlag == "true") ? "newLeftnavOpen" : "newLeftnavClose";
        Snapdeal.Cookie.set("track", "HID=" + F + "_" + D + "_" + A + "_level2_" + C + "_" + B);
        logTrack("track", "HID=" + F + "_" + D + "_" + A + "_level2_" + C + "_" + B)
    }
});
$('.leftNavChildWrapper map[track="omn"] area').live("click", function(G) {
    var E = $(this);
    var D = E.parents(".subMenuContainer").siblings(".leftCategoriesProduct").children()[0].innerHTML;
    var B = E.parents(".mainContentSubmenu").siblings(".L2AncorMenu").children()[0].innerHTML;
    var H = E.parents(".leftNavChildWrapper").attr("dalt");
    var A = "banner";
    var C = E.attr("href");
    var F = (window.openFlowFlag == "true") ? "newLeftnavOpen" : "newLeftnavClose";
    Snapdeal.Cookie.set("track", "HID=" + F + "_" + D + "_" + B + "_" + A + "_" + H + "_" + C);
    logTrack("track", "HID=" + F + "_" + D + "_" + B + "_" + A + "_" + H + "_" + C)
});
$(".thirdLevelFormFinal div a.leftNavp, .thirdLevelFormFinal div a.leftNavHead").live("click", function(G) {
    var E = $(this);
    var D = E.parents(".subMenuContainer").siblings(".leftCategoriesProduct").children()[0].innerHTML;
    var B = E.parents(".mainContentSubmenu").siblings(".L2AncorMenu").children()[0].innerHTML;
    var H = E.text();
    var A = (E.hasClass("leftNavp") ? "link" : "heading");
    var C = E.attr("href");
    var F = (window.openFlowFlag == "true") ? "newLeftnavOpen" : "newLeftnavClose";
    Snapdeal.Cookie.set("track", "HID=" + F + "_" + D + "_" + B + "_" + A + "_" + H + "_" + C);
    logTrack("track", "HID=" + F + "_" + D + "_" + B + "_" + A + "_" + H + "_" + C)
});
$(".leftNavChildWrapperOffer .offerFormFinal a").live("click", function(G) {
    var E = $(this);
    var D = E.parents(".subMenuContainer").siblings(".leftCategoriesProduct").children()[0].innerHTML;
    var B = E.parents(".mainContentSubmenu").siblings(".L2AncorMenu").children()[0].innerHTML;
    var H = E.children().attr("dalt");
    var A = "offer";
    var C = E.attr("href");
    var F = (window.openFlowFlag == "true") ? "newLeftnavOpen" : "newLeftnavClose";
    Snapdeal.Cookie.set("track", "HID=" + F + "_" + D + "_" + B + "_" + A + "_" + H + "_" + C);
    logTrack("track", "HID=" + F + "_" + D + "_" + B + "_" + A + "_" + H + "_" + C)
});
$(".thirdLevelFormFinal div p.PformPortLeft.logoPortLeft").live("click", function(G) {
    var E = $(this);
    var D = E.parents(".subMenuContainer").siblings(".leftCategoriesProduct").children()[0].innerHTML;
    var B = E.parents(".mainContentSubmenu").siblings(".L2AncorMenu").children()[0].innerHTML;
    var H = E.children("a").children("img").attr("dalt");
    var A = "logo";
    var C = E.find("a").attr("href");
    var F = (window.openFlowFlag == "true") ? "newLeftnavOpen" : "newLeftnavClose";
    Snapdeal.Cookie.set("track", "HID=" + F + "_" + D + "_" + B + "_" + A + "_" + H + "_" + C);
    logTrack("track", "HID=" + F + "_" + D + "_" + B + "_" + A + "_" + H + "_" + C)
});
(function() {
    var A = {
        seperator: "|",
        subSeperator: ":",
        subSubSeperator: "^",
        getFromHash: function(C) {
            var B = A.getAllFromHash();
            return B[C]
        },
        getAllFromHash: function() {
            var B = window.location.hash;
            return A._getAllFromHash(B)
        },
        _getAllFromHash: function(G) {
            G = G.split("#").join("");
            var C = G.split(A.seperator);
            var E = {};
            for (var D = 0; D < C.length; D++) {
                var F = C[D];
                var B = F.split(A.subSeperator);
                if (B[0]) {
                    E[unescape(B[0])] = unescape(B[1])
                }
            }
            return E
        },
        _putToHash: function(E, F, G) {
            if (typeof G == "string") {
                G = A._getAllFromHash(G)
            }
            var D = G || {};
            D[E] = F;
            var B = [];
            for (var C in D) {
                B.push(escape(C) + "" + A.subSeperator + escape(D[C]))
            }
            gDoNotReloadOnHashChange = true;
            return B.join(A.seperator)
        },
        putToHash: function(C, D) {
            var B = A.getAllFromHash();
            gDoNotReloadOnHashChange = true;
            window.location.hash = A._putToHash(C, D, B)
        }
    };
    window.Snapdeal = window.Snapdeal || {};
    Snapdeal.hashManager = A
})();
$(window).load(function() {
    if (loadingTime == 0) {
        loadingTime = (new Date().getTime() - startTime) / 1000
    }
    var A = $("#logServiceUrl").val();
    if (A != "" && A != undefined) {
        $.ajax({
            url: A + "/lg/all",
            dataType: "jsonp",
            cache: false,
            data: {
                u: Snapdeal.Cookie.get("u"),
                ref: document.referrer,
                url: document.URL,
                lt: loadingTime
            }
        });
        $.ajax({
            url: A + "/lg/iuv",
            dataType: "jsonp",
            cache: true,
            data: {
                url: document.URL
            },
            jsonp: false,
            jsonpCallback: "callback"
        });
        var B = Snapdeal.Cookie.get("u");
        var C = Snapdeal.Cookie.get("uid");
        if (B != undefined && B != "" && C != undefined && C != "") {
            $.ajax({
                url: A + "/lg/uum",
                dataType: "jsonp",
                cache: true,
                data: {
                    u: B,
                    uid: C
                },
                jsonp: false,
                jsonpCallback: "callback"
            })
        }
    }
});

function noLog() {}

function logNotFoundImage() {
    setTimeout(logNotFoundImageAfterTimeout, 20000)
}

function logNotFoundImageAfterTimeout() {
    var A = "";
    $("img").each(function() {
        var B = $(this);
        if ((B.attr("src") != undefined && B.attr("src") != null && B.attr("src") != "") && (B.attr("src").indexOf("png") != -1 || B.attr("src").indexOf("gif") != -1 || B.attr("src").indexOf("jpg") != -1) && (B.attr("src").indexOf("snapdeal.com") != -1)) {
            if (B.attr("naturalWidth") == "0") {
                A += B.attr("src") + "\t"
            }
        }
    });
    if (A != "") {
        $.ajax({
            url: $("#logServiceUrl").val() + "/lg/inf",
            dataType: "jsonp",
            data: {
                url: document.URL,
                ips: A
            }
        })
    }
}
if ($("#logServiceUrl").val() != "" && $("#logServiceUrl").val() != undefined) {
    window.onerror = function(D, B, A) {
        var C = "Line No.: " + A + " >> " + D + " on " + B;
        C += " for this hit >> " + document.URL;
        $.ajax({
            url: $("#logServiceUrl").val() + "/lg/jse",
            dataType: "jsonp",
            data: {
                log: C
            }
        });
        return false
    }
}
$(".hit-ss-logger").live("click", function() {
    vwSrchdPrd($(this), $(this).attr("href"))
});

function vwSrchdPrd(B, A) {
    if ($(B).attr("categoryId") && $(B).attr("v") && $(B).attr("pogId") && $(B).attr("pos") && $(B).attr("isPartialSearch")) {
        $.ajax({
            url: "/json/ssLogger",
            dataType: "json",
            type: "GET",
            async: false,
            cache: false,
            data: {
                query: $("#lastKeyword").val(),
                cid: $(B).attr("categoryId"),
                v: $(B).attr("v"),
                pogId: $(B).attr("pogId"),
                pos: $(B).attr("pos"),
                isPartialSearch: $(B).attr("isPartialSearch")
            },
            success: function(C) {
                var D = C.ssdto.query + ";" + C.ssdto.category + ";" + C.ssdto.vertical;
                Snapdeal.Cookie.set("searchString", D, 3)
            }
        })
    }
};

function submitSearchForm(F, E, H) {
    var G = false;
    var A = false;
    var I = $("#vertical").val();
    var C = $("#suggested").val();
    var B = $("#selectedLocality").val();
    $("#categoryId").val(E);
    $("#foundInAll").val(false);
    $("#clickSrc").val(F);
    G = (E != null && E != undefined && E != "0") ? true : false;
    A = ($("#keyword").val() == langSpecificLabels["label.searchBar.defaultString"] || $("#keyword").val() == "") ? false : true;
    if (G == false && A == false) {
        $("#searchBoxOuter").addClass("search-boxes-error");
        $(".search-error").html("Please choose a category or enter a keyword to search");
        return false
    }
    if (!A) {
        $("#keyword").val("")
    }
    var D = $("#keyword").val();
    if (A && (D.length == 0)) {
        $("#searchBoxOuter").addClass("search-boxes-error");
        $(".search-error").html("Please enter atleast a character");
        return false
    }
    if (F == "go_header") {
        $("#changeBackToAll").val(false)
    }
    window.sessionStorage.setItem("logLastSearch", $("#keyword").val());
    if (!H) {
        $("#formSearch").submit()
    }
    return true
}

function topSearchItems() {
    var A = $("#keyword").val();
    $.ajax({
        url: "json/topSimilarSearchProducts/get?keyword=" + escape(A),
        type: "GET",
        dataType: "json",
        success: function(B) {
            displayTopSearchProducts(B)
        }
    })
}

function displayTopSearchProducts(C) {
    var B = $("#keyword").val();
    if (C.productDtos != null) {
        var A = "";
        A += '<div class="srchwgt_heading"><span style="color:#292A2A">Top Viewed Products for</span> "' + B + '"</div>';
        A += '<div class="top_selling_products_con">';
        A += '<div class="multiple overhid" ><div style="width:740px;">';
        $.each(Snapdeal.Utils.fixArray(C.productDtos), function(E, F) {
            if ((E % 4 == 0) && E != 0) {
                A += "</div><div>"
            }
            E = E + 1;
            var D = (Snapdeal.getResourcePath(F.image)).replace("166x194", "small");
            A += '<div class="lfloat" style="width:185px"> <a href="' + Snapdeal.getStaticPath("/" + F.pageUrl + "?searchid=topviewed_" + E) + '">';
            A += '<div class="srchwgt_prdct">';
            A += '<div class="srchwgt_thumb"><img src="' + D + '" alt="" /></div>';
            A += '<div class="srchwgt_prdct_dtl">';
            A += '<div class="srchwgt_title">' + F.name + "<br /> <strong>Rs " + F.displayPrice + "</strong>";
            if (F.freebies != undefined) {
                A += '<div class="freebie-ico">Freebie inside</div>'
            }
            A += "</div>";
            A += "</div> </div>";
            A += "</a> </div>"
        });
        A += "</div></div></div>";
        $(".topSearchItems").html(A);
        $(".multiple").bxSlider({
            auto: false,
            pager: false,
            infiniteLoop: true,
            hideControlOnEnd: true
        })
    }
}
var cacheAS = {};
var cacheURL = {};
var moreCatIndex = false;
var showMoreCatgrs = false;
$.widget("custom.catcomplete", $.ui.autocomplete, {
    _renderMenu: function(E, F) {
        var I = this,
            H = "",
            C = 0,
            K = "",
            D = 0,
            G = "";
        var B = $("#autoSuggestorVersion").html();
        if (B == "v1") {
            $(E).addClass("autov1")
        } else {
            if (B == "v2") {
                $(E).addClass("autov2");
                if (Snapdeal.Cookie.get("alps") == "2") {
                    $(E).addClass("hd-rvmpAs")
                }
                setTimeout(function() {
                    $(E).find("li:last-child").addClass("lastChild")
                }, 1)
            }
        }
        $.each(F, function(L, M) {
            K = "";
            if (moreCatIndex == true && H != "" && C == 0 && M.category != H && $(".all-categories-drop-box").attr("catId") != "0") {
                K = " orangeClass";
                C = 1
            }
            if (B == "v1") {
                if (M.v == "showMore") {
                    K = " orangeClass";
                    G += "<li class='ui-autocomplete-category" + K + "' vid=" + M.v + " cid=" + M.cid + ">" + M.category + "</li>"
                }
            }
            if (M.category != "Match found in more categories") {
                K = ""
            }
            if (B == "v1") {
                if (M.category != H && M.v != "showMore") {
                    if (D == 1) {
                        E.append(G)
                    }
                    E.append("<li class='ui-autocomplete-category" + K + "' vid=" + M.v + " cid=" + M.cid + ">" + M.category + "</li>");
                    H = M.category;
                    D++
                }
            }
            I._renderItem(E, M);
            $(".ui-autocomplete li").last().attr("v", M.v);
            $(".ui-autocomplete li").last().attr("cn", M.category);
            $(".ui-autocomplete li").last().attr("cid", M.cid);
            $(".ui-autocomplete li").last().children().attr("v", M.v);
            $(".ui-autocomplete li").last().children().attr("cn", M.category);
            $(".ui-autocomplete li").last().children().attr("cid", M.cid)
        });
        var A = $("#keyword").val().toString().trim();
        A = A.replace(/[^a-zA-Z0-9' ]/g, "");
        A = A.trim();
        var J = A.split(" ");
        var B = $("#autoSuggestorVersion").html();
        $(".ui-autocomplete li").each(function() {
            var O = $(this);
            if (B == "v1") {
                if ($(this).hasClass("ui-autocomplete-category") == false) {
                    $.each(J, function(P, S) {
                        if (O.children().html() != null && S != "" && S != "b") {
                            var Q = O.children().html().toString().trim();
                            var R = new RegExp("(" + S + ")", "ig");
                            Q = Q.replace(R, "<span>$1</span>");
                            O.children().html(Q)
                        }
                    })
                }
            } else {
                if (B == "v2") {
                    var N = O.attr("cn");
                    var M = O.children().html().toString().trim();
                    $.each(J, function(P, R) {
                        if (O.children().html() != null && R != "" && R != "b") {
                            var Q = new RegExp(R + "(?=[^<>]*(<|$))", "i");
                            M = M.replace(Q, "<span>$&</span>")
                        }
                    });
                    M = "<span class='suggestedText'>" + M + "</span> ";
                    var L = M + '<span class="searchRed"> in ' + N + "</span>";
                    O.children().html(L)
                }
            }
        });
        if ($(".orangeClass").html() != "Match found in more categories") {
            $(".orangeClass").removeClass("orangeClass")
        }
    }
});
if (Snapdeal.componentSwitches.autoSuggestClosed == "false") {
    $(function() {
        function A() {
            $("#keyword").catcomplete({
                source: function(F, D) {
                    var E = F.term;
                    var C = "q=" + $("#keyword").val().toString().trim().replace(/[^a-zA-Z0-9' ]/g, "") + "&catId=" + $(".all-categories-drop-box").attr("catId") + "&v=" + $(".all-categories-drop-box").attr("vId");
                    if ((E in cacheAS) && (C in cacheURL)) {
                        D(cacheAS[E]);
                        if (cacheAS[E][0].label == "No items found") {
                            $(".ui-autocomplete").hide()
                        }
                        return
                    }
                    var B = $("#keyword").val().toString().trim().replace(/[^a-zA-Z0-9' ]/g, "");
                    B = B.trim();
                    $.ajax({
                        url: "/json/search/autoSuggestion",
                        dataType: "json",
                        data: {
                            q: B,
                            catId: "0",
                            v: $(".all-categories-drop-box").attr("vId")
                        },
                        success: function(I) {
                            moreCatIndex = false;
                            var G = parseAutoSuggestData(I, E);
                            cacheURL[C] = C;
                            D(G);
                            if (G[0].label == "No items found") {
                                $(".ui-autocomplete").hide()
                            }
                            var H = $(".autov2.ui-autocomplete").position().left;
                            $(".autov2.ui-autocomplete").css("left", H)
                        }
                    })
                },
                minLength: 0,
                autoFocus: false,
                select: function(B, C) {
                    $("#suggested").val(true);
                    $("#keyword").val(C.item.value);
                    $("#vertical").val(C.item.v);
                    if (C.item.v != "l") {
                        $("#strLocation, #localityAutocomplete").attr("disabled", true)
                    } else {
                        $("#strLocation").attr("disabled", false)
                    }
                    $('a[catId="' + C.item.cid + '"][vid="' + C.item.v + '"]').click();
                    submitSearchForm("suggested", C.item.cid)
                },
                change: function() {
                    $("#strLocation").attr("disabled", false);
                    $("#suggested").val(false)
                },
                delay: 300,
                disabled: false
            }).addClass("alreadyInit")
        }
        A();
        $("#keyword:not(.alreadyInit)").live("keyup.catcomplete", function() {
            A()
        })
    })
}
var refnSearchData = [];

function parseAutoSuggestData(B, A) {
    refnSearchData = [];
    showMoreCatgrs = false;
    if (B != "[]" && B != null && B != "") {
        var C = $("#autoSuggestorVersion").html();
        $.each(B, function(F) {
            if (C == "v2") {
                var H = this.categoryName;
                var E = "p";
                var D = this;
                var G = this.categoryId;
                if ($.isArray(D)) {
                    $.each(D, function(I) {
                        refnSearchData.push({
                            label: this.keyword,
                            category: H,
                            v: E,
                            cid: G,
                            cat: H
                        })
                    })
                } else {
                    if (D != null) {
                        refnSearchData.push({
                            label: D.keyword,
                            category: H,
                            v: E,
                            cid: G,
                            cat: H
                        })
                    }
                }
            } else {
                this.ssd = this;
                var H = this.ssd.cn;
                var E = this.ssd.v;
                var D = this.ssd.sgns;
                var G = this.ssd.cid;
                if (this.ssd.moreIndex == true && $(".all-categories-drop-box").attr("catId") != "0") {
                    moreCatIndex = true;
                    refnSearchData.push({
                        label: null,
                        category: "No results in " + $('a[catId="' + $(".all-categories-drop-box").attr("catId") + '"][vid="' + $(".all-categories-drop-box").attr("vId") + '"]').html(),
                        v: ""
                    });
                    refnSearchData.push({
                        label: null,
                        category: "Match found in more categories",
                        v: ""
                    })
                }
                if ($.isArray(D)) {
                    if (showMoreCatgrs == false && moreCatIndex == false && this.ssd.moreIndex == false && $(".all-categories-drop-box").attr("catId") != "0") {
                        refnSearchData.push({
                            label: null,
                            category: "Match found in more categories",
                            v: "showMore",
                            cid: ""
                        });
                        showMoreCatgrs = true
                    }
                    $.each(D, function(I) {
                        refnSearchData.push({
                            label: this.sn,
                            category: H,
                            v: E,
                            cid: G
                        })
                    })
                } else {
                    if (D != null) {
                        refnSearchData.push({
                            label: D.sn,
                            category: H,
                            v: E,
                            cid: G
                        })
                    }
                    if (showMoreCatgrs == false && moreCatIndex == false && this.ssd.moreIndex == false && $(".all-categories-drop-box").attr("catId") != "0") {
                        refnSearchData.push({
                            label: null,
                            category: "Match found in more categories",
                            v: "showMore",
                            cid: ""
                        });
                        showMoreCatgrs = true
                    }
                }
            }
        })
    } else {
        refnSearchData.push({
            label: "No items found",
            category: "",
            v: ""
        })
    }
    cacheAS[A] = refnSearchData;
    return refnSearchData
}

function initKeyword() {
    if ($("#keyword").val() == "") {
        $("#keyword").val(langSpecificLabels["label.searchBar.defaultString"]);
        $("#keyword").addClass("search-box-text-unactive")
    }
    if ($("#keyword").val() == langSpecificLabels["label.searchBar.defaultString"]) {
        $("#keyword").addClass("search-box-text-unactive")
    }
}
if (($.browser.version == "8.0" || $.browser.version == "9.0") && $.browser.msie) {
    initKeyword()
}
$("#keyword").live("focus", function() {
    if ($(this).val() == langSpecificLabels["label.searchBar.defaultString"]) {
        $(this).val("").removeClass("search-box-text-unactive");
        $(".search-error-outer").hide();
        $("#searchBoxOuter").removeClass("search-boxes-error")
    }
});
$("#keyword").live("blur", function() {
    if ($(this).val() == "") {
        $(this).val(langSpecificLabels["label.searchBar.defaultString"]).addClass("search-box-text-unactive");
        $(".search-error-outer").hide();
        $("#searchBoxOuter").removeClass("search-boxes-error")
    }
});

function clickGo(A) {
    if (A.keyCode == 13) {
        $("#changeBackToAll").val(false);
        submitSearchForm("go_header");
        return true
    } else {
        return false
    }
}

function searchOnSubCat(A) {
    $("#categoryId").val(A);
    $("#vertical").val("p");
    submitSearchForm("searchOnSubCat", A)
}

function searchOnBrand(B, C) {
    $("#categoryId").val(B);
    $("#vertical").val("p");
    submitSearchForm("searchOnBrand", B, true);
    var D = $("#formSearch").serialize();
    var A = D + "&q=Brand:" + C;
    return A
}

function searchUrlUpdate() {
    if (window.isSubCatLinkOnJs) {
        $("a.convertToLinks").attr("href", function() {
            var A = $(this).attr("catId");
            $(this).attr("onclick", "");
            if ($(this).attr("rbftype") == "brand") {
                var B = $(this).attr("brandName");
                return window.sdHttpPath + "/search?" + searchOnBrand(A, B)
            } else {
                return window.sdHttpPath + "/search?" + getSearchOnSubCatLink(A)
            }
        })
    }
}

function getSearchOnSubCatLink(A) {
    $("#categoryId").val(A);
    $("#vertical").val("p");
    submitSearchForm("searchOnSubCat", A, true);
    return $("#formSearch").serialize()
}

function partnerResults(A, E, B) {
    if (Snapdeal.componentSwitches.ebayClosed == "false") {
        var D = document.createElement("div");
        D.innerHTML = A;
        A = $(D).text();
        A = A.replace(/^["']/, "").replace(/["']$/, "");
        var C = "/json/getPartnerSearchResults?partnerName=ebay&keyword=" + A + "&start=" + E + "&noOfResults=" + B;
        $.ajax({
            url: C,
            type: "GET",
            dataType: "json",
            success: function(H) {
                var G = "";
                if (H) {
                    Snapdeal.Utils.fixArray(H);
                    if (H.length > 0) {
                        for (var F = 0; F < H.length; F++) {
                            G = G + '<div class="multiImgHolder wpr">';
                            G = G + '<a itemprop="url" name= "' + H[F].searchPartnerItemSRO.title + '" class="img ebayClick" target="_blank" href="' + H[F].searchPartnerItemSRO.url + '">';
                            G = G + '<div class="ImgBlock"><img alt="' + H[F].searchPartnerItemSRO.subtitle + '" class="img" src="' + H[F].searchPartnerItemSRO.imageUrl + '" itemprop="image">';
                            G = G + "</div>";
                            G = G + '<div class="ebayProductTitle">' + H[F].searchPartnerItemSRO.title + "</div>";
                            G = G + '<div class="ebayPrice">Rs ' + H[F].searchPartnerItemSRO.sellingPrice + "</div>";
                            if (H[F].searchPartnerItemSRO.freeShipping) {
                                G = G + '<div class="shipTxt">Free shipping</div>'
                            }
                            G = G + '<div class="ebayLink">Buy It Now</div> </a></div>'
                        }
                        $("#ebayResults").show()
                    } else {
                        $("#suggestions").show()
                    }
                }
                $("#ebaySuggestionContainer").append(G)
            }
        })
    }
}
$("#categoryId").val($.trim($("#categoryId").val()) || "0");
var wishlistArr = [];
var gActiveWishlistPogsList2 = function() {
    if (Snapdeal.signupWidConfig.loggedIn) {
        return wishlistArr
    } else {
        if (SnapBottomBar.wishList) {
            return SnapBottomBar.wishList.store.get()
        } else {
            return []
        }
    }
};

function getActiveWishlistProds() {
    if (Snapdeal.signupWidConfig.loggedIn) {
        if (window.localStorage.getItem("add-to-wishlist-pog") != null && window.localStorage.getItem("add-to-wishlist-pog") != "") {
            var C = window.localStorage.getItem("add-to-wishlist-pog").split(";")[0];
            var A = window.localStorage.getItem("add-to-wishlist-pog").split(";")[1];
            window.localStorage.removeItem("add-to-wishlist-pog");
            AddToWishlist(C, A);
            var B = {};
            B.pogId = C;
            fireOmnitureScriptForWishlist(B, "/omniture/getAddToWLScript")
        }
    }
}

function hgltWishlistProds() {
    var C;
    if (C = window.gActiveWishlistPogsList2()) {
        var B = $("#pppid").text();
        var A;
        if (B) {
            if (C.indexOf(B) != -1) {
                displayWishlistStatus(true)
            } else {
                displayWishlistStatus(false)
            }
        }
        if (window.selectedView == "Grid") {
            A = $(".product_grid_cont")
        } else {
            if (window.selectedView == "List") {
                A = $(".product_list_view_cont")
            } else {
                A = $("div[prodcat-id]")
            }
        }
        A.find(".icon-blue-heart").addClass("icon-grey-heart").removeClass("icon-blue-heart");
        A.find(".wish-text").text(langSpecificLabels["label.categoryPage.addToWishlist"]);
        A.find(".wishOnProduct").attr("data-selected", "false");
        if (window.selectedView == "Grid") {
            $.each(C, function(D, E) {
                A = $("#" + E);
                A.find(".icon-grey-heart").addClass("icon-blue-heart").removeClass("icon-grey-heart");
                A.find(".wish-text").text(langSpecificLabels["label.categoryPage.addedToWishlist"]);
                A.find(".wishOnProduct").attr("data-selected", "true")
            });
            $.each(C, function(D, E) {
                A = $(".product_grid_cont[prodcat-id=" + E + "]");
                A.find(".icon-grey-heart").addClass("icon-blue-heart").removeClass("icon-grey-heart");
                A.find(".wish-text").text(langSpecificLabels["label.categoryPage.addedToWishlist"]);
                A.find(".wishOnProduct").attr("data-selected", "true")
            })
        } else {
            if (window.selectedView == "csf" || window.selectedView == "pdp") {
                $.each(C, function(D, E) {
                    A = $(".product_grid_cont[prodcat-id=" + E + "]");
                    A.find(".icon-grey-heart").addClass("icon-blue-heart").removeClass("icon-grey-heart");
                    A.find(".wish-text").text(langSpecificLabels["label.categoryPage.addedToWishlist"]);
                    A.find(".wishOnProduct").attr("data-selected", "true")
                })
            } else {
                $(".listWishSelect").html("<i class=icon-grey-heart></i> " + langSpecificLabels["label.categoryPage.addToWishlist"]).attr("data-selected", "false").removeClass("liteBlue").addClass("litegrey");
                $.each(gActiveWishlistPogsList2(), function(D, E) {
                    $("#" + E).find(".listWishSelect").html("<i class=icon-blue-heart></i> " + langSpecificLabels["label.categoryPage.addedToWishlist"] + " >").attr("data-selected", "true").addClass("liteBlue").removeClass("litegrey")
                })
            }
        }
    }
}
$(".wishOnProduct,.listWishSelect,.add-to-wishlist").live("click", function() {
    var E = $(this).attr("pog");
    var A = $(this);
    window.selView = ($(this).hasClass("wishOnProduct")) ? "Grid" : "List";
    selView = (($(this).hasClass("wishOnCsf")) ? "CSF" : selView);
    var C;
    if ($(this).hasClass("wishOnProduct")) {
        C = $(this).parents(".product_grid_cont").find(".product-image img")
    } else {
        if ($(this).hasClass("listWishSelect")) {
            C = $(this).parents(".product_list_view_cont").find(".product-image img")
        } else {
            if ($(this).hasClass("add-to-wishlist")) {
                C = $(".active-slider img")
            }
        }
    }
    var H = $(this).attr("data-selected") || "false";
    if (H == "false") {
        var B = AddToWishlist(E, selView);
        if (B) {
            $(this).attr("data-selected", "true");
            hgltWishlistProds();
            if (C) {
                moveItemToDest(C, $(".bottomFreezeShortListCount"))
            }
            var D = {};
            D.pogId = E;
            fireOmnitureScriptForWishlist(D, "/omniture/getAddToWLScript")
        }
    } else {
        if (H == "true") {
            var D = {};
            fireOmnitureScriptForWishlist(D, "/omniture/getRemoveFromWLScript");
            checkAuth(function G() {
                RemoveFromWishlist(E, selView);
                A.attr("data-selected", "false")
            }, function F() {
                if (A.hasClass("listWishSelect")) {
                    window.location.href = httpPath + "/mywishlist"
                } else {
                    RemoveFromWishlist(E, selView);
                    A.attr("data-selected", "false")
                }
            })
        }
    }
});

function AddToWishlist(G, B) {
    var E = Snapdeal.Utils.fixArray(G);
    var F = E.join(",");
    var H = "/wishlist/add?pog=" + F;
    var A = Snapdeal.signupWidConfig.loggedIn;
    if (!A) {
        var D = SnapBottomBar.wishList.add(E);
        if (D == "max") {
            return false
        } else {
            hgltWishlistProds();
            return true
        }
    } else {
        wishlistArr.push.apply(wishlistArr, E);
        hgltWishlistProds();
        var C = SnapBottomBar.wishList.store;
        SnapBottomBar.wishList.add(E);
        C.setCount(C.getCount() + 1);
        return $.ajax({
            url: H,
            type: "GET",
            dataType: "json",
            success: function(I) {
                if (I && I.status == "SUCCESS") {}
            }
        })
    }
    return true
}

function _removeFromWishlist(A) {
    var B = "/wishlist/remove?pog=" + A;
    return $.ajax({
        url: B,
        type: "GET",
        dataType: "json"
    })
}

function _removeFromWishlistAndRefresh(D, B) {
    hgltWishlistProds();
    var A = gActiveWishlistPogsList2() || [];
    var C = A.indexOf(D);
    if (C != -1) {
        wishlistArr.splice(C, 1)
    }
    var E = $("");
    if (B == "Grid") {
        var E = $("#" + D)
    } else {
        if (B == "CSF") {
            var E = $("div[prodcat-id=" + D + "]")
        }
    }
    E.find(".icon-blue-heart").addClass("icon-grey-heart").removeClass("icon-blue-heart");
    E.find(".wish-text").text(langSpecificLabels["label.categoryPage.addToWishlist"]);
    E.find(".wishOnProduct").attr("data-selected", "false")
}

function RemoveFromWishlist(C, B) {
    var B = B || "";
    SnapBottomBar.wishList.remove(C);
    var A = Snapdeal.signupWidConfig.loggedIn;
    if (!A) {
        _removeFromWishlistAndRefresh(C, B)
    } else {
        _removeFromWishlistAndRefresh(C, B);
        _removeFromWishlist(C)
    }
}

function fireOmnitureScriptForWishlist(B, A) {
    Snapdeal.omni.fire("AddToWishlist", B)
}

function displayWishlistStatus(A) {
    checkAuth(function() {
        if (A) {
            $(".wishlist-select-show").eq(0).removeClass("added-to-wishlist").addClass("add-to-wishlist").attr("data-selected", "true");
            $(".wishlist-select-show").eq(0).html('<a class="pdp-added-to-wishlist-lang liteBlue"><i class=icon-blue-heart></i> ' + langSpecificLabels["label.pdpBuyWidget.addedToWishlist"] + " ></a>")
        } else {
            $(".wishlist-select-show").eq(0).removeClass("added-to-wishlist").addClass("add-to-wishlist").attr("data-selected", "false");
            $(".wishlist-select-show").eq(0).html('<a class="pdp-add-to-wishlist-lang litegrey"><i class=icon-grey-heart></i> ' + langSpecificLabels["label.pdpBuyWidget.addToWishlist"] + "</a>")
        }
    }, function() {
        var B = $(".wishlist-select-show").attr("data-selected");
        if (B == "true") {
            $(".wishlist-select-show").eq(0).removeClass("add-to-wishlist").addClass("added-to-wishlist");
            $(".wishlist-select-show").eq(0).html('<a href="/mywishlist" class="pdp-added-to-wishlist-lang liteBlue"><i class=icon-blue-heart></i> ' + langSpecificLabels["label.pdpBuyWidget.addedToWishlist"] + " ></a>")
        } else {
            $(".wishlist-select-show").eq(0).removeClass("added-to-wishlist").addClass("add-to-wishlist");
            $(".wishlist-select-show").eq(0).html('<a class="pdp-add-to-wishlist-lang litegrey"><i class=icon-grey-heart></i> ' + langSpecificLabels["label.pdpBuyWidget.addToWishlist"] + "</a>")
        }
    })
};
$(window).load(function() {
    if (typeof isLocalizationEnabled == undefined || typeof isLocalizationEnabled == "undefined") {
        return false
    }
    if (isLocalizationEnabled == "true") {
        var F = window.location.pathname;
        var D = /^\/(en|hi|ta)\//;
        var B = /^\/(en|hi|ta)$/;
        if (D.test(F) || B.test(F)) {
            var C = F.match(D);
            if (B.test(F)) {
                C = F.match(B)
            }
            if (C != null && C.length > 1) {
                var A = C[1];
                var E = "en";
                if (A == "hi") {
                    E = "hindi"
                } else {
                    if (A == "ta") {
                        E = "tamil"
                    }
                }
                Snapdeal.Cookie.remove("lang");
                Snapdeal.Cookie.remove("lang", "/", document.location.hostname);
                Snapdeal.Cookie.set("lang", E, 90)
            }
        }
        if (Snapdeal.Cookie.get("lang") == null || Snapdeal.Cookie.get("lang") == "" || Snapdeal.Cookie.get("lang") == undefined || Snapdeal.Cookie.get("lang") == "undefined") {
            Snapdeal.Cookie.remove("lang");
            Snapdeal.Cookie.remove("lang", "/", document.location.hostname);
            Snapdeal.Cookie.set("lang", "en", 90)
        }
        if (window.localizationHeaderToggle === "0") {
            $("#localizationHeader").css("display", "none")
        }
        if (window.localizationHeaderToggle === "1") {
            if (window.localStorage.getItem("localizationHeaderState") == null && Snapdeal.Cookie.get("lang") == "en") {
                window.localStorage.setItem("localizationHeaderState", localizationHeaderState);
                displayLocalizationHeader()
            } else {
                if (parseInt(localizationHeaderState, 10) >= parseInt(window.localStorage.getItem("localizationHeaderState"), 10) && Snapdeal.Cookie.get("lang") == "en") {
                    window.localStorage.setItem("localizationHeaderState", localizationHeaderState);
                    displayLocalizationHeader()
                }
            }
        }
    } else {
        Snapdeal.Cookie.remove("lang");
        Snapdeal.Cookie.remove("lang", "/", document.location.hostname);
        Snapdeal.Cookie.set("lang", "en", 90)
    }
});
$(".setLang").click(function() {
    window.localStorage.setItem("localizationHeaderState", (parseInt(window.localStorage.getItem("localizationHeaderState"), 10) + 1));
    if ($("#keyword").val() == langSpecificLabels["label.searchBar.defaultString"]) {
        $("#keyword").val("")
    }
    var C = $(this).attr("lang");
    if (C != Snapdeal.Cookie.get("lang")) {
        var B = $(this).attr("switchSrc");
        localStorage.setItem("localizationSwitchSrc", B)
    }
    Snapdeal.Cookie.remove("lang");
    Snapdeal.Cookie.remove("lang", "/", document.location.hostname);
    Snapdeal.Cookie.set("lang", C, 90);
    var A = modifyLangUrl();
    if (A[0] == true) {
        window.location = A[1]
    } else {
        window.location.reload()
    }
});

function modifyLangUrl() {
    var C = /\/(en|hi|ta)\//;
    var B = /\/(en|hi|ta)$/;
    var D = window.location.href;
    var A = false;
    if (C.test(D)) {
        A = true;
        return [A, D.replace(C, "/")]
    } else {
        if (B.test(D)) {
            A = true;
            return [A, D.replace(B, "/")]
        } else {
            return [A, D]
        }
    }
}

function showLanguageModal() {
    $("#lang_modal_box").css("display", "block");
    $(document).bind("keyup.escapeListener", function(A) {
        if (A.keyCode == 27) {
            hideLanguageModal()
        }
    })
}

function hideLanguageModal() {
    $("#sel-lang-modal").html("Select");
    $("#localization-modal-lang-drop").slideUp(100);
    $(document).unbind("keyup.escapeListener");
    $("#lang_modal_box").css("display", "none");
    $(".error-lang").css("display", "none");
    $("#sel-lang-modal").attr("lang", "")
}

function displayLocalizationHeader() {
    $("#localizationHeader").css("display", "block");
    $(".arrow-down").css("display", "block");
    $("#localizationHeader").slideDown(300);
    $(".arrow-down").slideDown(300)
}
$("#close-localization-header").bind("click", function() {
    window.localStorage.setItem("localizationHeaderState", (parseInt(window.localStorage.getItem("localizationHeaderState"), 10) + 1));
    $("#localizationHeader").css("display", "none");
    $(".arrow-down").css("display", "none");
    $("#localizationHeader").slideUp(300);
    $(".arrow-down").slideUp(300);
    $("#trendingNowWrapper").css("top", "555px")
});
$("#dropLang-selectBox").bind("click", function(A) {
    if (A.preventDefault) {
        A.preventDefault()
    }
    A.stopPropagation();
    if ($("#lang-droplist").css("display") == "none") {
        $("#lang-droplist").css("display", "block")
    } else {
        $("#lang-droplist").css("display", "none")
    }
    $(document).one("click", function(B) {
        if ($("#lang-droplist").has(B.target).length === 0) {
            $("#lang-droplist").css("display", "none")
        }
    })
});
$("#localization-modal-lang-chooser").click(function(A) {
    if (A.preventDefault) {
        A.preventDefault()
    }
    A.stopPropagation();
    $("#localization-modal-lang-drop").slideDown(100);
    $(document).one("click", function(B) {
        if ($("#localization-modal-lang-drop").has(B.target).length === 0) {
            $("#localization-modal-lang-drop").slideUp(100)
        }
    })
});
$(".chooseLang").bind("click", function(A) {
    if (A.preventDefault) {
        A.preventDefault()
    }
    A.stopPropagation();
    var C = $(this).html();
    $("#sel-lang-modal").html(C);
    var B = $(this).attr("lang");
    $("#sel-lang-modal").attr("lang", B);
    $("#localization-modal-lang-drop").slideUp(100);
    $(".error-lang").css("display", "none")
});
$(".buttonBigBlue").click(function() {
    window.localStorage.setItem("localizationHeaderState", (parseInt(window.localStorage.getItem("localizationHeaderState"), 10) + 1));
    var B = $("#sel-lang-modal").attr("lang");
    if (B == "") {
        $(".error-lang").css("display", "inline");
        return false
    }
    if ($("#keyword").val() == langSpecificLabels["label.searchBar.defaultString"]) {
        $("#keyword").val("")
    }
    $(".error-lang").css("display", "none");
    if (B != Snapdeal.Cookie.get("lang")) {
        localStorage.setItem("localizationSwitchSrc", "modal")
    }
    Snapdeal.Cookie.remove("lang");
    Snapdeal.Cookie.remove("lang", "/", document.location.hostname);
    Snapdeal.Cookie.set("lang", B, 90);
    var A = modifyLangUrl();
    if (A[0] == true) {
        window.location = A[1]
    } else {
        window.location.reload()
    }
});

function isLocalizationHeaderVisible() {
    return (((window.localStorage.getItem("localizationHeaderState") == null || parseInt(localizationHeaderState, 10) >= parseInt(window.localStorage.getItem("localizationHeaderState"), 10)) && Snapdeal.Cookie.get("lang") == "en") ? true : false)
};
var lStorageKeys = window.lStorageKeys || [];
var productListObjs = {};
var ProductList = function(A, B) {
    var N = this;
    var C = Snapdeal.Utils.fixArray;
    var G = {
        localStorageKey: "plStore-" + A,
        localStorageTempKey: "plStore-" + A + "-temp",
        pogFetchUrl: "/json/getProductById",
        pogRecoFetchUrl: "/json/gsr",
        identifierClass: "btmFreezProd",
        attachPoint: "",
        getStrategy: "",
        isLazy: true,
        pagesize: 5,
        maxItems: 1000,
        truncateOnMax: true,
        attachPointParent: ".bottomFreezeDiv"
    };
    var I = this.cfg = $.extend(G, B);
    productListObjs[I.localStorageKey] = this;
    var F = G.maxItems;
    this.getMaxItems = function() {
        return F
    };
    this.setMaxItems = function(P) {
        F = parseInt(P) || F || 0
    };
    var M = {
        _getData: function(X, R, V, W, S) {
            var U = V.pogArr;
            var Z = V._ || {};
            U = C(U);
            U = U.filter(function(a) {
                if ($.trim(a)) {
                    return true
                }
            });
            var Y = {};
            var Q = [];
            var P = window.fetcherStore;
            if (R) {
                if (P) {
                    Y = P.get(X, U)
                }
                for (var T in Y) {
                    Q.push(Y[T])
                }
                U = U.filter(function(a) {
                    return !Y[a]
                });
                if (U.length == 0) {
                    if (W) {
                        W(Q)
                    }
                    return
                }
            }
            $.ajax({
                url: X,
                data: $.extend(Z, {
                    pogIds: U.join(","),
                    lang: Snapdeal.Cookie.get("lang")
                }),
                dataType: "text",
                success: function(d) {
                    d = $.parseJSON(d) || {};
                    if (I.getStrategy == "reco") {
                        d = d.data
                    }
                    d = d || [];
                    var b = d.length;
                    P.putFromArr(X, d);
                    d.concat(Q);
                    var c = d.map(function(e) {
                        return e.id
                    });
                    var a = U.filter(function(e) {
                        return c.indexOf(e) == -1
                    });
                    if (W) {
                        W(d, a)
                    }
                },
                error: function() {
                    if (S) {
                        S(U)
                    }
                }
            })
        },
        getDetailsForPOG: function(P, R, Q) {
            M._getData(I.pogFetchUrl, true, {
                pogArr: P
            }, function(T, S) {
                console.log(S);
                R.apply(this, arguments)
            }, Q)
        },
        getRecoProducts: function E(P, S, V, U, T) {
            var R = "";
            var W = [""];
            var Q = [];
            if (P.length > 0) {
                R = P.reduce(function(X, a) {
                    var Y = X.split(":");
                    var Z = a.split(":");
                    return Y[0] + "," + Z[0] + ":" + Y[1] + "," + Z[1]
                });
                W = R.split(":");
                Q = W[1].split(",")
            }
            M._getData(I.pogRecoFetchUrl, false, {
                pogArr: Q,
                _: {
                    catIds: W[0],
                    start: S,
                    count: V
                }
            }, U, T)
        },
        getWishlistProductsData: function L(P, T, S, Q) {
            var R = "/wishlist/getProducts/" + P + "/" + T + "?sort=dhtl&lang=" + Snapdeal.Cookie.get("lang");
            $.ajax({
                url: R,
                type: "GET",
                dataType: "json",
                success: function(X) {
                    if (!X || !X.totalLength) {
                        W = 0;
                        K.setCount(W);
                        D.setCount(W);
                        S([]);
                        return
                    }
                    var W = X.totalLength || 0;
                    K.setCount(W);
                    D.setCount(W);
                    var V = X.wishlistProductDisplayDTOs.map(function(Y) {
                        return Y.listViewProductOfferGroupDTO
                    });
                    var U = V.map(function(Y) {
                        return Y.id
                    });
                    if (S) {
                        S(V, U)
                    }
                },
                error: function() {
                    if (Q) {
                        Q()
                    }
                }
            })
        }
    };
    var K = {
        seperator: ",",
        get: function() {
            var Q = LocalStorageW.getItem(I.localStorageKey) || "";
            var P = Q.split(this.seperator);
            P = P.filter(function(R) {
                if ($.trim(R)) {
                    return true
                }
            });
            if (P.length > I.maxItems) {
                P.length = I.maxItems
            }
            return P
        },
        setCount: function(P) {
            LocalStorageW.setItem(I.localStorageKey + "-count", P)
        },
        getCount: function() {
            var P = LocalStorageW.getItem(I.localStorageKey + "-count");
            if (parseInt(P) == 0) {
                return 0
            }
            return parseInt(P || K.get().length || 0)
        },
        put: function(S) {
            if (N.disableLocalStorage) {
                return true
            }
            S = (S || "") + "";
            var P = this.get();
            var R = P.map(function(V) {
                return (V || "").split(":")[0]
            });
            var U = S.split(":")[0];
            var Q = R.indexOf("" + U);
            if (Q == -1) {
                P.unshift(S)
            } else {
                P[Q] = S
            }
            var T = false;
            if (P.length > I.maxItems) {
                P.length = I.maxItems;
                T = true;
                if (!I.truncateOnMax) {
                    O.trigger("maxItems")
                }
            }
            if (I.truncateOnMax || !T) {
                LocalStorageW.setItem(I.localStorageKey, P.join(this.seperator))
            }
            return I.truncateOnMax || !T
        },
        putTemp: function(P) {
            LocalStorageW.setItem(I.localStorageTempKey, P)
        },
        addFromTemp: function() {
            var P = LocalStorageW.getItem(I.localStorageTempKey);
            if (P) {
                K.remove(P);
                K.put(P)
            }
            LocalStorageW.setItem(I.localStorageTempKey, "");
            SnapBottomBar.bar && SnapBottomBar.bar.refreshBarVisibility()
        },
        remove: function(Q) {
            var P = this.get();
            var R = P.indexOf(Q);
            if (R != -1) {
                P.splice(R, 1);
                LocalStorageW.setItem(I.localStorageKey, P.join(this.seperator))
            }
        },
        clear: function() {
            LocalStorageW.setItem(I.localStorageKey, "")
        }
    };
    var O = $("<div class='plistContainer'></div>");
    var J = $("#listProductTemplate").html();
    var H = $.trim(I.identifierClass);
    var D = {
        isBuyVisible: true,
        setEmpty: function(Q, P) {
            if (!Q) {
                $(I.attachPointParent).find(".emptyContent").show();
                $(I.attachPoint).find(".container,.otherConts").hide();
                $(I.attachPoint).find(".onVisible").hide()
            } else {
                $(I.attachPointParent).find(".emptyContent").hide();
                $(I.attachPoint).find(".container,.otherConts").show();
                $(I.attachPoint).find(".onVisible").show()
            } if (!P && !Q) {
                D.setCount(0)
            }
        },
        setLoaderVisible: function(P) {
            var Q = $(I.attachPoint).find(".lazyLoader");
            if (Q.length == 0) {
                Q = $("#lazyLoaderTmpl").clone().removeAttr("id").removeClass("hidden").hide();
                $(I.attachPoint).append(Q)
            }
            if ( !! P) {
                return Q.show()
            } else {
                return Q.hide()
            }
        },
        _st: 0,
        render: function(Q) {
            if (D._st == 0) {
                O.html("")
            }

            function R(W) {
                N.refreshCount();
                var Y = D._st;
                var X = W.slice(Y, Y + I.pagesize);
                return X
            }
            var S = function(Z, Y) {
                D.addUsingDataArr(Z, true);
                var X = Z.expectedLength;
                var a = D._st;
                if (Z.length > 0) {
                    if (I.getStrategy == "reco") {
                        D._st = D._st + I.pagesize
                    } else {
                        D._st = D._st + Z.length
                    } if (D.count > a + I.pagesize) {
                        if (Z.length == 0) {
                            D.setLoaderVisible(false)
                        }
                        var W = D.setLoaderVisible(true);
                        W.appear();
                        W.one("appear", function() {
                            D.render(function() {})
                        })
                    } else {
                        D.setLoaderVisible(false)
                    }
                } else {
                    D.setLoaderVisible(false)
                } if (typeof Q == "function") {
                    Q(Z)
                }
            };
            var V = function(X, W) {
                W = W || [];
                N.refreshCount();
                S.apply(this, arguments)
            };
            var P = [];
            switch (I.getStrategy) {
                case "wish":
                    checkAuth(function U() {
                        I.maxItems = SnapBottomBarCfg.max.wish;
                        P = K.get();
                        hgltWishlistProds();
                        M.getDetailsForPOG(R(P), V);
                        O.find(".shortlistLogin").show()
                    }, function T() {
                        I.maxItems = 99999;
                        N.disableLocalStorage = true;
                        M.getWishlistProductsData(D._st, I.pagesize, S)
                    });
                    break;
                case "reco":
                    var P = K.get();
                    M.getRecoProducts(P, D._st, I.pagesize, S);
                    break;
                case "compare":
                    var P = K.get();
                    D.isBuyVisible = false;
                    M.getDetailsForPOG(R(P), V);
                    break;
                default:
                    P = K.get();
                    M.getDetailsForPOG(R(P), V)
            }
        },
        reset: function() {
            D._st = 0;
            N.init()
        },
        addUsingDataArr: function(S, T) {
            var Q = "";
            for (var P = 0; P < S.length; P++) {
                var R = S[P];
                if (R) {
                    Q += D.getProductHtml(R)
                }
            }
            if (T) {
                O.append(Q)
            } else {
                O.prepend(Q)
            }
        },
        count: 0,
        hideCount: function() {
            $(I.countAttachPoint).hide();
            $(I.attachPointParent).find(".bottomFreezeCount").css("visibility", "hidden");
            $(I.attachPointParent).find(".bottomFreezeCount").filter(".bottomFreezeCountInt").parent().hide()
        },
        setCount: function(P) {
            P = P || 0;
            if (P < 0) {
                P = 0
            }
            D.count = P;
            if (P == 0) {
                D.setEmpty(false, true)
            } else {
                D.setEmpty(true, true)
            } if (I.countAttachPoint) {
                $(I.countAttachPoint).html(P).show().css("visibility", "visible")
            } else {
                $(I.attachPointParent).find(".bottomFreezeCount").filter(".bottomFreezeCountInt").parent().show();
                $(I.attachPointParent).find(".bottomFreezeCount").html(P).show().css("visibility", "visible")
            }
        },
        refreshCountWithAnimate: function() {
            var P = $(I.attachPointParent).find(".bottomFreezeCount.circle");
            if (I.disableCountAnimate) {
                N.refreshCount();
                return
            }
            if (I.getStrategy == "reco") {
                $(I.attachPointParent).find(".bottomFreezeIcon").addClass("hover");
                return
            }
            P.addClass("redHighlight").addClass("shadow-animation");
            setTimeout(function() {
                P.removeClass("shadow-animation");
                if (N && N.refreshCount) {
                    N.refreshCount()
                }
            }, 500)
        },
        addForCompare: function(R) {
            var Q = {
                name: R.prodName,
                displayPrice: R.price || 0,
                vendorCode: "",
                id: R.prodId,
                supc: "",
                catId: "",
                identifierClass: H,
                attrJson: "",
                dontShowBuy: true
            };
            var P = D.getProductHtml(Q);
            if (O.find("[pog=" + R.prodId + "]").length == 0) {
                O.prepend(P)
            }
            $(".bottomButtomCompare").show()
        },
        addUsingPogId: function(R, Q) {
            var P = C(R);
            M.getDetailsForPOG(P, function(S) {
                D.addUsingDataArr(S);
                if (Q) {
                    Q()
                }
            })
        },
        getProductHtml: function(R) {
            $.template("listProdTmpl", J);
            var P = "";
            var S = "";
            if (R.offers && R.offers.length == 1 && R.offers[0].supcs.length == 1) {
                P = R.offers[0].supcs[0];
                S = R.offers[0].id
            }
            var Q = (R.initAttributesFull || R.initialAttributes);
            return $("<div></div>").append($.tmpl("listProdTmpl", [$.extend({
                track: "FootID=" + I.getStrategy + "_" + R.categoryUrl + "_",
                pageUrl: R.pageUrl || "#",
                prebook: false || R.prebook,
                prodImage: (Snapdeal.getResourcePath(R.image) || "").replace("166x194", "small"),
                prodTitle: R.name,
                prodPrice: R.displayPrice,
                vendorCode: R.vendorCode,
                pogId: R.id,
                supc: P,
                canDelete: I.canDelete != false,
                isSoldOut: R.soldOut || false,
                catalogid: S,
                catId: R.categoryId,
                catUrl: R.categoryUrl,
                identifierClass: H,
                attrJson: JSON.stringify(Q),
                dontShowBuy: false || !D.isBuyVisible,
                comingSoon: R.productLifeState == "Coming Soon",
                discontinued: R.productLifeState == "Discontinued" || R.productLifeState == "Terminated",
                notAvaillableAtTheMoment: R.productStatus == "inactive" && (R.productLifeState == "Prebook" || R.productLifeState == "Buy Now"),
            }, R)])).html()
        },
        remove: function(Q) {
            var P = C(Q);
            for (var S = 0; S < P.length; S++) {
                var R = O.find("." + H + "[pog=" + P[S] + "]");
                R.fadeOut("fast", function() {
                    R.remove();
                    if (I.getStrategy == "compare") {
                        compareStore.updateCompareButtonLink()
                    }
                })
            }
        }
    };
    $.extend(this, {
        _tmpl: "",
        localStorageKey: null,
        getDomNode: function() {
            return O
        },
        dom: D,
        store: K,
        add: function(Q, P) {
            if (P) {
                K.putTemp(Q);
                return true
            }
            if (D.count < I.maxItems || I.isTruncate) {
                if (K.put(Q) != false) {
                    if (this.initialized) {
                        D.addUsingPogId(Q, function() {
                            if (typeof I.onAdd == "function") {
                                try {
                                    I.onAdd()
                                } catch (S) {}
                            }
                        })
                    } else {
                        if (typeof I.onAdd == "function") {
                            try {
                                I.onAdd()
                            } catch (R) {}
                        }
                    }
                    D.refreshCountWithAnimate();
                    try {
                        SnapBottomBar.httpsHandler.refresh()
                    } catch (R) {}
                    return true
                }
            } else {
                O.trigger("maxItems");
                return "max"
            }
        },
        addFromData: function(P) {
            if (K.put(P.prodId)) {
                D.addForCompare(P);
                if (typeof I.onAdd == "function") {
                    try {
                        I.onAdd()
                    } catch (Q) {}
                }
                D.refreshCountWithAnimate()
            } else {
                return "max"
            }
        },
        refreshCount: function() {
            var Q = I.getStrategy;
            switch (Q) {
                case "reco":
                    D.setCount(I.maxItems);
                    break;
                case "wish":
                    checkAuth(function S() {
                        I.maxItems = SnapBottomBarCfg.max.wish;
                        var T = K.get();
                        D.setCount(T.length)
                    }, function R() {
                        var T = K.getCount() || D.count || 0;
                        N.disableLocalStorage = true;
                        if (T <= 0 && !D.initialized) {
                            D.setCount(0);
                            D.hideCount()
                        } else {
                            D.setCount(T)
                        }
                    });
                    break;
                default:
                    var P = K.get();
                    D.setCount(P.length)
            }
        },
        refreshExternalState: function() {
            var P = I.getStrategy;
            switch (P) {
                case "wish":
                    hgltWishlistProds();
                    break;
                case "compare":
                    if (window.updateCompareState) {
                        updateCompareState()
                    }
            }
        },
        remove: function(Q, P) {
            $(I.attachPointParent).find(".maxItemsMsg").hide();
            K.remove(Q);
            D.remove(Q);
            var S = P != false;
            if (I.onRemove) {
                I.onRemove(Q)
            }
            K.setCount(K.getCount() - 1);
            try {
                SnapBottomBar.httpsHandler.refresh()
            } catch (R) {}
            D.count--;
            if (S) {
                D.refreshCountWithAnimate()
            } else {
                N.refreshCount()
            }
        },
        _: function() {
            K.addFromTemp();
            this.refreshCount();
            switch (I.getStrategy) {
                case "wish":
                    checkAuth(function() {
                        hgltWishlistProds()
                    }, function() {
                        var P = K.get();
                        N.disableLocalStorage = true;
                        if (P.length > 0) {
                            AddToWishlist(P).complete(function() {
                                K.clear();
                                M.getWishlistProductsData(D._st, I.pagesize, callBack)
                            })
                        }
                    })
            }
            lStorageKeys.push(I.localStorageKey);
            lStorageKeys.push(I.localStorageTempKey)
        },
        onMaxItems: function() {
            O.trigger("maxItems")
        },
        initialized: false,
        init: function() {
            N.initialized = true;
            $(I.attachPoint).find(".bottomFreezeOverlay").removeClass("hidden");
            D.render(function() {
                $(I.attachPoint).find(".bottomFreezeOverlay").addClass("hidden");
                if (I.getStrategy == "compare") {
                    compareStore.updateCompareButtonLink()
                }
            });
            $(I.attachPoint).find(".container").append(O)
        }
    });
    $(function() {
        N._();
        var P;
        O.on("maxItems", function() {
            if (I.getStrategy == "wish" && Snapdeal.signupWidConfig.loggedIn) {
                return
            }
            if (I.getStrategy == "compare") {
                $(".bottomFreezeCompareContent .container").scrollTo(0)
            }
            $(I.attachPoint).find(".maxItemsMsg").show();
            SnapBottomBar.bar.selectBar($(I.attachPointParent).index())
        });
        O.on("click", ".closeX", function() {
            var Q = $(this).parent(".btmFreezProd").attr("pog");
            N.remove(Q, false)
        }).on("mousedown", ".somn-track-btmBar", function() {
            var Q = $(this).attr("hidOmnTrack");
            var R = $(this).parents(".btmFreezProd").index();
            Snapdeal.Cookie.set("track", Q + R, 90)
        });
        $(I.attachPointParent).on("click", ".showLoginButton", function() {
            SnapBottomBar.bar.selectBar(-1);
            showLogin();
            Snapdeal.omni.fire("bottomBarShowLogin", {})
        }).on("click", ".shortUparrow", function() {
            $(I.attachPoint).find(".maxItemsMsg").addClass("maxItemsMini")
        });
        if (I.isLazy) {
            $(I.attachPoint).appear();
            $(I.attachPoint).one("appear", function() {
                N.init()
            })
        } else {
            N.init()
        }
    })
};
if (typeof isIframe == "undefined") {
    window.SnapBottomBar = {};
    var lStorageKeys = window.lStorageKeys || [];
    var LocalStorageW = {
        getItem: function(A) {
            return localStorage.getItem(A)
        },
        setItem: function(A, C) {
            if (lStorageKeys.indexOf(A) == -1) {
                lStorageKeys.push(A)
            }
            localStorage.setItem(A, C);
            try {
                SnapBottomBar.httpsHandler.refresh()
            } catch (B) {}
        }
    };
    window.fetcherStore = {
        store: {},
        put: function(B, D, C) {
            var A = this.store;
            if (!A[B]) {
                A[B] = {}
            }
            if (C) {
                this.store[B][D] = C
            }
        },
        putAll: function(B, C) {
            var A = this.store;
            if (!A[B]) {
                A[B] = {}
            }
            $.extend(this.store[B], C)
        },
        putFromArr: function(C, A) {
            var B = this.store;
            if (!B[C]) {
                B[C] = {}
            }
            for (var D = 0; D < A.length; D++) {
                var E = A[D].id;
                if (E) {
                    B[C][E] = A[D]
                }
            }
        },
        get: function(C, E) {
            var B = this.store;
            var A = Snapdeal.Utils.fixArray(E);
            if (!B[C]) {
                B[C] = {}
            }
            var F = {};
            for (var D = 0; D < A.length; D++) {
                F[A[D]] = B[C][A[D]]
            }
            return F
        }
    };
    SnapBottomBar.httpsHandler = {
        refresh: function() {
            var B = {};
            $.each(lStorageKeys, function(D, F) {
                var E = LocalStorageW.getItem(F);
                B[F] = E
            });
            var A = JSON.stringify(B);
            var C = $("#loginIframe").attr("src");
            XD.postMessage("iframeStore#" + A, C, window.frames.iframeLogin)
        },
        canDelete: function() {
            return window.location.port != https
        }
    };
    SnapBottomBar.recentlyList = new ProductList("recently", {
        getStrategy: "recent",
        attachPoint: ".bottomFreezeRecently .itemList",
        isTruncate: true,
        attachPointParent: ".bottomFreezeRecently",
        maxItems: SnapBottomBarCfg.max.recent,
        disableCountAnimate: true,
        canDelete: SnapBottomBar.httpsHandler.canDelete()
    });
    SnapBottomBar.recoList = new ProductList("recommendation", {
        getStrategy: "reco",
        attachPoint: ".bottomFreezeRecommendations .itemList",
        attachPointParent: ".bottomFreezeRecommendations",
        maxItems: SnapBottomBarCfg.max.reco,
        isTruncate: true,
        canDelete: false
    });
    $(window).load(function() {
        SnapBottomBar.wishList = new ProductList("wishlist", {
            truncateOnMax: false,
            getStrategy: "wish",
            attachPoint: ".bottomFreezeShortList .itemList",
            attachPointParent: ".bottomFreezeShortList",
            onRemove: function(C) {
                var A = Snapdeal.signupWidConfig.loggedIn;
                var B = window.selView;
                _removeFromWishlistAndRefresh(C, B);
                if (A) {
                    _removeFromWishlist(C)
                }
            }
        })
    });
    SnapBottomBar.bar = {
        selectBar: function(E, B) {
            var D = $(".bottomFreezeContent");
            var C = $("");
            if (E >= 0) {
                C = D.eq(E)
            }
            var A = true;
            if (B && C.is(":visible")) {
                A = false
            }
            if (A) {
                D = D.not(C);
                C.show();
                C.parents(".bottomFreezeEach").eq(0).addClass("selected");
                C.parents(".bottomFreezeEach").eq(0).find(".bottomFreezeCount.circle").removeClass("redHighlight");
                C.parents(".bottomFreezeEach").eq(0).find(".bottomFreezeIcon").removeClass("hover");
                this.omniture.onOpen(C)
            }
            D.hide();
            D.parents(".bottomFreezeEach").removeClass("selected")
        },
        init: function() {
            var A = window.SnapBottomBarCfg;
            if (window.selectedTab != "hp" && A.guideScreen) {
                this.initGuideScreen()
            }
            this.refreshBarVisibility()
        },
        initGuideScreen: function() {
            var A = "bottomFreezeGuided";
            var B = LocalStorageW.getItem(A);
            if (!B) {
                LocalStorageW.setItem(A, "true", true);
                $(".guidedTourBottom").removeClass("hide");
                $(".guidedTourBottom").click(function() {
                    $(".guidedTourBottom").addClass("hide")
                });
                $(document).one("keyup.guideEscape", function(C) {
                    if (C.keyCode == 27) {
                        $(".guidedTourBottom").addClass("hide")
                    }
                });
                SnapBottomBar.httpsHandler.refresh()
            }
        },
        refreshBarVisibility: function() {
            var D = SnapBottomBar.recentlyList;
            var B = SnapBottomBar.recoList;
            var F = D.store.get().length;
            var E = B.store.get().length;
            var A = !! Snapdeal.Cookie.get("eid");
            var C = window.SnapBottomBarCfg;
            if (F == 0) {
                $(D.cfg.attachPointParent).hide()
            } else {
                $(D.cfg.attachPointParent).show()
            } if (A || E > 0) {
                $(B.cfg.attachPointParent).show()
            } else {
                $(B.cfg.attachPointParent).hide()
            } if (C.state.wish == false) {
                $(".bottomFreezeShortList").hide()
            } else {
                $(".bottomFreezeShortList").show()
            }
        },
        omniture: {
            onOpen: function(B) {
                B = $(B);
                if (B.is(":visible")) {
                    var A = window.omniturePanel;
                    try {
                        var D = "bottomFreeze_tab_" + B.attr("name");
                        A.btmFreeze.tabOpen(D)
                    } catch (C) {}
                }
            }
        }
    };
    SnapBottomBar.bar.init();
    SnapBottomBar.httpsHandler.refresh();
    $(document).ready(function() {
        $(".bottomFreezeActivator").click(function() {
            SnapBottomBar.bar.selectBar($(this).index(".bottomFreezeActivator"), true)
        });
        $(".bottomFreezeContent .close").click(function() {
            $(this).parents(".bottomFreezeContent").hide()
        });
        $(document).bind("keyup.escapeListener", function(A) {
            if (A.keyCode == 27) {
                SnapBottomBar.bar.selectBar(-100)
            }
        })
    });
    var windowIdentifier = Math.random();
    $(window).bind("storage", function(C) {
        var A = C.originalEvent.key;
        var D = C.originalEvent.newValue;
        if (/^plStore/.test(A)) {
            SnapBottomBar.bar.init();
            var B = productListObjs[A];
            if (B) {
                B.refreshCount();
                B.dom.reset();
                B.refreshExternalState()
            }
        }
    });

    function moveItemToDest(E, G, D, H) {
        var H = H || {};
        var I = $(E);
        var B = I.offset();
        var C = I.clone();
        C.css({
            position: "absolute",
            width: I.width(),
            height: I.height(),
            top: B.top,
            left: B.left,
            opacity: 0.7,
            zIndex: 100000
        });
        $(document.body).append(C);
        var G = $(G);
        var A = G.offset();
        var F = H.topOffset || -30;
        C.animate({
            top: A.top + F,
            left: A.left - 10,
            opacity: 0.3,
            width: 50,
            height: 50
        }, 1000, function() {
            C.hide();
            if (D) {
                D()
            }
        })
    }
};
if (typeof isIframe == "undefined") {
    var compareStore = SnapBottomBar.compareStore = {
        seperator: ",",
        localStorageKey: "compareProdKeys",
        get: function() {
            var A = LocalStorageW.getItem(this.localStorageKey) || "";
            return A.split(this.seperator).filter(function(B) {
                if (B) {
                    return true
                }
            })
        },
        put: function(D) {
            var A = this.get();
            var C = A.map(function(F) {
                return (F || "").split(":")[0]
            });
            var E = (D || "").split(":")[0];
            var B = C.indexOf("" + E);
            if (B == -1) {
                A.push(D)
            } else {
                A[B] = D
            }
            LocalStorageW.setItem(this.localStorageKey, A.join(this.seperator))
        },
        remove: function(C) {
            var A = this.get();
            var B = A.map(function(E) {
                return (E || "").split(":")[0]
            });
            var D = B.indexOf(C);
            if (D != -1) {
                A.splice(D, 1);
                LocalStorageW.setItem(this.localStorageKey, A.join(this.seperator))
            }
        },
        removeTab: function(D, C) {
            var A = SnapBottomBar.compareProdMap[C];
            if (A) {
                var B = A.store.get().length;
                if (B == 0) {
                    compareStore.remove(C)
                }
            }
        },
        removePog: function(D, C) {
            var A = SnapBottomBar.compareProdMap[C];
            if (A) {
                A.remove(D);
                var B = A.store.get().length;
                if (B == 0) {
                    compareStore.remove(C);
                    A.store.clear()
                }
            }
        },
        clear: function() {
            LocalStorageW.setItem(this.localStorageKey, "")
        },
        updateCompareButtonLink: function() {
            var A = $(".bottomFreezeCompareContent .itemList");
            A.each(function() {
                var D = this;
                var C = [];
                $(D).find(".btmFreezProd").each(function() {
                    C.push($(this).attr("pog"))
                });
                if (C.length > 1) {
                    var B = A.find(".btmFreezProd").attr("catUrl") || "a";
                    $(D).find(".bottomButtomCompare").removeClass("disableButton");
                    $(D).find(".compareLink").attr("href", Snapdeal.getAbsoluteStaticPath("/product/" + B + "/compare?ids=" + C.join(","))).attr("target", "_blank")
                } else {
                    $(D).find(".bottomButtomCompare").addClass("disableButton");
                    $(D).find(".compareLink").attr("href", "javascript:void(0);").attr("target", "")
                }
            })
        },
        add: function(G, C, B, E) {
            if (!SnapBottomBar.compareProdMap[G] || SnapBottomBar.compareProdMap[G].store.get().length == 0) {
                this.put(G + ":" + escape(C));
                var D = this.get().length - 1;
                this.createTabTitle(G, C);
                this.selectTabById(G);
                var A = $(".bottomFreezeCompare #virginItemList").eq(0).clone().removeAttr("id").addClass("itemList");
                $(".bottomFreezeCompare .itemListWrapper").append(A);
                A.attr("catUrl", B);
                var F = new ProductList("compareList" + G, {
                    getStrategy: "compare",
                    truncateOnMax: false,
                    maxItems: SnapBottomBarCfg.max.compare,
                    attachPointParent: ".bottomFreezeCompare",
                    attachPoint: $(".bottomFreezeCompare .itemList").eq(D),
                    canDelete: SnapBottomBar.httpsHandler.canDelete(),
                    onAdd: function() {
                        compareStore.updateCompareButtonLink()
                    },
                    onRemove: function(I) {
                        var H = $("#prodTypeId").html();
                        compareStore.removeTab(I, H);
                        if (window.updateCompareState) {
                            updateCompareState()
                        }
                        compareStore.updateCompareButtonLink()
                    },
                    countAttachPoint: $(".bottomFreezeCompare .tabHeader").find(".bottomFreezeCount").eq(D).add(".bottomFreezeCompare .bottomFreezeCompareCount")
                });
                A.find(".bottomButtomCompare").show();
                SnapBottomBar.compareProdMap[G] = (F)
            }
            this.selectTabById(G);
            this.refreshTabLeftRightState();
            return SnapBottomBar.compareProdMap[G].add(E.prodId)
        },
        createTabTitle: function(A, B) {
            if ($(".bottomFreezeCompare .tabHeader div[tabId=" + A + "]").length == 0) {
                $(".bottomFreezeCompare .tabHeader").append("<div class='sort-value2' tabId='" + A + "'>" + B + "<span>&nbsp;(<span class='bottomFreezeCount'></span>)</span></div>")
            }
            compareStore.refreshTabLeftRightState()
        },
        createTabs: function() {
            var I = this.get();
            var A = I.length;
            var K = SnapBottomBar.compareProdMap = {};

            function B() {
                if ($(".bottomFreezeCompare .itemListWrapper .itemList").length == 0) {
                    var N = $(".bottomFreezeCompare #virginItemList").eq(0).clone().removeAttr("id").addClass("itemList");
                    $(".bottomFreezeCompare .itemListWrapper").append(N);
                    N.find(".bottomFreezeOverlay").hide();
                    N.find(".bottomButtomCompare").hide()
                }
                $(".bottomFreezeCompare .bottomFreezeCompareCount").html(0).css("visibility", "visible");
                $(".bottomFreezeCompare .textheadings").show();
                $(".bottomFreezeCompare .container").hide();
                $(".bottomFreezeCompare .sortHeader").hide();
                $(".otherConts").hide()
            }
            var J = 0;
            if (A > 0) {
                for (var G = 0; G < A; G++) {
                    var M = I[G].split(":");
                    var L = decodeURIComponent(M[1]),
                        E = M[0];
                    var C = new ProductList("compareList" + E, {
                        countAttachPoint: "#daskldmasskldmaklsd"
                    });
                    var D = C.store.get().length;
                    if (D > 0) {
                        this.createTabTitle(E, L);
                        var H = $(".bottomFreezeCompare #virginItemList").eq(0).clone().removeAttr("id").addClass("itemList");
                        $(".bottomFreezeCompare .itemListWrapper").append(H);
                        var F = $(".bottomFreezeCompare .itemListWrapper > div").eq(J);
                        C = new ProductList("compareList" + E, {
                            getStrategy: "compare",
                            truncateOnMax: false,
                            maxItems: SnapBottomBarCfg.max.compare,
                            attachPointParent: ".bottomFreezeCompare",
                            canDelete: SnapBottomBar.httpsHandler.canDelete(),
                            attachPoint: F,
                            onAdd: function() {
                                compareStore.updateCompareButtonLink()
                            },
                            onRemove: function(O) {
                                var N = $("#prodTypeId").html();
                                compareStore.removeTab(O, N);
                                if (window.updateCompareState) {
                                    updateCompareState()
                                }
                                compareStore.updateCompareButtonLink()
                            },
                            countAttachPoint: $(".bottomFreezeCompare .tabHeader").find(".bottomFreezeCount").eq(J).add(".bottomFreezeCompare .bottomFreezeCompareCount")
                        });
                        K[E] = (C);
                        J++
                    }
                }
                if (J == 0) {
                    B()
                } else {
                    $(".bottomFreezeCompare .textheadings").hide();
                    $(".bottomFreezeCompare .sortHeader").show();
                    this.refreshTabLeftRightState()
                }
            } else {
                B()
            }
            this.refreshTabLeftRightState()
        },
        refreshTabLeftRightState: function() {
            var C = $(".bottomFreezeCompare .tabWrapper");
            var A = 0;
            C.find(".tabHeader > *").each(function(D, E) {
                A += $(E).outerWidth()
            });
            var B = C.outerWidth();
            if (B >= A) {
                C.parent().find(".tabheaderLeft , .tabheaderRight").hide();
                this.inTabbedState = false
            } else {
                C.parent().find(".tabheaderLeft , .tabheaderRight").show();
                this.inTabbedState = true
            }
            this.focusSelectedIndex()
        },
        focusSelectedIndex: function() {
            if (!this.inTabbedState) {
                return false
            }
            var A = this.selectedIndex || 0;
            var B = 0;
            $(".tabHeader > div").each(function(C, D) {
                if (C >= A) {
                    return
                }
                B += $(this).width()
            });
            B = -1 * B + ($(".tabHeader > div").eq(A).width() / 2);
            if (A == 0) {
                B = 0
            }
            $(".tabHeader").css("left", B)
        },
        selectTab: function(B, E) {
            var D = $(".sort-value2").eq(B).attr("tabId");
            var A = SnapBottomBar.compareProdMap[D];
            var C = $(".bottomFreezeCompare .itemListWrapper > .itemList");
            if (B >= 0 && B < C.length) {
                if (A) {
                    A.refreshCount()
                }
                $(".sort-value2").removeClass("popbox");
                $(".sort-value2").eq(B).addClass("popbox");
                C.hide().eq(B).show()
            }
            this.selectedIndex = B;
            this.focusSelectedIndex();
            if (!E) {
                if (C.eq(B).length > 0) {
                    $(".bottomFreezeCompare .textheadings").hide();
                    $(".bottomFreezeCompare .sortHeader").show()
                }
            }
        },
        selectTabById: function(B) {
            var A = $(".sort-value2[tabId=" + B + "]").index();
            if (A >= 0) {
                this.selectTab(A)
            }
        },
        refreshFromStore: function() {
            this.createTabs()
        },
        init: function() {
            this.createTabs();
            this.initTabEvents();
            this.selectTab(0, true);
            lStorageKeys.push(this.localStorageKey)
        },
        initTabEvents: function() {
            $(".bottomFreezeCompare .itemList").appear();
            $(".sort-value2").live("click", function() {
                SnapBottomBar.compareStore.selectTab($(this).index())
            });
            this.setupTabScrolling()
        },
        setupTabScrolling: function() {
            var A;

            function B(G, D) {
                clearInterval(A);
                var C = 0;
                G.children().each(function(H, I) {
                    C += $(I).width()
                });
                var F = G.parent().width();
                var E = C - F;
                if (D != 0) {
                    A = setInterval(function() {
                        var I = parseInt(G.css("left")) || 0;
                        var H = I + D;
                        if (H <= 0 && H > -1 * C) {
                            G.css("left", H)
                        }
                    }, 4)
                }
            }
            $(".tabheaderLeft").live("mouseenter", function() {
                var C = $(this).parent().find(".tabWrapper .tabHeader");
                B(C, 1)
            }).live("mouseleave", function() {
                var C = $(this).parent().find(".tabWrapper .tabHeader");
                B(C, 0)
            });
            $(".tabheaderRight").live("mouseenter", function() {
                var C = $(this).parent().find(".tabWrapper .tabHeader");
                B(C, -1)
            }).live("mouseleave", function() {
                var C = $(this).parent().find(".tabWrapper .tabHeader");
                B(C, 0)
            })
        }
    };
    compareStore.init();
    SnapBottomBar.httpsHandler.refresh();
    $(".bottomFreezeCompareContent").one("appear", function() {
        compareStore.refreshTabLeftRightState()
    })
};
$(".miniBuy").live("click", function() {
    $("#frequently-popup-button-id").removeClass("floatBuy");
    $("#frequently-popup-button-id").addClass("miniBuy");
    var D = $(this).attr("supc");
    var B = $(this).attr("catalogid");
    var C = $(this).attr("vc") || $(this).attr("vendorcode");
    var A = $(this).attr("name");
    var F = $(this).attr("hidOmnTrack");
    Snapdeal.Cookie.set("track", F);
    if (D && B && C) {
        window.cartService.addToCart(B, C, D).complete(function() {
            $(".bottomFreezeContent").hide()
        });
        $("#frequently-bought").hide();
        window.cartService.fireOmnitureScript("addToCart", B, true, false, C)
    } else {
        var E = $.parseJSON($(this).parents(".miniBuyWrapper").find(".attributeJson").html());
        selectAttributesForCart(E, A, C)
    }
});
$(".floatBuy").live("click", function() {
    var D = $(this).attr("supc");
    var B = $(this).attr("catalogid");
    var C = $(this).attr("vc") || $(this).attr("vendorcode");
    var A = $(this).attr("name");
    var F = $(this).attr("hidOmnTrack");
    Snapdeal.Cookie.set("track", F);
    if (D && B && C) {
        window.cartService.addToCart(B, C, D).complete(function() {
            $(".bottomFreezeContent").hide()
        });
        $("#frequently-bought").hide();
        window.cartService.fireOmnitureScript("addToCart", B, true, false, C, $("#fltBuyBtn").attr("vendorScore"), "BuyButton_Floating", $("#fltBuyBtn").attr("shippingCharges"))
    } else {
        var E = $.parseJSON($(this).parents(".miniBuyWrapper").find(".attributeJson").html());
        selectAttributesForCart(E, A, C)
    }
});
$("#freq-popup-select-id-0").live("change", function() {
    var A = $("#freq-popup-select-id-0 option:selected");
    $("#frequently-popup-button-id").attr("supc", $(A).attr("supc"));
    $("#frequently-popup-button-id").attr("catalogId", $(A).attr("catalogId"));
    $("#frequently-popup-button-id").attr("vendorCode", $(A).attr("vendorCode"))
});
var freqDepth;

function selectAttributesForCart(J, U, I) {
    var M = Snapdeal.Utils.fixArray;
    var F = J;
    var S = 0;
    var C = F || [];
    var N = C;
    while (N && N.length > 0) {
        S++;
        N = N[0].subAttributes
    }
    freqDepth = S;
    var Q = true;
    var A = true;
    if ($.isArray(F)) {
        Q = !F.some(function(V) {
            return (V.subAttributes || []).some(function(W) {
                return W.live && !W.soldOut
            })
        });
        A = F.every(function(V) {
            return (V.subAttributes || []).every(function(W) {
                return W.live && !W.soldOut
            })
        });
        if (Q) {} else {
            if (!A && !Q) {} else {}
        }
        $("#freq-bought-popup-top").html("Select ");
        $("#freq-bought-popup-top").show();
        $("#freq-popup-button-id").show();
        $("#frequently-popup-requirement").html(U);
        $("#notifyme-top").hide();
        $("#notifyMe-popup-button-id").hide();
        $("#frequently-attributes-outer").html("");
        var T = "";
        for (var P = 0; P < S; P++) {
            T = "";
            T += '<div class="overhid">';
            T += '   <div id="freq-popup-attribute' + P + '" class="notifyMe-popup-fieldsText">Select </div>';
            T += '   <div class="notifyMe-popup-fields"><div class="notifyMenDropdown">';
            T += '       <select id="freq-popup-select-id-' + P + '" class="notifyMe-popup-select-2 " >';
            T += "           <option>--</option></div>";
            T += "       </select>";
            T += "   </div>";
            T += "</div>";
            $("#frequently-attributes-outer").append(T)
        }
        var R = F;
        if (S == 1) {
            var U = R[0].name;
            var B = $("#freq-bought-popup-top").html() + U;
            $("#freq-bought-popup-top").html(B);
            $("#freq-popup-attribute0").append(U);
            for (var P = 0; P < R.length; P++) {
                if (!R[P].soldOut && R[P].live) {
                    $("#freq-popup-select-id-0").append('<option value="' + R[P].value + '" supc="' + R[P].supc + '" catalogId="' + R[P].catalogId + '" vendorCode="' + I + '" pogId="' + R[P].id + '">' + R[P].value + "</option>")
                }
            }
            $("#freq-popup-select-id-0").off("change").on("change", function() {
                var V = $("#freq-popup-select-id-0 option:selected");
                $("#frequently-popup-button-id").attr("supc", $(V).attr("supc")).attr("catalogId", $(V).attr("catalogId")).attr("vendorCode", $(V).attr("vendorCode"))
            })
        } else {
            var E = 1;
            for (var P = 0; P < S; P++) {
                if (P > 0) {
                    C = C[0].subAttributes
                }
                subattributes = C;
                var U = subattributes[0].name;
                $("#freq-popup-attribute" + P + "").append(U);
                var B = $("#freq-bought-popup-top").html() + U;
                $("#freq-bought-popup-top").html(B);
                $("#freq-bought-popup-vendorCode").html(I);
                if (E == 1) {
                    $("#freq-bought-popup-top").append(", ");
                    E++
                }
            }
            for (var P = 0; P < R.length; P++) {
                var G = R[P];
                var D = false;
                D = G.subAttributes.some(function(V) {
                    return V.live && !V.soldOut
                });
                if (D) {
                    var O = JSON.stringify(R[P]);
                    var K = new Option(O, O);
                    $(K).html(R[P].value);
                    $("#freq-popup-select-id-0").append(K)
                }
            }
        } if (Q) {
            $(".notifyMe-email-outer").hide();
            var H = "hi";
            if (S == 1) {} else {}
            $("#notifyMe-popup-requirement").html(H);
            $("#notifyMe-partialSold-requirement").html(" with " + H)
        } else {
            if (!A && !Q) {
                $(".notifyMe-partialSoldOut, .vertSep-sizechart").show();
                var H;
                if (S == 1) {} else {}
                $("#notifyMe-popup-requirement").html(H);
                $("#notifyMe-partialSold-requirement").html(" with " + H)
            } else {
                $("#notifyMe-wrapper").hide();
                $(".notifyMe-partialSoldOut, .vertSep-sizechart").hide()
            }
        }
        $("#notifyMe-popup-button-id").click(function() {
            var X;
            if (S == 1) {
                X = document.getElementById("freq-popup-select-id-0")
            }
            if (S > 1) {
                X = document.getElementById("freq-popup-select-id-1")
            }
            var W = X.selectedIndex;
            var Y = X.options[W].value;
            Y = jQuery.trim(Y);
            var V = $(".notifyMe-popup-email").val();
            if (validateEmail(V)) {
                if (Y != "--") {
                    launchNotifyMeRequest(Y, V);
                    $(".notifyMe-popup-invalidEmail").hide();
                    $(".notifyMe-attribute-error-message").hide()
                }
                if (Y == "--") {
                    if (Y == "--") {
                        $(".notifyMe-attribute-error-message").html("Please select" + attrStr() + " of Product").show()
                    }
                }
                $(".notifyMe-popup-invalidEmail").hide()
            } else {
                if (!validateEmail(V)) {
                    $(".notifyMe-popup-invalidEmail").show()
                }
                if (Y == "--") {
                    $(".notifyMe-attribute-error-message").show()
                }
            }
        });
        $("#frequentlyboughtClose").click(function() {
            $("#frequently-bought").hide()
        });
        var L = "";
        if (L != 0) {
            $("#notifyMe-popup-email").val(L);
            if ($("#notifyMe-email") != undefined) {
                $("#notifyMe-email").val(L)
            }
        }
        $(".notifyMe-popup-divtwo").hide();
        $(".notifyMe-popup-divfour").hide();
        $("#frequently-bought").show()
    }
}
$("#freq-popup-select-id-0").live("change", function() {
    if ($("#freq-popup-select-id-0").val() == "--") {
        $("#freq-popup-select-id-1").html("<option> --</option>")
    } else {
        if (freqDepth == 1) {
            $(".notifyMe-attribute-error-message").hide()
        }
        if (freqDepth > 1) {
            var D = $("#freq-popup-select-id-0").val();
            var B = JSON.parse(D);
            var A = $("#freq-bought-popup-vendorCode").html();
            $("#freq-popup-select-id-1").html("<option> --</option>");
            $("#freq-popup-select-id-1").removeClass("disabled");
            for (var C = 0; C < B.subAttributes.length; C++) {
                if (!B.subAttributes[C].soldOut && B.subAttributes[C].live) {
                    var E = new Option(B.subAttributes[C].catalogId, B.subAttributes[C].catalogId);
                    $(E).html(B.subAttributes[C].value);
                    $("#freq-popup-select-id-1").append('<option value="' + B.subAttributes[C].value + '" vendorCode="' + A + '" supc="' + B.subAttributes[C].supc + '" catalogId="' + B.subAttributes[C].catalogId + '" >' + B.subAttributes[C].value + "</option>")
                }
            }
            $("#freq-popup-select-id-1").off("change").on("change", function() {
                var F = $("#freq-popup-select-id-1 option:selected");
                $("#frequently-popup-button-id").attr("supc", $(F).attr("supc")).attr("catalogId", $(F).attr("catalogId")).attr("vendorCode", $(F).attr("vendorCode"))
            })
        }
    }
});

var pogIds = new Array();
var subCat = new Array();
var recViewed;
$(function() {
    var A = window.SnapBottomBarCfg;
    if (A && A.state && A.state.reco) {
        var C = Snapdeal.Cookie.get("hprv");
        if (!C) {
            return
        }
        var B = C.split("|");
        $.each(B, function(D, E) {
            var F = E.split("-");
            SnapBottomBar.recoList.store.put(F[1] + ":" + F[0]);
            SnapBottomBar.recoList.refreshCount()
        })
    }
});

function homePageRecentlyViewWidget() {
    if (Snapdeal.componentSwitches.recSysClosed == "false") {
        var D = "";
        var A = SnapBottomBar.recoList.store.get();
        jQuery.each(A, function(M, N) {
            if (M <= 4) {
                var L = N.split(":");
                D += (L[1] + "-" + L[0] + "|")
            } else {
                return
            }
        });
        var G = D.substr(0, D.length - 1);
        if (!G) {
            return
        }
        var J = Snapdeal.getAjaxResourcesPath("/ghprvp?ids=");
        var E = G.split("|");
        var B = new Array();
        var H = new Array();
        var C = new Array();
        var K = 0;
        var I = 0;
        for (var F = 0; F < (E.length) - 1; F++) {
            B = E[F].split("-");
            J = J + B[0] + ",";
            pogIds[F] = B[0];
            subCat[F] = B[1];
            I = F
        }
        if (I == 0) {
            return false
        }
        B = E[I + 1].split("-");
        J = J + B[0];
        pogIds[(E.length) - 1] = B[0];
        subCat[(E.length) - 1] = B[1];
        if ((E.length) >= 2) {
            $.ajax({
                url: J,
                type: "GET",
                dataType: "html",
                success: function(L) {
                    $("#recViewedProductOuter").html(L);
                    $.ajax({
                        url: Snapdeal.getAjaxResourcesPath("/ghpsp/" + pogIds[0] + "/" + subCat[0]),
                        type: "GET",
                        dataType: "html",
                        success: function(N) {
                            $("#recViewedSimilarProductsOuter").html(N);
                            var M = 0;
                            M = $("#homepageCounter").html();
                            if (M > 3) {
                                $(".productSlideWrapper").bxSlider({
                                    nextText: "",
                                    prevText: "",
                                    auto: false,
                                    pager: false,
                                    infiniteLoop: true,
                                    hideControlOnEnd: true,
                                })
                            }
                            $(" .details-similar").bind("mousedown", function() {
                                Snapdeal.Cookie.set("track", $(this).attr("hidOmnTrack"), 90)
                            });
                            ratingStars();
                            $(document).trigger("loadLazyImages")
                        }
                    });
                    $("#homePageRecentlyViewed").css("display", "block");
                    $("#product-1").addClass("rec-viewed-arrow")
                }
            })
        }
    }
}

function getSimilar(A) {
    var B = 0;
    for (i = 1; i < pogIds.length + 1; i++) {
        $("#product-" + i).removeClass("rec-viewed-arrow")
    }
    $("#product-" + A).addClass("rec-viewed-arrow");
    $.ajax({
        url: Snapdeal.getAjaxResourcesPath("/ghpsp/" + pogIds[A - 1] + "/" + subCat[A - 1]),
        type: "GET",
        dataType: "html",
        success: function(C) {
            $("#recViewedSimilarProductsOuter").html(C);
            var D = 0;
            D = $("#homepageCounter").html();
            if (D > 3) {
                $(".productSlideWrapper").bxSlider({
                    nextText: "",
                    prevText: "",
                    auto: false,
                    pager: false,
                    infiniteLoop: true,
                    hideControlOnEnd: true,
                })
            }
            ratingStars();
            $(document).trigger("loadLazyImages")
        }
    })
};
(function() {
    var A = {
        labelHashN: "bcrumbLabelId",
        searchHashN: "bcrumbSearch",
        render: function() {
            $("#breadCrumbWrapper2").addClass("notVisible");
            var E = $.Deferred();
            var B = Snapdeal.hashManager.getFromHash(A.labelHashN);
            var D = Snapdeal.hashManager.getFromHash(A.searchHashN);
            if (D) {
                D = "Search: " + D
            }
            var G = $("#breadCrumbItemTmpl").html();
            $.template("breadCrumbItemTmpl", G);
            var C = ($("#breadCrumbLabelIds").text()).split(",");
            if (C.indexOf(B) != -1) {
                B = null
            }
            if (B || D) {
                windowLoaded.done(function() {
                    A.getParents(B, D).done(function(H) {
                        F(H)
                    })
                })
            } else {
                F()
            }

            function F(I) {
                if (I) {
                    I.reverse();
                    var H = $.tmpl("breadCrumbItemTmpl", [{
                        data: I
                    }]);
                    $("#breadCrumbWrapper2").html(H)
                }
                $("#breadCrumbWrapper").removeClass("notVisible");
                $("#breadCrumbWrapper2").removeClass("notVisible");
                E.resolve();
                if (window.pdpType && (B || D)) {
                    initBread()
                }
            }
            return E
        },
        initEvents: function() {
            var B = $("#breadCrumbDropdownItemTmpl").html();
            $.template("breadCrumbListTmpl", B);
            $(".breadcrumbArrow-down").live("hover", function() {
                var H = $(this).parents(".containerBreadcrumb").eq(0);
                if (!$(this).attr("loaded")) {
                    $(this).attr("loaded", true);
                    var D = $(this).attr("prevLink") || "";
                    var C = $(this).attr("prevName") || "";
                    var E = Snapdeal.hashManager.getFromHash("bcrumbSearch");
                    var G = D.split("#")[0].split("/");
                    var K = G[G.length - 1];
                    var J = window.pageName2 || window.pageName;
                    var F = {
                        q: "",
                        keyword: E
                    };
                    var L = false;
                    var I;
                    switch (J) {
                        case "brandCategoryPage":
                        case "brandPage":
                            F.q += "Brand:" + window.brandName;
                            if (window.brandName == C) {
                                K = ""
                            }
                            type = "brand";
                            L = true;
                            var I = function(M) {
                                if (M && M.length > 0) {
                                    M.map(function(N) {
                                        N.url = "/brand/" + window.brandName.toLowerCase() + "/" + N.url
                                    })
                                }
                            };
                            break
                    }
                    A.getSubCategories(K, F).success(function(N) {
                        N = $.parseJSON(N);
                        if (typeof I == "function") {
                            I(N)
                        }
                        if (N && N.length > 0) {
                            if (J == "brandCategoryPage") {
                                if (K != "" && N > 0) {
                                    var M = $.tmpl("breadCrumbListTmpl", [{
                                        data: N,
                                        isAbs: L
                                    }])
                                } else {
                                    var M = $.tmpl("breadCrumbListTmpl", [{
                                        data: N,
                                        isAbs: L
                                    }])
                                }
                            } else {
                                if (!E && N > 0) {
                                    var M = $.tmpl("breadCrumbListTmpl", [{
                                        data: N,
                                        isAbs: L
                                    }])
                                } else {
                                    var M = $.tmpl("breadCrumbListTmpl", [{
                                        data: N,
                                        isAbs: L
                                    }])
                                }
                            }
                        } else {
                            var M = "<li class='ClsNotFound'>Not Available</li>"
                        }
                        H.find("ul.breadcrumbinnerleft").html(M);
                        if (N.length > 12) {
                            initSdScroll()
                        }
                    })
                }
            })
        },
        getSubCategories: function(B, D) {
            D = D || {};
            var C = $.extend({
                labelUrl: B
            }, D);
            return $.ajax({
                url: "/acors/get/json/subLeftNav",
                method: "get",
                data: C
            })
        },
        getParents: function(G, B) {
            var E = $.Deferred();
            var F = B;
            if (B) {
                var D = B.split(": ");
                if (D.length > 1) {
                    F = D[1]
                } else {
                    F = D[0]
                }
            }
            var C = [];
            if (window.pdpType) {
                C.push({
                    name: $(".pdpName > h1, .productTitle > h1").text(),
                    url: "",
                    isAbs: true,
                    hasSub: false
                });
                if (!G) {
                    if (B) {
                        C.push({
                            name: B,
                            url: "/search?keyword=" + escape(F) + "&noOfResults=20",
                            isAbs: true,
                            hasSub: false
                        })
                    }
                    E.resolve(C)
                }
            }
            $.ajax({
                url: "/acors/get/json/breadcrumbs",
                method: "get",
                data: {
                    label: ((window.ssVars && window.ssVars.labelId) ? ssVars.labelId : G)
                },
                success: function(I) {
                    I = $.parseJSON(I);
                    if (I && I.length > 0) {
                        var H = I.map(function(J) {
                            return {
                                name: J.displayName,
                                url: J.url,
                                isAbs: false,
                                hasSub: true
                            }
                        });
                        C.push.apply(C, H);
                        if (B) {
                            C.push({
                                name: B,
                                url: "/search?keyword=" + escape(F) + "&noOfResults=20",
                                isAbs: true,
                                hasSub: false
                            })
                        }
                        E.resolve(C)
                    } else {
                        E.resolve()
                    }
                }
            });
            return E
        },
        addHashToAllInternalLinks: function() {
            if (A.getHashValueForPage()) {
                $("#topsearches a").addClass("hashAdded");
                $("a:not(.hashAdded)").attr("href", function(C, B) {
                    if (window.location.hostname === this.hostname && this.pathname != "/") {
                        var D = this.hash;
                        return B + "#" + A.getHashValueForPage(D)
                    }
                }).addClass("hashAdded")
            }
        },
        getHashValueForPage: function(E) {
            var C = window.pageName2 || window.pageName;
            var B = E;
            switch (C) {
                case "search":
                    var F = $("#santizedKeyword").val();
                    var D = A.searchHashN;
                    B = Snapdeal.hashManager._putToHash(D, F, B);
                case "productDeals":
                    if (window.ssVars && window.ssVars.labelId) {
                        var F = ssVars.labelId;
                        var D = A.labelHashN;
                        B = Snapdeal.hashManager._putToHash(D, F, B)
                    }
                    break
            }
            return B
        }
    };
    window.Snapdeal = window.Snapdeal || {};
    Snapdeal.breadcrumb = A;
    A.render().done(function() {
        A.initEvents()
    });
    windowLoaded.done(function() {
        A.addHashToAllInternalLinks()
    })
})();
$(".tracking").live("mouseover", function() {
    Snapdeal.Cookie.set("track", $(this).attr("track"), 90)
});

function track(B, A) {
    $(B).addClass("somn-track");
    $(B).attr("hidOmnTrack", A)
}
$(".somn-track").live("mousedown", function() {
    var A = $(this).attr("hidOmnTrack");
    logTrack("track", A);
    Snapdeal.Cookie.set("track", A, 90)
});
$(".omnTrack").mousedown(function() {
    Snapdeal.Cookie.set("track", $(this).attr("omnValue"), 90)
});
$(".bCrumbOmniTrack").live("click", function() {
    Snapdeal.Cookie.set("track", $(this).attr("omniID"), 90)
});
$(".ebayClick").live("mousedown", function() {
    var A = $(this).attr("name");
    updateOmniture("ebayPartnerResults", "name", A)
});
$(".recommendViewCookie").live("mousedown", function() {
    Snapdeal.Cookie.set("track", "HID=recoForYou_ViewAll", 90)
});

function updateOmniture(A, D, C) {
    if (window.loggingOmnitureEnabled == "true") {
        logTrack(A, D, C)
    }
    var B = "/omn/getOmnitureCode?eventType=" + A + "&param=" + D + "&value=" + C;
    $.ajax({
        url: B,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        success: function(E) {
            $("body").append(E)
        },
        error: function() {
            $(".ajax-loader-icon").hide()
        }
    })
}
if (typeof Snapdeal == "undefined") {
    Snapdeal = {}
}
Snapdeal.omni = {};
Snapdeal.omni.fire = function(A, B) {
    logTrack(A, B);
    return $.ajax({
        url: "/omn/getOmnitureCode?eventType=" + A + "&" + $.param(B),
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        success: function(C) {
            $("body").append(C)
        },
        error: function() {
            $(".ajax-loader-icon").hide()
        }
    })
};

function logTrack(B, C, E) {
    if (window.loggingOmnitureEnabled == "true") {
        var A = $("#logServiceUrl").val();
        if (A != "" && A != undefined && A != null) {
            var D = A + "/lg/trackEvent?uid=" + Snapdeal.Cookie.get("u") + "&event=" + B;
            if (E != undefined) {
                D = D + "&" + C + "=" + E
            } else {
                params = $.param(C);
                if (B == "track") {
                    params = C
                }
                D = D + "&" + params
            }
            $.ajax({
                url: D,
                type: "GET",
                jsonpCallback: "jsonCallback",
                dataType: "jsonp",
                success: function(F) {},
                error: function() {}
            })
        }
    } else {
        return true
    }
}

function getOmnitureScript(B, A) {
    return Snapdeal.omni.fire(A, B)
};
var loginValue = true;
var signupValue = false;
var passValue = false;
var signupJson = null;
Snapdeal.signupWidget = function() {
    var C = Snapdeal.getStaticPath("/login_security_check?");
    var E = Snapdeal.getStaticPath("/signupAjax");
    var B = Snapdeal.getStaticPath("/info/terms");
    var D = null;
    var A = null;
    this.init = function(F) {
        signupJson = F;
        if (signupJson != null && signupJson.targetUrl) {}
        D = encodeURI(C);
        A = {};
        A.promo = F.registerLoginDTOs[0];
        A.login = F.registerLoginDTOs[1]
    };
    this.postAppend = function() {
        this.phoneNumberFormValidate();
        this.loginFormValidate();
        var F = Snapdeal.signupWidConfig;
        if (F.activeTab != 0 && !F.activeTab) {
            F.activeTab = 1
        }
        if (F.iframeActiveTab) {
            F.activeTab = F.iframeActiveTab
        }
        setActiveTab(F.activeTab);
        F.activeTab = null;
        $("#logInByEmailBtn").click(function() {
            $(".logInByEmailDiv").hide();
            $(".logInFormWrapper").show()
        })
    };
    this.getHtml = function(K) {
        if (K == "quickBuy") {
            var F = $("#loginWidgetTmpl").html()
        } else {
            var F = $("#signupWidgetTmpl").html()
        }
        $.template("signupWidget", F);
        var H = this;
        var J = httpPath;
        if (typeof isIframe != "undefined") {
            var L = parentLoc;
            var G = L.split(/\//);
            L = G[0] + "//" + G[2];
            J = L || J
        }
        var I = httpPath;
        return $.tmpl("signupWidget", [$.extend({
            fbPath: getHttpsPathStart(),
            img: Snapdeal.getStaticPath,
            hostPath: J,
            googlePath: I,
            encodedLoginPath: D,
            fbAppId: signupJson.fbAppId,
            fbPermissions: fbPermissions
        }, A)])
    };
    this.getSucc = function(F) {
        $(".loginParts").hide();
        $("#forgotPwdSuccess").show();
        $("#forgotPwdSuccess .idmail").html(F)
    };
    this.loginFormValidate = function() {
        var F = $("#j_username").val();
        $("#j_username").attr("value", F);
        $("#ajaxSignin").validate({
            rules: {
                j_username: {
                    required: true,
                    email: true
                },
                j_password: {
                    required: true
                }
            },
            messages: {
                j_username: "Please enter a valid email address",
                j_password: {
                    required: "Please provide a password"
                }
            },
            submitHandler: function(G) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: C,
                    data: $(G).serialize(),
                    beforeSend: function(H) {
                        H.withCredentials = true
                    },
                    success: function(H) {
                        return window.widget.loginCallback(H)
                    }
                })
            }
        })
    };
    this.loginCallback = function(G) {
        if (G.status == "success") {
            var F = window.location.hash.replace(/^#/, "");
            if (G.items != undefined && G.items.targetUrl != null && G.items.targetUrl != "") {
                F = G.items.targetUrl
            }
            $("#ajaxSignin").clearForm();
            $("#signin_box").hide();
            $(".signupopupbox").hide();
            if (F.indexOf("?") == -1) {
                F += "?"
            } else {
                F += "&"
            } if (self == top) {
                window.location.href = F + "loginSuccess=success"
            } else {
                XD.postMessage("loginAndRefresh", parentLoc)
            }
        } else {
            $("#invalidPassword").html(G.message).show()
        }
    };
    this.signupFormValidate = function() {
        var F = $("#j_username_new").val();
        $("#j_username_new").attr("value", F);
        $("#ajaxSignup").validate({
            rules: {
                j_username: {
                    required: true,
                    email: true
                },
                j_password: {
                    required: true,
                    minlength: 6
                },
                j_confpassword: {
                    required: true,
                    minlength: 6,
                    equalTo: "#j_password"
                },
                j_gender: {
                    required: true
                },
                agree: "required"
            },
            messages: {
                j_username: "Please enter a valid email address",
                j_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long"
                },
                j_confpassword: {
                    required: "Please confirm your password",
                    minlength: "Your password must be at least 6 characters long",
                    equalTo: "Please enter the same password as above"
                },
                j_gender: {
                    required: "Please select gender"
                },
                agree: "Please accept our policy"
            },
            submitHandler: function(G) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    data: $(G).serialize() + "&" + $("#phoneNumberForm").serialize(),
                    url: E,
                    success: function(H) {
                        return window.widget.signupCallback(H)
                    }
                })
            }
        })
    };
    signupCallback = this.signupCallback = function(H) {
        var G = window.location.href;
        if (H.status == "success") {
            var F = $("#j_username_new").val();
            var I = window.location.hash.split("#").join("");
            if (!H.hasPromo) {
                XD.postMessage("showMessage:" + F, I)
            } else {
                XD.postMessage("showPromoMessage:" + F, I)
            } if ($("#viaCheckout").val() == "true") {
                $("#system_message").show();
                $("#ajaxSignup").clearForm();
                omnitureOnCheckout("checkoutSignup&emailId=" + F)
            }
        } else {
            $("#ajaxSignupResponse").html(H.message)
        }
    };
    this.phoneNumberFormValidate = function() {
        $("#phoneNumberForm").validate({
            rules: {
                j_number: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 10
                }
            },
            messages: {
                j_number: "Please enter a valid mobile number"
            },
            submitHandler: function(F) {
                $.ajax({
                    url: "/ajaxRegister",
                    type: "POST",
                    data: $("#phoneNumberForm").serialize(),
                    dataType: "json",
                    success: function(G) {
                        if (G.status == "success") {
                            try {
                                $("#ajaxSignupResponse").html("")
                            } catch (H) {}
                        } else {
                            var I = "Promo codes unavailable as mobile no. already exists";
                            $("#ajaxSignupResponse").html(I)
                        }
                        createAccount(true)
                    }
                })
            }
        });
        $("#forgotPasswordNavigation").validate({
            rules: {
                fp_email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                fp_email: "Please enter a valid email address"
            },
            submitHandler: function(H) {
                var G = "/forgotPassword?";
                G += "email=" + $("#fp_email").val();
                var F = $("#fp_email").val();
                $.ajax({
                    url: G,
                    type: "POST",
                    dataType: "json",
                    success: function(I) {
                        if (I.status == "success") {
                            $("#tab3").remove();
                            if (I.items["invalidContent"] == false) {
                                $(".signupopupbox").append(window.widget.getSucc(F))
                            }
                        } else {
                            var J = "This email id is not registered with us.";
                            $("#forgotPasswordNavigation .error").html(J).show();
                            if (I.items["invalidContent"] == false) {
                                $("#fp_email").attr("value", F)
                            }
                        }
                    }
                })
            }
        })
    };
    this.rpxLogin = function(F) {
        loginWindow = window.open(F, "OAuthLogin", "menubar=yes,resizable=yes,scrollbars=yes,width=600,height=450,left=150,top=150");
        return false
    };
    this.fbLogin = function(F) {
        loginWindow = window.open(F, "OAuthLogin", "menubar=yes,resizable=yes,scrollbars=yes,width=850,height=550,left=100,top=100");
        return false
    };
    this.loadLogin = function() {
        var F = $("#targetUrl").html() ? "/ajaxLogin?targetUrl=" + $("#targetUrl").html() : "/ajaxLogin";
        $.ajax({
            url: F,
            type: "POST",
            dataType: "json",
            success: function(G) {
                window.widget.init(G);
                $("#signin_box").html(window.widget.getHtml());
                $(".signupopupbox").show("fast");
                $(".signin").toggleClass("menu-open", true)
            }
        });
        return false
    };
    this.loadSignup = function() {
        var F = $("#targetUrl").html() ? "/ajaxSignup?targetUrl=" + $("#targetUrl").html() : "/ajaxSignup";
        $.ajax({
            url: F,
            type: "POST",
            dataType: "json",
            success: function(G) {
                $("#signin_box").hide();
                $(".signupopupbox").hide("fast");
                window.widget.init(G);
                $("#signin_box").html(window.widget.getHtml());
                $(".signupopupbox").show("fast");
                $(".signin").toggleClass("menu-open", true)
            }
        });
        return false
    };
    this.loadForgotPass = function() {
        $(".toggle_container").show();
        return false
    };
    this.forgotPassword = function() {
        return false
    };
    this.checkPrevSource = function() {
        var F = "/checkSource?";
        F += "email=" + $("#j_username").val() + "&cs=" + $("#CSRFToken").val();
        $.ajax({
            url: F,
            type: "POST",
            dataType: "json",
            success: function(G) {
                if (G != null && G.status == "success") {
                    $("#pSource").html(G.message);
                    $("#ajaxSigninResponse").hide();
                    $("#prevSource").show()
                }
            }
        });
        return false
    };
    this.getCSRFToken = function() {
        var F = "/checkCSRF";
        $.ajax({
            url: F,
            type: "POST",
            dataType: "json",
            async: false,
            success: function(G) {
                if (G.status == "success") {
                    $("#CSRFToken").val(G.message)
                }
            }
        });
        return false
    };
    $("#signin_box").live("click", function(F) {
        if (F.target.id == this.id) {
            closepopup()
        }
    })
};
(function() {
    var G = false,
        F = false;
    var H = [];
    var D = null;

    function E(N, M, L) {
        if (L && L.authenticated) {
            Snapdeal.signupWidConfig.loggedIn = true;
            M(L);
            var K = $("#user-name").text();
            if (K == "") {
                $("#user-name, .customLoggedInState .userName").text(L.trackingEmail);
                $(".customLoggedInState .userName").attr("title", L.trackingEmail);
                $("#loggedInAccount").removeClass("hidden");
                $("#loggedOutAccount").addClass("hidden")
            }
            if (Snapdeal.sellerChat) {
                Snapdeal.sellerChat.setPersonalDetails(L.firstName, L.trackingEmail)
            }
        } else {
            Snapdeal.signupWidConfig.loggedIn = false;
            N();
            $("#loggedInAccount").addClass("hidden");
            $("#loggedOutAccount").removeClass("hidden")
        }
    }
    window.checkAuth = function(Q, P) {
        var L = "checkAuthSuccessHandler" + Math.floor((Math.random() * 1000));
        window["" + L] = (function(V, U, R) {
            var T = V,
                S = U;
            return function(Y) {
                if (Y) {
                    Y.loginTimestamp = new Date().getTime()
                }
                localStorage.setItem("authData", JSON.stringify(Y));
                G = true;
                D = Y;
                E(T, S, Y);
                for (var W = 0; W < H.length; W++) {
                    var X = H[W];
                    E(X.no, X.yes, Y)
                }
                window["" + R] = undefined
            }
        })(Q, P, L);
        var M = Snapdeal.Cookie.get("lu");
        var O = $.parseJSON(localStorage.getItem("authData"));
        if (M == "true") {
            if (O && O.authenticated == true && O.loginTimestamp && ((new Date().getTime() - O.loginTimestamp) < 1500000)) {
                G = true
            } else {
                G = false;
                localStorage.removeItem("authData");
                if (window.LocalStorageW) {
                    LocalStorageW.setItem("authData", "")
                }
            }
        } else {
            G = true;
            localStorage.removeItem("authData");
            if (window.LocalStorageW) {
                LocalStorageW.setItem("authData", "")
            }
        }
        var N = "/json/checkAuthentication";
        if (!G) {
            if (F) {
                H.push({
                    yes: P,
                    no: Q
                })
            } else {
                F = true;
                $.getScript(getHttpsPathStart() + "/checkAuthentication?cb=" + L)
            }
        } else {
            var K = $.parseJSON(localStorage.getItem("authData"));
            D = K || D;
            E(Q, P, D)
        }
    };
    window.checkAuthenticationOnLoad = function() {
        addLoginIframe();
        checkAuth(function() {
            C();
            var N = Snapdeal.signupWidConfig;
            var O = Snapdeal.Cookie.get("eid") ? true : false;
            var M = Snapdeal.Cookie.get(N.cookieNameFirstTime) ? false : true;
            if (typeof loginDialogDisabled == "undefined" && !Snapdeal.signupWidConfig.loggedIn) {
                if (M) {
                    if (N.onloadShow && !Snapdeal.signupWidConfig.loggedIn) {
                        addLoginIframe();
                        var L = !N.onloadShowOnlyIfNewUser;
                        var K = !O;
                        if (L || K) {
                            showLogin();
                            Snapdeal.Cookie.set(N.cookieNameFirstTime, "no")
                        }
                    }
                }
            }
        }, function() {
            B()
        })
    };
    var A = false;
    window.redirectURLonLogin = undefined;

    function J(K) {
        $("#signin_box").show();
        addLoginIframe();
        if (K) {
            window.redirectURLonLogin = K
        } else {
            window.redirectURLonLogin = undefined
        } if (A) {
            $("#signin_box").css({
                top: "0",
                left: "0"
            });
            $("#loginIframe").attr("width", "100%").attr("height", "100%")
        } else {
            $(document).bind("iframeLoaded", function() {
                $("#signin_box input").show();
                $("#signin_box").css({
                    top: "0",
                    left: "0"
                });
                $("#loginIframe").attr("width", "100%").attr("height", "100%")
            })
        }
    }

    function B() {
        Snapdeal.signupWidConfig.loggedIn = true
    }

    function C() {
        Snapdeal.signupWidConfig.loggedIn = false
    }
    window.showLogin = function(L) {
        var K = L;
        checkAuth(function() {
            J(K);
            C()
        }, function() {
            B()
        })
    };
    var I = false;
    addLoginIframe = function() {
        if (typeof isIframe == "undefined" && !I) {
            var L = $('<div style="width:100%; height:100%; margin:0 auto;"></div>');
            var K = $('<iframe id="loginIframe" src="/iframeLogin?targetUrl' + Snapdeal.signupWidConfig.targetUrl + "#" + window.location.href + '" onload="onloadSignupIframe()" frameborder="0" name="iframeLogin" marginwidth="0" marginheight="0" seamless="seamless" width="0%" height="0%" style="margin:0 auto;"  allowtransparency="true"></iframe>');
            K.attr({
                src: getHttpsPathStart() + "/iframeLogin#" + window.location.href
            });
            L.append(K);
            $("#signin_box").html(L);
            I = true;
            $("#signin_box").css({
                top: "-9999px",
                left: "-9999px",
                position: "fixed"
            });
            $("#signin_box input").hide();
            $("#signin_box").show()
        }
    };
    var A = false;
    onloadSignupIframe = function() {
        if (window.SnapBottomBar) {
            SnapBottomBar.httpsHandler.refresh()
        }
        A = true;
        $(document).trigger("iframeLoaded")
    }
})();
if (typeof isIframe == "undefined") {
    $(window).load(function() {
        checkAuthenticationOnLoad()
    })
}
$(function() {
    $("a.signIn").parents(".sdNavWrapper").on("click", function(G) {
        for (var F = 0; F < window.frames.length; F++) {
            XD.postMessage("dflt", "*", window.frames[F])
        }
        showLogin()
    });
    $(".hd-rvmp-log #loggedOutAccount li.stop-event").on("click", function(F) {
        if (F.stopPropagation) {
            F.stopPropagation()
        } else {
            F.returnValue = false
        }
    });
    var D = Snapdeal.signupWidConfig;
    var E = Snapdeal.Cookie.get("eid") ? true : false;
    var C = Snapdeal.Cookie.get(D.cookieNameFirstTime) ? false : true;
    if (typeof isIframe == "undefined") {
        $(".signIn").bind("click", function() {
            $(document).bind("keyup.escapeListener", function(F) {
                if (F.keyCode == 27) {
                    closepopup()
                }
            })
        })
    } else {
        if (C) {
            if (D.onloadShow && !Snapdeal.signupWidConfig.loggedIn) {
                var B = E && !D.onloadShowOnlyIfNewUser;
                var A = !E;
                if (B || A) {
                    Snapdeal.Cookie.set(D.cookieNameFirstTime, "no")
                }
            }
        }
        if (!E) {
            D.activeTab = 0
        } else {
            if (E) {
                D.activeTab = 1
            }
        }
        D.iframeActiveTab = D.activeTab
    }
});

function moreoptions() {
    $(".showmore").slideToggle();
    $(".moreoptions").toggleClass("active");
    return false
}

function createAccount(B) {
    loginValue = false;
    signupValue = true;
    passValue = false;
    setActiveTab(0);
    window.widget.getCSRFToken();
    if (signupJson.source && $("#ajaxSignup [name=j_source]").length == 0) {
        $("#ajaxSignup").append('<input type="hidden" id="j_source" name="j_source" value="' + signupJson.source + '"/>')
    }
    if (signupJson.targetUrl && $("#ajaxSignup [name=j_targetUrl]").length == 0) {
        $("#ajaxSignup").append('<input type="hidden" id="j_targetUrl" name="j_targetUrl" value="' + signupJson.targetUrl + '"/>')
    }
    var A = $("#regSlideWrapper > div").outerWidth();
    if (B) {
        $("#regSlideWrapper").animate({
            marginLeft: (-1 * A)
        }, 500, null, function() {
            if (isVisibleOffset($("#j_username_new"))) {
                $("#j_username_new:visible").focus()
            }
        })
    } else {
        $("#regSlideWrapper").css(marginLeft, (-1 * A));
        if (isVisibleOffset($("#j_username_new"))) {
            $("#j_username_new:visible").focus()
        }
    }
    return false
}

function isVisibleOffset(A) {
    return canFocus
}

function toPhoneNumberForm(A) {
    if (!A) {
        $("#regSlideWrapper").animate({
            marginLeft: 0
        }, 500, null, function() {
            if (isVisibleOffset($("#phoneNumberForm [name=j_number]"))) {
                $("#phoneNumberForm [name=j_number]:visible").focus()
            }
        })
    } else {
        $("#regSlideWrapper").css({
            marginLeft: 0
        });
        if (isVisibleOffset($("#phoneNumberForm [name=j_number]"))) {
            $("#phoneNumberForm [name=j_number]:visible").focus()
        }
    }
}

function alreadyAccount() {
    loginValue = true;
    signupValue = false;
    passValue = false;
    setActiveTab(1);
    backToLogin();
    return false
}

function forgotPassword() {
    loginValue = false;
    signupValue = false;
    passValue = true;
    $("#forgotPasswordNavigation").validate().resetForm();
    $(".loginParts").hide();
    $("#forgotPasswordDiv").show();
    if (isVisibleOffset($("#fp_email"))) {
        $("#fp_email:visible").focus()
    }
    return false
}

function forgotContinue() {
    $("#tab3").remove();
    $(".signupopupbox").append(window.widget.getSucc());
    if (isVisibleOffset($("#j_username"))) {
        $("#j_username:visible").focus()
    }
    return false
}

function backToLogin() {
    loginValue = true;
    signupValue = false;
    passValue = false;
    $("#ajaxSignin,#forgotPasswordNavigation").validate().resetForm();
    $(".successful").remove();
    $(".loginParts").hide();
    $("#ajaxSignin").show();
    if (isVisibleOffset($("#j_username"))) {
        $("#j_username:visible").focus()
    }
    return false
}

function changeColor() {
    return false
}

function forgotEnterPress(A) {
    if (A == 13) {
        return window.widget.forgotPassword()
    }
}
var currentActiveTab = 0;

function setActiveTab(A, D) {
    var C = A;
    if (C == undefined || C == null) {
        C = 0
    }
    $("ul.headtabs li").find("a").removeClass("active").eq(C).addClass("active");
    if (!D) {
        $("#signin_box .tabContainer").hide().eq(C).show()
    } else {
        $("#signin_box .tabContainer").show();
        var B = $(".tabContainer").width();
        $("#tabSlideWrapper").animate({
            marginLeft: -1 * B
        }, 1000, function() {
            $("#signin_box .tabContainer").hide().eq(C).show();
            $("#tabSlideWrapper").css("marginLeft", 0)
        })
    }
    currentActiveTab = C;
    if (A == 1) {
        backToLogin()
    } else {
        if (A == 0) {
            toPhoneNumberForm(true)
        }
    }
}

function closepopup() {
    window.localStorage.removeItem("navigate-to-myrecommendations");
    $(".signupopupbox").fadeOut();
    $("#signin_box").fadeOut();
    $(".signUpBoxWrapper").remove();
    $(document).unbind("keyup.escapeListener");
    $(".logInByEmailDiv").show();
    $(".logInFormWrapper").hide();
    return false
}
$("input[data-placeholder]").live("focus", function() {
    $(this).removeClass("placeholder");
    if ($(this).data("placeholderFlag")) {
        $(this).val("");
        $(this).data("placeholderFlag", false)
    }
});

function placeHolderInit() {
    if ($(this).val() == "" || $(this).data("placeholderFlag")) {
        $(this).val($(this).attr("data-placeholder"));
        $(this).data("placeholderFlag", true);
        $(this).addClass("placeholder")
    } else {
        $(this).data("placeholderFlag", false);
        $(this).removeClass("placeholder")
    }
}
$("input[data-placeholder]").live("blur", function() {
    if ($(this).val() == "") {
        $(this).val($(this).attr("data-placeholder"));
        $(this).data("placeholderFlag", true);
        $(this).addClass("placeholder")
    } else {
        $(this).data("placeholderFlag", false);
        $(this).removeClass("placeholder")
    }
});

function rpxSuccessHandler(B) {
    var C = "";
    var D = window.redirectURLonLogin || window.location.href;
    if (D.indexOf("?") != -1) {
        if (D.indexOf("#") != -1) {
            var E = D.substring(0, D.indexOf("#"));
            var A = D.substring(D.indexOf("#"), D.length);
            if (B) {
                C = "systemcode=" + Snapdeal.signupWidConfig.socialMsgCode;
                window.location.href = E + "&" + C + "&rand=" + (100 * Math.random()) + A
            } else {
                window.location.href = E + "&rand=" + (100 * Math.random()) + A
            }
        } else {
            if (B) {
                C = "systemcode=" + Snapdeal.signupWidConfig.socialMsgCode;
                window.location.href = D + "&" + C + "&rand=" + (100 * Math.random())
            } else {
                window.location.href = D + "&rand=" + (100 * Math.random())
            }
        }
    } else {
        if (D.indexOf("#") != -1) {
            var E = D.substring(0, D.indexOf("#"));
            var A = D.substring(D.indexOf("#"), D.length);
            if (B) {
                C = "systemcode=" + Snapdeal.signupWidConfig.socialMsgCode;
                window.location.href = E + "?" + C + "&rand=" + (100 * Math.random()) + A
            } else {
                window.location.href = E + "?rand=" + (100 * Math.random()) + A
            }
        } else {
            if (B) {
                C = "systemcode=" + Snapdeal.signupWidConfig.socialMsgCode;
                window.location.href = D + "?" + C + "&rand=" + (100 * Math.random())
            } else {
                window.location.href = D + "?rand=" + (100 * Math.random())
            }
        }
    }
}
if (window.isIframe) {
    XD.receiveMessage(function(F) {
        if (/^iframeStore/.test(F.data)) {
            var C = F.data.split("iframeStore#")[1];
            var B;
            try {
                B = JSON.parse(C);
                for (var A in B) {
                    var E = "";
                    if (B[A] || B[A] == 0) {
                        E = B[A]
                    }
                    localStorage.setItem(A, E)
                }
            } catch (D) {}
        }
    })
}
setReciever();

function setReciever() {
    if (typeof isIframe != "undefined" || typeof XD == "undefined") {
        return
    }
    XD.receiveMessage(function(E) {
        if (E.data == "close") {
            closepopup()
        } else {
            if (/^refresh/.test(E.data)) {
                var B = /true/.test(E.data);
                rpxSuccessHandler(B)
            } else {
                if (/^showMessage/.test(E.data)) {
                    var D = window.redirectURLonLogin || window.location.href;
                    var C = E.data.split(":")[1];
                    Snapdeal.Cookie.set("eid", C, 365);
                    var G = D.split("?");
                    D = G[0] + "?systemcode=" + Snapdeal.signupWidConfig.regMsgCode + "&msg=signup&email=" + C + G.slice(1).join("?");
                    if (D.indexOf("checkout") != -1) {
                        window.location.href = D;
                        return
                    }
                    window.location.href = D
                } else {
                    if (/^showPromoMessage/.test(E.data)) {
                        var D = window.redirectURLonLogin || window.location.href;
                        var C = E.data.split(":")[1];
                        Snapdeal.Cookie.set("eid", C, 365);
                        var G = D.split("?");
                        D = G[0] + "?systemcode=" + Snapdeal.signupWidConfig.hasCouponMsgCode + "&msg=signup&email=" + C + "&" + G.slice(1).join("?");
                        if (D.indexOf("checkout") != -1) {
                            window.location.href = D;
                            return
                        }
                        window.location.href = D
                    } else {
                        if (/^loginAndRefresh/.test(E.data)) {
                            var D = window.redirectURLonLogin || window.location.href;
                            var A = D.split("#");
                            if (D.indexOf("?") == -1) {
                                A[0] += "?"
                            } else {
                                A[0] += "&"
                            }
                            var F;
                            if (A[1]) {
                                F = "#" + (A[1] || "")
                            }
                            window.location.href = A[0].split("?")[0] + checkIfSystemCodePresentInUrl(A[0].split("?")[1]) + (F || "")
                        }
                    }
                }
            }
        }
    })
}

function splitUrlParams(C, B) {
    var A = [];
    if (C.indexOf(B) != -1) {
        A = C.split(B)
    } else {
        A.push(C)
    }
    return A
}

function checkIfSystemCodePresentInUrl(D) {
    if (typeof D != "undefined") {
        var B = splitUrlParams(D, "&");
        var A = "";
        var E = "?loginSuccess=success";
        for (var C = 0; C < B.length; C++) {
            if (B[C].indexOf("systemcode") == -1) {
                A += "&";
                A += B[C]
            }
        }
        if (A != "") {
            E += A
        }
    }
    return E
}

function onRPXSuccess(A) {
    if (typeof isIframe == "undefined") {
        rpxSuccessHandler(A)
    } else {
        XD.postMessage("refresh:" + A, parentLoc)
    }
}

function onRPXFail() {
    var A = window.location.href;
    if (A.indexOf("?") != -1) {
        window.location.href = A.substring(0, A.indexOf("?")) + "?systemcode=503"
    } else {
        window.location.href = window.location.href + "?systemcode=503"
    }
}
if (window.fbLoginId) {
    $(window).load(function() {
        window.fbAsyncInit = function() {
            FB._https = true;
            FB.init({
                appId: window.fbLoginId,
                status: true,
                cookie: true,
                xfbml: true
            });
            FB.getLoginStatus(function(B) {
                if (B.status === "connected") {
                    var D = B.authResponse.userID;
                    var C = B.authResponse.accessToken;
                    $.get("//graph.facebook.com/" + D + "?fields=name,picture", function(E) {
                        if (Snapdeal.Cookie.get("sdfblogin") == "yes") {
                            A(E.name, E.picture.data.url)
                        } else {
                            $("#loggedInAccount").removeClass("profile")
                        }
                    })
                } else {
                    $("#loggedInAccount").removeClass("profile")
                }
            })
        };
        (function(D) {
            var C, E = "facebook-jssdk",
                B = D.getElementsByTagName("script")[0];
            if (D.getElementById(E)) {
                return
            }
            C = D.createElement("script");
            C.id = E;
            C.async = true;
            C.src = "//connect.facebook.net/en_US/all.js";
            B.parentNode.insertBefore(C, B)
        }(document));

        function A(C, B) {
            $("#loggedInAccount").addClass("profile");
            $("#loggedInAccount .icon-user, .customLoggedInState").css("background-image", "url(" + B + ")");
            $(".profile #user-name, .customLoggedInState .userName").text(C);
            $(".customLoggedInState .userName").attr("title", C)
        }
    })
}
$("#logout-account").on("click", function() {
    Snapdeal.Cookie.set("sdfblogin", "yes", -1);
    LocalStorageW.setItem("authData", "")
});
$("#hideGutterMobilePromotion").click(function() {
    $("#gutterMobilePromotionContainer").attr("style", "display:none !important")
});
var checkSupportInput = document.createElement("input");
if (typeof checkSupportInput.autofocus === "undefined") {
    $("#keyword").focus()
}
var firefox = /Firefox/i.test(navigator.userAgent);
if (firefox) {
    var val = $("#keyword").val();
    $("#keyword").val("");
    setTimeout(function() {
        $("#keyword").val(val)
    }, 1)
};
var tagToSearch;
var theTag;
var imageURL;
stateProperties = {};
stateProperties.set = false;
window.showTag = false;

function getStateProperties(A) {
    stateProperties.set = true;
    stateProperties.soldOut = A.soldOut && A.productLifeState != "Coming Soon" && A.productLifeState != "Discontinued" && A.productLifeState != "Terminated";
    stateProperties.live = (A.productLifeState == "Prebook" || A.productLifeState == "Buy Now") && A.productStatus == "active";
    stateProperties.preOrBuy = A.productLifeState == "Prebook" || A.productLifeState == "Buy Now";
    stateProperties.buynow = A.productLifeState == "Buy Now";
    stateProperties.prebook = A.productLifeState == "Prebook";
    stateProperties.comingSoon = A.productLifeState == "Coming Soon";
    stateProperties.discontinued = A.productLifeState == "Discontinued" || A.productLifeState == "Terminated";
    stateProperties.launched = A.productLifeState == "Buy Now" && A.productStatus == "active";
    stateProperties.productLifeState = A.productLifeState;
    stateProperties.productStatus = A.productStatus;
    stateProperties.active = A.productStatus == "active";
    stateProperties.inactive = A.productStatus == "inactive";
    stateProperties.notAvaillableAtTheMoment = A.productStatus == "inactive" && (A.productLifeState == "Prebook" || A.productLifeState == "Buy Now")
}

function getGridProductHtml(H, B, G, A) {
    getStateProperties(H);
    window.showState = true;
    var E = "";
    E = H.productTag;
    if (E != undefined && E != null && E != "") {
        imageUrl = "";
        var D = ""
    }
    var C = "";
    C += '<div class="product_grid_cont gridLayout' + gridViewLayout + '" id="' + H.id + '"><div class="product_grid_box">';
    C += '<div class="wishOnProduct" pog="' + H.id + '" data-selected="false"><i class="icon-grey-heart"></i><span class="fnt11 wish-text">' + langSpecificLabels["label.categoryPage.addToWishlist"] + "</span></div>";
    C += '<div class="quickView-wrapper" quickviewid="' + H.id + '"><span class="fnt11">' + langSpecificLabels["label.categoryPage.quickView"] + " </span></div>";
    window.showTag = true;
    C += getGridProductContHtml(H, B, G, A);
    if (!stateProperties.comingSoon) {
        C += '<div class="product-txtWrapper">';
        var I = Snapdeal.Utils.fixArray(H.initialAttributes);
        if (H.initialAttributes != null && I.length > 0) {
            C += '<div class="hit-ss-logger productAttr" v="p" isPartialSearch="' + partialSearch + '" categoryId="' + categoryId + '" pogId="' + H.id + '" pos="' + B + ";" + A + '"> <select class="sel-attribute" onChange=" redirectToPDP( this, \' ' + Snapdeal.getAbsoluteStaticPath(H.pageUrl) + "')\">";
            C += '<option value=""> Select ' + I[0].name + "</option>";
            for (i = 0; i < I.length; i++) {
                if (I[0].name == I[i].name) {
                    if (!I[i].soldOut && I[i].live) {
                        C += '<option value="' + I[i].value + '">' + I[i].value + "</option>"
                    }
                }
            }
            C += "</select></div>"
        }
        var F = window.compareFlag || $("#compareFlag").val();
        if (!stateProperties.discontinued && !stateProperties.comingSoon) {
            if (F == "true") {
                C += '<div class="sdCheckbox addToCompare"><input type="checkbox" id="checkboxAddCompare-' + H.id + '" onClick="addToCompareClick(this)"/><label for="checkboxAddCompare-' + H.id + '" class="lang">' + langSpecificLabels["label.categoryPage.addToCompare"] + "</label></div>"
            }
        }
        C += "</div><!-- end of text wrapper -->"
    }
    C += "</div> <!-- grid box --> </div> <!-- grid layout end -->";
    $(document).trigger("loadLazyImages");
    return C
}

function getGridProductContHtml(I, B, G, A, J) {
    if (window.stateProperties && !(stateProperties.set == true)) {
        getStateProperties(I);
        stateProperties.set = false
    }
    if (window.showTag == true) {
        var F = "";
        F = I.productTag;
        var D;
        if (window.jsonMapForTags) {
            if (D = jsonMapForTags[F]) {
                imageUrl = D.tagImageUrl;
                var E = "";
                if (D.offer == true) {
                    E = D.offerDescription
                }
            }
        }
    }
    if (!J) {
        J = ""
    }
    var C = "";
    C += '<div class="productWrapper"><div class="product-image ' + (stateProperties.soldOut || stateProperties.discontinued ? "prodSoldout" : "") + '" alt="' + I.name + '" atl="' + I.name + '">';
    C += '<a class="hit-ss-logger somn-track prodLink" pos="' + B + ";" + A + '" v="p" isPartialSearch="' + partialSearch + '" categoryId="' + I.categoryId + '" href="' + Snapdeal.getAbsoluteStaticPath(I.pageUrl) + '" pogId="' + I.id + '" hidOmnTrack="' + J + '">';
    if ((stateProperties.comingSoon || stateProperties.discontinued) && !window.hPRecentlyWidget) {
        if (stateProperties.comingSoon) {
            C += '<div class="comingSoonTag statusTag"></div>'
        } else {
            if (stateProperties.discontinued) {
                C += '<div class="discontinuedGridTag">' + langSpecificLabels["label.productStatus.discontinued"] + "</div>"
            }
        }
    }
    if (!(stateProperties.comingSoon || stateProperties.discontinued)) {
        if (window.showTag == true) {
            if (D && imageUrl) {
                C += '<div class ="productTagImage" tagName="' + F + '">';
                C += '<img src="' + imageUrl + '"  data-attr="' + imageUrl + '" title ="' + E + '" />';
                C += "</div>"
            }
            window.showTag == false
        }
    }
    if (G != 1) {
        C += '<img class="lazy-effect" anim="true" lazySrc="' + Snapdeal.getResourcePath(I.image).replace("166x194", prodGridImgSizeCust) + '" src="" border="0"alt="' + I.name + '" title="' + I.name + '" />'
    } else {
        C += '<img src="' + Snapdeal.getResourcePath(I.image).replace("166x194", prodGridImgSizeCust) + '" border="0" alt="' + I.name + '" title="' + I.name + '" />'
    }
    C += "</a>";
    if (!gpsf && (!isPersonalizedHP || window.displayHPTrendingProduct) && I.shippingDays != null && I.shippingDays == 1) {
        C += '<div class="shippingImage">';
        C += '<span class="tooltip">';
        C += '<label class="lang dispatched-one-day-lang">' + langSpecificLabels["label.categoryPage.dispatchedInOneBusinessDay"] + "</label>";
        C += "</span>";
        C += "</div>"
    }
    C += "" + ((stateProperties.soldOut && !stateProperties.notAvaillableAtTheMoment) == true ? '<div class="soldout-tag prod-grid-sold-out-lang">' + langSpecificLabels["label.categoryPage.soldOut"] + "</div>" : "") + '</div><!-- product Image --><div class="product-txtWrapper">';
    if (window.hPRecentlyWidget) {
        if (stateProperties.comingSoon) {
            C += '<div class="redText">' + langSpecificLabels["label.productStatus.comingSoon"] + "</div>"
        } else {
            if (stateProperties.discontinued) {
                C += '<div class="redText">' + langSpecificLabels["label.productStatus.discontinued"] + "</div>"
            } else {
                if (stateProperties.notAvaillableAtTheMoment) {
                    C += '<div class="redText">' + langSpecificLabels["label.productStatus.notAvailableAtTheMoment"] + "</div>"
                }
            }
        }
        window.hPRecentlyWidget = false
    }
    C += '<div class="product-title">';
    C += '<a class="hit-ss-logger somn-track prodLink" pos="' + B + ";" + A + '" v="p" isPartialSearch="' + partialSearch + '" categoryId="' + I.categoryId + '" href="' + Snapdeal.getAbsoluteStaticPath(I.pageUrl) + '" pogId="' + I.id + '" hidOmnTrack="' + J + '">';
    if (window.characterLength != "" && window.characterLength != undefined) {
        C += sanitizeProductName(I.name, characterLength)
    } else {
        C += I.name
    }
    C += "</a></div> <!-- product title -->";
    if (!stateProperties.comingSoon) {
        C += '<a rel="nofollow" class="hit-ss-logger somn-track prodLink" pos="' + B + ";" + A + '" v="p" isPartialSearch="' + partialSearch + '" categoryId="' + I.categoryId + '" href="' + Snapdeal.getAbsoluteStaticPath(I.pageUrl) + '" pogId="' + I.id + '" hidOmnTrack="' + J + '">';
        if ((!isPersonalizedHP || window.displayHPTrendingProduct) && I.noOfReviews > 0) {
            C += '<div class="ratingsWrapper"><div class="ratingStarsSmall" style="background-position: 0px -' + imageOffsetForRating(I.avgRating) * 10 + 'px;"></div><div class="lfloat"> (' + I.noOfReviews + ") </div></div>"
        }
        C += '<div class="product-price">';
        if (I.prebook && !I.launched) {
            C += '<div class="fnt12 prod-prebook">Pre Order at <br><strong class="blackText">Rs ' + I.displayPrice + "</strong></div>"
        } else {
            if ((!isPersonalizedHP || window.displayHPTrendingProduct) && I.price != null && I.voucherPrice != null) {
                C += '<div><span class="' + (I.discount > 0 ? "" : "invisible") + '"><strike>Rs ' + I.price + "</strike> <s>(" + I.discount + "%)</s></span>Rs " + I.displayPrice + "</div>"
            } else {
                if (isPersonalizedHP && I.price != null && I.voucherPrice != null) {
                    C += "<div>Rs " + I.displayPrice + "</div>"
                }
            }
        } if ((!isPersonalizedHP || window.displayHPTrendingProduct) && I.externalCashBackApplicable == true && I.cashBackValue > 0) {
            var H = I.cashBackValue.toString().split(".");
            if (I.cashBackType == "PCT") {
                if (H[1] > 1) {
                    C += '<div class="cashBack">Insta Cashback:&nbsp;' + I.cashBackValue + "%</div>"
                } else {
                    C += '<div class="cashBack">Insta Cashback:&nbsp;' + H[0] + "%</div>"
                }
            }
            if (I.cashBackType == "ABS") {
                C += '<div class="cashBack">Insta Cashback:&nbsp;Rs ' + H[0] + "</div>"
            }
        }
        C += "</div>";
        if ((!isPersonalizedHP || window.displayHPTrendingProduct) && I.freebies != null && I.freebies.length > 0) {
            C += '<div class="freebie-ico"><i class="list-gridIcons freebieIcon"></i><label class="lang freebie-lang">' + langSpecificLabels["label.categoryPage.freebie"] + "</label></div>"
        }
        C += "</a>"
    }
    C += "</div></div>";
    return C
}

function getQuickviewWishlist(A) {
    var B = "";
    B += '<div class="wishOnProduct" pog="' + A.id + '" data-selected="false"><i class="icon-grey-heart"></i><span class="fnt11 wish-text">' + langSpecificLabels["label.categoryPage.addToWishlist"] + "</span></div>";
    B += '<div class="quickView-wrapper" quickViewId="' + A.id + '"><span class="fnt11">' + langSpecificLabels["label.categoryPage.quickView"] + " </span></div>";
    return B
};
var tagToSearch;
var theTag;
var imageURL;
window.showTag = false;
var gridForFashionFlag = "${gridForFashion}";
var gridForElectronicsFlag = "${gridForElectronics}";
var cartFlag = false;
var catalog = 0;
var catalogGrid = 0;
var Arr = new Array();
var iArrAttr = new Array();
var arrSoldOut = new Array();
$(".buybutton").live("click", function() {
    $(this).parents(".hoverProdCont").css("box-shadow", "0px 0px 1px #000 inset");
    $(this).parents(".hoverProdCont").css("-moz-box-shadow", "0px 0px 1px #000 inset");
    $(this).parents(".hoverProdCont").css("-webkit-box-shadow", "0px 0px 1px #000 inset")
});
$(".hoverProdCont4Grid .buybutton").live("click", function() {
    $(this).parents(".hoverProdCont4Grid").css("box-shadow", "0px 0px 1px #000 inset");
    $(this).parents(".hoverProdCont4Grid").css("-moz-box-shadow", "0px 0px 1px #000 inset");
    $(this).parents(".hoverProdCont4Grid").css("-webkit-box-shadow", "0px 0px 1px #000 inset")
});
$(".addToCartOnGrid").live("click", function() {
    cartFlag = false;
    if ($(this).parents(".hoverProdCont").find(".prod-buy-outer").size() != 0) {
        $(this).parents(".hoverProdCont").find(".hoverProductImage").css("opacity", "0.85");
        $(this).parents(".hoverProdCont").find(".bx-wrapper").css("background-color", "#000");
        $(this).parents(".hoverProdCont").find(".outerImg").css("background-color", "#000")
    }
    if ($(this).attr("addedToCart") == "true" && $(this).parents(".hoverProdCont").find(".prod-buy-outer").size() != 0) {
        $(this).parents(".hoverProdCont").find(".productAttr .product-attr:not(':first')").css("display", "none");
        $(this).parents(".hoverProdCont").find(".productAttr .attrActive").removeClass("selected");
        $(this).parents(".hoverProdCont").find(".productAttr .attrDisabled").removeClass("selected");
        $(this).parents(".hoverProdCont").find(".productAttr .product-attr").eq(0).css("display", "block");
        $(this).attr("catalog", "undefined");
        $(this).attr("supc", "undefined");
        $(this).attr("vendorcode", "undefined")
    }
    var G = $(this).parent().siblings(".product-hoverWrapper").css("display", "block");
    initSdScrollHover(G);
    if ($(this).parent().siblings(".slimScrollDiv").size() == 1) {
        $(this).parent().siblings(".slimScrollDiv").css("display", "block");
        $(this).parent().siblings(".slimScrollDiv").find(".product-hoverWrapper").css("display", "block");
        $(this).parent().siblings(".slimScrollDiv").find(".product-hoverWrapper").css("bottom", "0px");
        $(this).parent().siblings(".slimScrollDiv").find(".hoverClose").css("margin-right", "7px")
    }
    if ($(this).attr("catalog") == "undefined") {
        $(this).parents(".hoverProdCont").find(".product-attr").not(":first").css("display", "none");
        $(this).parents(".hoverProdCont").find(".product-attr").eq(0).css("display", "block");
        $(this).parents(".hoverProdCont").find(".productAttr .attrActive").removeClass("selected");
        $(this).parents(".hoverProdCont").find(".productAttr .attrDisabled").removeClass("selected");
        return false
    } else {
        if ($(this).parents(".hoverProdCont").find(".product-hoverWrapper").children(".prod-buy-outer").size() == 0) {
            $(this).parents(".hoverProdCont").find(".product-hoverWrapper").css("display", "none");
            if (buyButtonTransitionFlag == "true") {
                cartFlag = true;
                var D = $(this).parents(".hoverProdCont").find(".productWrapper").find(".product-image a").length;
                var F = $(this).parents(".hoverProdCont").find(".productWrapper").find(".product-image a");
                for (var C = 0; C < D; C++) {
                    if (F.eq(C).is(":visible")) {
                        var E = F.eq(C).children("img")
                    }
                }
                if (E != "" || E != null) {
                    var B = E
                }
                moveItemToDest(B, $("#navCartWrapper"));
                var A = false;
                if ($(this).attr("prebook") == "true") {
                    A = true
                }
                $(".menucard-overlay").hide();
                window.cartService.addToCart($(this).attr("catalog"), $(this).attr("vendorCode"), $(this).attr("supc"));
                window.cartService.fireOmnitureScript("addToCart", $(this).attr("catalog"), true, A, undefined, undefined, "categoryHover", $(this).attr("shippingCharges"));
                $(this).attr("addedToCart", "true");
                catalog = $(this).attr("catalog");
                return false
            } else {
                cartFlag = false;
                var A = false;
                if ($(this).attr("prebook") == "true") {
                    A = true
                }
                $(".menucard-overlay").hide();
                window.cartService.addToCart($(this).attr("catalog"), $(this).attr("vendorCode"), $(this).attr("supc"));
                window.cartService.fireOmnitureScript("addToCart", $(this).attr("catalog"), true, A, undefined, undefined, "categoryHover", $(this).attr("shippingCharges"));
                $(this).attr("addedToCart", "true");
                catalog = $(this).attr("catalog");
                return false
            }
        }
    } if (buyButtonTransitionFlag == "true") {
        cartFlag = true;
        $("#cart-layout-div.activeTransModalOuter").css("display", "none");
        $(".cart-cont-outer.qv_pane_outer").css("display", "none");
        cartFlag = false
    }
    return false
});
$(".addToCartOn4Grid").live("click", function() {
    cartFlag = false;
    if ($(this).attr("catalog") == "undefined") {
        var G = $(this).attr("id").split("-")[1];
        return false
    }
    if ($(this).attr("catalog") != "undefined") {
        if (buyButtonTransitionFlag == "true") {
            cartFlag = true;
            var A = false;
            if ($(this).attr("prebook") == "true") {
                A = true
            }
            $(".menucard-overlay").hide();
            var D = $(this).parents(".hoverProdCont4Grid").find(".productWrapper").find(".product-image a").length;
            var F = $(this).parents(".hoverProdCont4Grid").find(".productWrapper").find(".product-image a");
            for (var C = 0; C < D; C++) {
                if (F.eq(C).is(":visible")) {
                    var E = F.eq(C).children("img")
                }
            }
            if (E != "" || E != null) {
                var B = E
            }
            moveItemToDest(B, $("#navCartWrapper"));
            window.cartService.addToCart($(this).attr("catalog"), $(this).attr("vendorCode"), $(this).attr("supc"));
            window.cartService.fireOmnitureScript("addToCart", $(this).attr("catalog"), true, A, undefined, undefined, "categoryHover", $(this).attr("shippingCharges"));
            catalogGrid = $(this).attr("catalog");
            $(this).attr("addedToCart", "true");
            return false
        } else {
            cartFlag = false;
            var A = false;
            if ($(this).attr("prebook") == "true") {
                A = true
            }
            $(".menucard-overlay").hide();
            window.cartService.addToCart($(this).attr("catalog"), $(this).attr("vendorCode"), $(this).attr("supc"));
            window.cartService.fireOmnitureScript("addToCart", $(this).attr("catalog"), true, A, undefined, undefined, "categoryHover", $(this).attr("shippingCharges"));
            catalogGrid = $(this).attr("catalog");
            $(this).attr("addedToCart", "true");
            return false
        }
    }
});

function createAttributesGrid(B, D) {
    if (D == "true") {
        var A = $("#productAttributesJson-" + B).text()
    } else {
        var A = $("#productAttributesJson-" + B).text()
    } if (A == "[]" || A == "") {
        return
    }
    if (A && A != "undefined") {
        var C = $.parseJSON(A);
        window.attributeHandler = new Snapdeal.GridAttributeHandler(B);
        window.attributeHandler.init(C)
    }
}
$(document).ready(function() {
    $(".hoverProductImage").each(function() {
        $(this).children("a:not(:first)").css("display", "none")
    });
    $(".product-image").each(function() {
        $(this).parents(".hoverProdCont").find(".attrValue i:visible").last().css("display", "none");
        $(this).parents(".hoverProdCont").find(".subAttrValue i:visible").last().css("display", "none");
        $(this).parents(".hoverProdCont").find(".attrValue:visible").first().css("padding-left", "2px");
        $(this).parents(".hoverProdCont").find(".subAttrValue:visible").first().css("padding-left", "2px")
    });
    $(".product_grid_row").each(function() {
        if ($(this).find(".hoverProductWrapper").eq(0).height() > 0) {
            var D = $(this).find(".hoverProductWrapper").eq(0).height();
            if ($(this).find(".hoverProdCont").size() > 0) {
                $(this).find(".hoverProductWrapper").each(function() {
                    var E = $(this).height();
                    if (E > D) {
                        D = E
                    }
                });
                $(this).find(".hoverProductWrapper").each(function() {
                    $(this).css("height", D)
                })
            }
        }
    });
    $(".product_grid_row").each(function() {
        if ($(this).find(".hoverProdCont4Grid").size() > 0) {
            var D = $(this).find(".hoverProdCont4Grid").eq(0).find(".hoverProductWrapper").height() + $(this).find(".hoverProdCont4Grid").eq(0).find(".product_list_view_highlights").height() + $(this).find(".hoverProdCont4Grid").eq(0).find(".addToCompare").height() + $(this).find(".hoverProdCont4Grid").eq(0).find(".emiMonthsHoverGrid").height();
            $(this).find(".hoverProductWrapper").each(function() {
                var E = $(this).height() + $(this).find(".product_list_view_highlights").height() + $(this).find(".emiMonthsHoverGrid").height() + $(this).parents(".hoverProdCont4Grid").find(".addToCompare").height();
                if (E > D) {
                    D = E
                }
            });
            $(this).find(".hoverProductWrapper").each(function() {
                $(this).css("height", D + 3)
            })
        }
    });
    $(".hoverProdCont.gridLayout4").each(function() {
        if ($(this).find(".buybutton").css("top") != undefined) {
            var D = ($(this).height()) - $(this).find(".buybutton").css("top").split("px")[0];
            $(this).find(".product-hoverWrapper").css("bottom", (D + 11) + "px")
        }
    });
    $(".hoverProdCont.gridLayout3").each(function() {
        if ($(this).find(".buybutton").css("top") != undefined) {
            var D = ($(this).height()) - $(this).find(".buybutton").css("top").split("px")[0];
            $(this).find(".product-hoverWrapper").css("bottom", (D + 11) + "px")
        }
    });
    if (gridForFashionFlag == "true") {
        if ($("#defaultViewType").html() == "Grid") {
            var B = getGridList();
            var C = B.length;
            for (var A = 0; A < C; A++) {
                createAttributesGrid(B[A], "false")
            }
        }
    }
});

function getGridList() {
    var A = [];
    $(".productAttr").each(function() {
        A.push($(this).attr("productId"))
    });
    return A
}
Snapdeal.GridAttributeHandler = function(B) {
    var A = this;
    this.attributeNames = {};
    this.init = function(E) {
        this.attributeJson = E;
        depth = this.getDepth();
        var C = "";
        for (var D = 0; D < depth; D++) {
            C += '<div class="product-attr blk" id="attr-level-' + D + "-" + B + '" style="display:' + ((D > 0) ? "none" : "block") + '">';
            C += '<div class="attrText lfloat"><span class="opti-txt">Select ' + this.attributeNames[D] + "</span></div>";
            C += '<div class="lfloat ' + ((this.attributeNames[D] == "Colour") ? "colorMenu" : "selmenu") + '" level="' + D + '" id="attribute-select-' + D + "-" + B + '"></div>';
            C += "</div>"
        }
        $(document.querySelectorAll("#product-option-" + B)).each(function(L, K) {
            $(this).html(C)
        });
        var J = "";
        J += "<div class='overhid'>";
        for (var D = 0; D < this.attributeJson.length; D++) {
            if (depth == 1 && !this.attributeJson[D].live) {}
            var H = "";
            var I = "attrActive";
            if (depth > 1) {
                var G = this.attributeJson[D].subAttributes;
                var F = false;
                if (G.length > 1) {
                    F = subLevelCheck(G)
                } else {
                    if (G.length == 1) {
                        F = G[0].live || false
                    }
                } if (F == false) {
                    I = "attrDisabled"
                }
            }
            if (depth == 1 && this.attributeJson[D].soldOut) {
                H = " - Sold Out";
                I = "attrDisabled"
            }
            if (depth > 1 && this.attributeJson[D].soldOut) {
                H = " - Sold Out";
                I = "attrDisabled"
            }
            if (this.attributeJson[D].value == attrVal) {
                J += "<div class='lfloat attr-" + B + " pdpAattr " + I + " selected' id='" + this.attributeJson[D].value + "' title='" + this.attributeJson[D].value + H + "'><span>" + this.attributeJson[D].value + "<div class='attrState'></div></span></div>"
            } else {
                J += "<div class='lfloat attr-" + B + " pdpAattr " + I + "' id='" + this.attributeJson[D].value + "' title='" + this.attributeJson[D].value + H + "'><span>" + this.attributeJson[D].value + "<div class='attrState'></div></span></div>"
            }
        }
        J += "</div>";
        $("#attribute-select-0-" + B).html(J);
        $(".attr-" + B).die("click").live("click", function() {
            A.attributeChange($(this))
        });
        $(".selmenu").on("mouseout", ".attrActive", function() {
            $(".clsToolTipDiv").css("display", "none")
        });
        if (attrVal != "") {
            $(".selected").trigger("click")
        }
    };
    this.getDepth = function() {
        var E = 0;
        var D = {};
        var C = this.attributeJson;
        while (C.length > 0) {
            this.attributeNames[E] = C[0].name;
            E++;
            C = C[0].subAttributes
        }
        this.depth = E;
        return E
    };
    this.attributeChange = function(N) {
        if (N.hasClass("attrDisabled") == true) {
            return false
        }
        var D = N.parents(".sdScroll");
        var F = parseInt(N.parent().parent().attr("level"));
        var P = Snapdeal.getStaticPath("") + "/checkout?catalog=";
        $(N).parent().parent().attr("slctAttr", N.attr("id"));
        var H = "undefined";
        var G = "undefined";
        var R = "";
        $("#attribute-select-" + F + "-" + B).find(".selected").removeClass("selected");
        $(N).addClass("selected");
        if (N.parents(".productAttr").find(".selmenu:last").html() != N.parents(".selmenu").html()) {
            N.parents(".product-attr").css("display", "none");
            N.parents(".product-attr").next().css("display", "block")
        }
        if (N.parents(".productAttr").find(".colorMenu:last").html() != N.parents(".colorMenu").html()) {
            N.parents(".product-attr").css("display", "none");
            N.parents(".product-attr").next().css("display", "block")
        }
        if (F != this.depth - 1) {
            var L = "";
            R = this.getAttributesAtDepth(F + 1);
            var E = "";
            var O = "";
            $("div[pdpAttr='ColorrBox']").html('<ul id="zoom-select-1" class="sd-droplist" num="1"></ul>');
            E += "<div class='overhid'>";
            for (var K = 0; K < R.length; K++) {
                var J = "";
                var Q = "attrActive";
                if (!R[K].live) {
                    continue
                }
                $("#attr-level-" + (F + 1) + "-" + B).show();
                if (F == this.depth - 2 && R[K].soldOut) {
                    J = " - Sold Out";
                    Q = "attrDisabled"
                }
                if ((R.subAttributes) && (R.subAttributes.length > 1)) {
                    Q = (subLevelCheck(R.subAttributes)) ? "attrActive" : "attrDisabled"
                } else {
                    if ((R.subAttributes)) {
                        Q = R.subAttributes[0] || false
                    }
                } if (R[K].name == "Colour") {
                    if (R[K].value == "Multi") {
                        E += "<div class='lfloat attr-" + B + " pdpAattr " + Q + "' id='" + R[K].value + "' title='" + R[K].value + J + "'><span class='filterMultiColor'>" + R[K].value + "<div class='attrState'></div></span></div>"
                    } else {
                        E += "<div class='lfloat attr-" + B + " pdpAattr " + Q + "' id='" + R[K].value + "' title='" + R[K].value + J + "'><span>" + R[K].value + "<div class='attrState'></div></span></div>"
                    } if (Q == "attrActive") {
                        O += "<div class='color-code-box' colorName=" + R[K].value + " style='background-color:" + R[K].value.replace(/\ /g, "") + "'></div>"
                    }
                } else {
                    E += "<div class='lfloat attr-" + B + " pdpAattr " + Q + "' id='" + R[K].value + "' title='" + R[K].value + J + "'><span>" + R[K].value + "<div class='attrState'></div></span></div>";
                    O += "<li>" + R[K].value + "</li>"
                }
            }
            E += "</div>";
            $("#attribute-select-" + (F + 1) + "-" + B).html(E);
            $("#attribute-select-" + (F + 1) + "-" + B).removeClass("disabled no-display");
            if ($("#attribute-select-" + (F + 1) + "-" + B).hasClass("colorMenu")) {
                $("div[pdpAttr='ColorrBox']").append(O)
            } else {
                $("#zoom-select-" + (F + 1)).html(O).show();
                $("#zoom-select-" + (F + 1)).parent().parent().show()
            }
            for (var K = F + 2; K < this.depth; K++) {
                $("#attribute-select-" + K + "-" + B).addClass("disabled no-display")
            }
            P += "undefined"
        } else {
            var M = $("#attribute-select-" + F + "-" + B).attr("slctAttr");
            R = this.getAttributesAtDepth(F);
            if (R.length != undefined) {
                for (var I = 0; I < R.length; I++) {
                    if (R[I].value == M) {
                        P += R[I].catalogId;
                        H = R[I].catalogId;
                        G = R[I].supc;
                        pincodeButtonClick = false;
                        attrNotSelected = false;
                        if (R[I].images != undefined && R[I].images != null && R[I].images != "") {
                            this.changeImageOnSizeSelection(R[I].images)
                        }
                    }
                }
            } else {
                P += R.catalogId;
                H = R.catalogId;
                G = R.supc
            }
        }
        $(".buylink-" + B).attr("href", P);
        $(".buylink-" + B).attr("catalog", H);
        $(".buylink-" + B).attr("supc", G);
        if (G != "undefined") {
            $("#hightLightSupc").html(G);
            A.getVendorCodeForGrid(G, B, D)
        }
        $(".verifylink").attr("supc", G);
        $(".verifylink").attr("catalogId", H);
        mainProductAttrHtml = "";
        var C = $("div.browseCategories-popup-catName[typpe='main'][cubAttr='" + $(N).text() + "']");
        initSdScrollHover(D)
    };
    this.returnToCart = function(F) {
        if ($(".buylink-" + B).attr("catalog") != "undefined") {
            if (buyButtonTransitionFlag == "true") {
                cartFlag = true;
                var C = false;
                if ($(".buylink-" + B).attr("prebook") == "true") {
                    C = true
                }
                $(".menucard-overlay").hide();
                imgElementLength = F.parents(".hoverProdCont").find(".productWrapper").find(".product-image a").length;
                imgElement = F.parents(".hoverProdCont").find(".productWrapper").find(".product-image a");
                for (var E = 0; E < imgElementLength; E++) {
                    if (imgElement.eq(E).is(":visible")) {
                        finalElement = imgElement.eq(E).children("img")
                    }
                }
                if (finalElement != "" || finalElement != null) {
                    var D = finalElement
                }
                moveItemToDest(D, $("#navCartWrapper"));
                window.cartService.addToCart($(".buylink-" + B).attr("catalog"), $(".buylink-" + B).attr("vendorCode"), $(".buylink-" + B).attr("supc"));
                window.cartService.fireOmnitureScript("addToCart", $(".buylink-" + B).attr("catalog"), true, C, undefined, undefined, "categoryHover", $(".buylink-" + B).attr("shippingCharges"));
                F.css("display", "none");
                F.parent(".slimScrollDiv").css("display", "none");
                F.parents(".hoverProdCont").find(".addToCartOnGrid").attr("addedToCart", "true");
                F.parents(".hoverProdCont").find(".hoverProductImage").css("opacity", "1");
                F.parents(".hoverProdCont").find(".bx-wrapper").css("background-color", "#fff");
                F.parents(".hoverProdCont").find(".outerImg").css("background-color", "#fff");
                return false
            } else {
                cartFlag = false;
                var C = false;
                if ($(".buylink-" + B).attr("prebook") == "true") {
                    C = true
                }
                $(".menucard-overlay").hide();
                window.cartService.addToCart($(".buylink-" + B).attr("catalog"), $(".buylink-" + B).attr("vendorCode"), $(".buylink-" + B).attr("supc"));
                window.cartService.fireOmnitureScript("addToCart", $(".buylink-" + B).attr("catalog"), true, C, undefined, undefined, "categoryHover", $(".buylink-" + B).attr("shippingCharges"));
                F.css("display", "none");
                F.parent(".slimScrollDiv").css("display", "none");
                F.parents(".hoverProdCont").find(".addToCartOnGrid").attr("addedToCart", "true");
                F.parents(".hoverProdCont").find(".hoverProductImage").css("opacity", "1");
                F.parents(".hoverProdCont").find(".bx-wrapper").css("background-color", "#fff");
                F.parents(".hoverProdCont").find(".outerImg").css("background-color", "#fff");
                return false
            }
        }
    };
    this.changeImageOnSizeSelection = function(C) {
        $.each(C, function(E, D) {
            if (D != undefined && D != "") {
                var F = D.replace("img/product/main/", "img/product/main/small/");
                $("#product-thumbs li img").eq(E).attr("src", Snapdeal.getResourcePath(F));
                $("#product-slider li img").eq(E).attr("src", Snapdeal.getResourcePath(D))
            }
        })
    };
    this.getAttributesAtDepth = function(G) {
        var F = this.attributeJson;
        for (var E = 0; E < G; E++) {
            var D = $("#attribute-select-" + E + "-" + B).attr("slctAttr");
            for (var C = 0; C < F.length; C++) {
                if (F[C].value == D) {
                    F = F[C].subAttributes;
                    break
                }
            }
        }
        return F
    };
    this.getVendorCodeForGrid = function(C, D, E) {
        if (C != "undefined") {
            $.ajax({
                url: Snapdeal.getStaticPath("/json/getInventoryPrice?supc=" + C),
                type: "GET",
                dataType: "json",
                cache: false,
                success: function(G) {
                    if (G != null && G != "") {
                        $(".buylink-" + D).attr("vendorCode", G.vendorCode);
                        $(".shippingDays-" + D).html(G.shippingDays);
                        if (G.freebies.length > 0) {
                            $("#freebies_text_wrapper-" + D).html(A.getFreebiesText(G.freebies));
                            $("#freebies_wrapper-" + D).show()
                        } else {
                            $("#freebies_wrapper-" + D).hide()
                        } if (G.externalCashBackApplicable == true && G.cashBackValue > 0) {
                            var F = G.cashBackValue.toString().split(".");
                            if (G.cashBackType == "PCT") {
                                if (F[1] > 1) {
                                    $("#cashBackListView-" + D).find(".cashBackPrice").html(" " + G.cashBackValue + "%")
                                } else {
                                    $("#cashBackListView-" + D).find(".cashBackPrice").html(" " + F[0] + "%")
                                }
                            }
                            if (G.cashBackType == "ABS") {
                                $("#cashBackListView-" + D).find(".cashBackPrice").html(" Rs " + F[0])
                            }
                            $("#cashBackListView-" + D).show()
                        } else {
                            $("#cashBackListView-" + D).hide()
                        }
                    }
                    A.returnToCart(E)
                }
            })
        }
    };
    this.getFreebiesText = function(D) {
        var C = "";
        for (i = 0; i < D.length; i++) {
            C += (i == 0 ? "" : "+&nbsp;") + D[i] + "&nbsp;"
        }
        return C
    }
};

function generateSlider() {
    var A = 0;
    var C = 0;
    for (A = 0; A < iArrAttr.length; A++) {
        $(".product-image[prodId='" + iArrAttr[A] + "']").each(function() {
            $(this).parents(".hoverProdCont").find(".attrValue i:visible").last().css("display", "none");
            $(this).parents(".hoverProdCont").find(".subAttrValue i:visible").last().css("display", "none");
            $(this).parents(".hoverProdCont").find(".attrValue:visible").first().css("padding-left", "2px");
            $(this).parents(".hoverProdCont").find(".subAttrValue:visible").first().css("padding-left", "2px");
            $(this).children("a").removeClass("lazyBg")
        })
    }
    for (C = 0; C < arrSoldOut.length; C++) {
        $(".product-image[prodId='" + arrSoldOut[C] + "']").each(function() {
            $(this).parents(".hoverProdCont").find(".attrValue i:visible").last().css("display", "none");
            $(this).parents(".hoverProdCont").find(".subAttrValue i:visible").last().css("display", "none");
            $(this).parents(".hoverProdCont").find(".attrValue:visible").first().css("padding-left", "2px");
            $(this).parents(".hoverProdCont").find(".subAttrValue:visible").first().css("padding-left", "2px")
        })
    }
    $(".product_grid_row.prodGrid").each(function() {
        var D = $(this).find(".hoverProductWrapper").eq(0).height();
        if ($(this).find(".hoverProdCont").size() > 0) {
            $(this).find(".hoverProductWrapper").each(function() {
                var E = $(this).height();
                if (E > D) {
                    D = E
                }
            });
            $(this).find(".hoverProductWrapper").each(function() {
                $(this).css("height", D)
            })
        }
    });
    $(".prodheight.gridLayout4").each(function() {
        if ($(this).find(".buybutton").css("top") != undefined) {
            var D = ($(this).height()) - $(this).find(".buybutton").css("top").split("px")[0];
            $(this).find(".product-hoverWrapper").css("bottom", (D + 11) + "px")
        }
    });
    $(".prodheight.gridLayout3").each(function() {
        if ($(this).find(".buybutton").css("top") != undefined) {
            var D = ($(this).height()) - $(this).find(".buybutton").css("top").split("px")[0];
            $(this).find(".product-hoverWrapper").css("bottom", (D + 11) + "px")
        }
    });
    for (var B = 0; B < Arr.length; B++) {
        $(".product-image[prodId='" + Arr[B] + "']").each(function() {
            var D = $(this).bxSlider({
                auto: false,
                pause: 2000,
                easing: "swing",
                prevText: "",
                nextText: "",
                mode: "fade",
                pager: false,
                autoHover: false,
                adaptiveHeight: true,
                controls: true,
            });
            $(".product-image[prodId='" + Arr[B] + "']").parent().siblings().css("display", "none");
            D.parent().siblings().click(function() {
                if (D.find("img[hoverSrc]").size() > 0) {
                    D.find("img[hoverSrc]").each(function() {
                        $(this).attr("src", $(this).attr("hoverSrc"));
                        $(this).removeAttr("hoverSrc")
                    })
                }
            })
        })
    }
    $(".product_grid_row").each(function() {
        if ($(this).hasClass("prodGrid")) {
            $(this).removeClass("prodGrid");
            $(this).find(".prodheight").removeClass("prodheight")
        }
    })
}
$(".hoverClose").live("click", function() {
    $(this).parent(".product-hoverWrapper").css("display", "none");
    $(this).parents(".slimScrollDiv").css("display", "none");
    $(this).parents(".hoverProdCont").find(".hoverProductImage").css("opacity", "1");
    $(this).parents(".hoverProdCont").find(".bx-wrapper").css("background-color", "#fff");
    $(this).parents(".hoverProdCont").find(".outerImg").css("background-color", "#fff")
});
$(".hoverProductImage.product-image").each(function() {
    if ($(this).children(".soldout-tag").size() == 0 && $(this).parents(".productWrapper").find(".comingSoonTag").size() == 0 && $(this).parents(".productWrapper").find(".discontinuedGridTag").size() == 0 && $(this).children("a").length > 1) {
        var A = $(this).bxSlider({
            auto: false,
            pause: 2000,
            easing: "swing",
            prevText: "",
            nextText: "",
            mode: "fade",
            pager: false,
            autoHover: false,
            adaptiveHeight: true,
            controls: true,
        });
        $(this).parent().siblings().css("display", "none");
        A.parent().siblings().click(function() {
            if (A.find("img[hoverSrc]").size() > 0) {
                A.find("img[hoverSrc]").each(function() {
                    $(this).attr("src", $(this).attr("hoverSrc"));
                    $(this).removeAttr("hoverSrc")
                })
            }
        })
    }
});
$(".product_grid_box").live("mouseover", function() {
    $(this).find(".bx-wrapper .bx-has-controls-direction").css("display", "block")
});
$(".product_grid_box").live("mouseout", function() {
    $(this).find(".bx-wrapper .bx-has-controls-direction").css("display", "none")
});

function getGridHoverProductHtml(E, A, D, H) {
    getStateProperties(E);
    window.showState = true;
    if (gridForFashionFlag == "true") {
        if (!stateProperties.soldOut) {
            iArrAttr.push(E.id)
        }
        if (stateProperties.soldOut) {
            arrSoldOut.push(E.id)
        }
        if (!stateProperties.soldOut && stateProperties.preOrBuy && E.offers[0].images.length > 1) {
            Arr.push(E.id)
        }
    }
    var G = "";
    G = E.productTag;
    if (G != undefined && G != null && G != "") {
        imageUrl = "";
        var B = ""
    }
    var F = "";
    F += '<div class="product_grid_cont ' + (gridForFashionFlag == "true" ? "hoverProdCont " : "hoverProdCont4Grid ") + " prodheight prodGridLayout gridLayout" + gridViewLayout + '" id="' + E.id + '"><div class="product_grid_box">';
    if (gridForFashionFlag == "true") {
        F += '<div class="product-hoverWrapper sdScroll" style="display:none;">';
        F += '<div class="hoverClose"></div>';
        if (!stateProperties.comingSoon && !stateProperties.soldOut && !stateProperties.discontinued) {
            if (E.initAttributesFull.length != 0) {
                F += '<div id="pb" class="prod-buy-outer">';
                F += '<div class="product-attr-outer">';
                F += '<div id="attribute-error-message-' + E.id + '" style="display: none" class="error">Please select attribute(s)</div>';
                F += '<div style="display:none;" id="productAttributesJson-' + E.id + '">';
                F += JSON.stringify(E.initAttributesFull);
                F += "</div>";
                F += '<div id="product-option-' + E.id + '" class="productAttr prod-' + E.id + '">';
                F += '<script type="text/javascript">createAttributesGrid("' + E.id + '","true")<\/script>';
                F += "</div>";
                F += "</div>";
                F += "</div>"
            }
        }
        F += "</div>"
    }
    if (!stateProperties.soldOut) {
        F += '<div class="hoverWish wishOnProduct"' + (!stateProperties.live ? 'style="left:50%;margin-left:-39.5px"' : "") + 'pog="' + E.id + '" data-selected="false"><i class="icon-grey-heart"></i><span class="fnt11 wish-text">' + langSpecificLabels["label.categoryPage.addToWishlist"] + "</span></div>"
    }
    F += '<div class="hoverQuick quickView-wrapper" quickviewid="' + E.id + '"><span class="quickViewImage"></span><span class="fnt11 quickView">' + langSpecificLabels["label.categoryPage.quickView"] + " </span></div>";
    if (stateProperties.live) {
        if (stateProperties.buynow && !stateProperties.soldOut) {
            F += '<div class="buybutton">';
            F += '<a href="javascript:void(0);" class="buyBlueButton ' + (gridForFashionFlag == "true" ? "addToCartOnGrid " : "addToCartOn4Grid ") + " buylink-" + E.id + " prodlistBuy-" + E.id + '" catalog="' + ((stateProperties.attrPresent == "true" && gridForFashionFlag == "true") ? "undefined" : E.offers[0].id) + '" vendorCode="' + ((stateProperties.attrPresent == "true" && gridForFashionFlag == "true") ? "undefined" : E.vendorCode) + '" supc="' + ((stateProperties.attrPresent == "true" && gridForFashionFlag == "true") ? "undefined" : E.offers[0].supcs[0]) + '" prebook="true"  shippingCharges="' + E.shippingCharges + '" id="listBuy-' + E.id + '" onclick="return onBuyClick(' + E.id + ');">';
            F += '<div><span class="cart"></span>';
            F += '<span class="buyLabel"><label class="lang">' + langSpecificLabels["label.categoryPage.listBuyBtn"] + "</label></span>";
            F += "</div></a></div>"
        } else {
            if (stateProperties.prebook && !stateProperties.soldOut) {
                F += '<div class="buybutton">';
                F += '<a href="javascript:void(0);" class="hoverGrid buyBlueButton pre-order ' + (gridForFashionFlag == "true" ? "addToCartOnGrid " : "addToCartOn4Grid ") + " buylink-" + E.id + " prodlistBuy-" + E.id + '" catalog="' + ((stateProperties.attrPresent == "true" && gridForFashionFlag == "true") ? "undefined" : E.offers[0].id) + '" vendorCode="' + ((stateProperties.attrPresent == "true" && gridForFashionFlag == "true") ? "undefined" : E.vendorCode) + '" supc="' + ((stateProperties.attrPresent == "true" && gridForFashionFlag == "true") ? "undefined" : E.offers[0].supcs[0]) + '" prebook="true" shippingCharges="' + E.shippingCharges + '" id="listBuy-' + E.id + '" onclick="return onBuyClick(' + E.id + ');">Pre-Order </a>';
                F += "</div>"
            }
        }
    }
    window.showTag = true;
    F += getGridHoverProductContHtml(E, A, D, H);
    if (!stateProperties.discontinued && !stateProperties.comingSoon) {
        var C = window.compareFlag || $("#compareFlag").val();
        if (C == "true") {
            F += '<div class="product-txtWrapper attriWrapper">';
            F += '<div class="sdCheckbox addToCompare"><input type="checkbox" id="checkboxAddCompare-' + E.id + '" onClick="addToCompareClick(this)"/><label for="checkboxAddCompare-' + E.id + '" class="lang">' + langSpecificLabels["label.categoryPage.addToCompare"] + "</label></div>";
            F += "</div>"
        }
    }
    F += "</div></div>";
    $(document).trigger("loadLazyImages");
    return F
}
$(".hoverProdCont").live("mouseover", function() {
    $(this).find(".buybutton, .hoverWish").css("display", "block");
    $(this).find(".product-title, .product-price span#disc, .product-price span#preOrd").css("display", "none");
    $(this).find(".cashBack, .freebie-ico, .ratingsWrapper").css("display", "none");
    $(this).find(".product-attrWrapper").css("visibility", "visible");
    $(this).css("box-shadow", "0px 0px 1px #000 inset");
    $(this).css("-moz-box-shadow", "0px 0px 1px #000 inset");
    $(this).css("-webkit-box-shadow", "0px 0px 1px #000 inset");
    if ($(this).hasClass("gridLayout3")) {
        $(this).find(".product-attrWrapper").css("position", "absolute").css("top", 60);
        $(this).find("#prodDetails").css("position", "absolute").css("top", 36)
    } else {
        if ($(this).hasClass("gridLayout4")) {
            $(this).find(".product-attrWrapper").css("position", "absolute").css("top", 54);
            $(this).find("#prodDetails").css("position", "absolute").css("top", 30)
        }
    }
});
$(".hoverProdCont").live("mouseout", function() {
    if ($(this).find(".product-hoverWrapper").is(":hidden")) {
        $(this).find(".buybutton, .hoverWish").css("display", "none");
        $(this).find(".product-title, .product-price span#disc, .product-price span#preOrd").css("display", "block");
        $(this).find(".cashBack, .freebie-ico, .ratingsWrapper").css("display", "block");
        $(this).find(".product-attrWrapper").css("visibility", "hidden");
        $(this).css("box-shadow", "none");
        $(this).css("-moz-box-shadow", "none");
        $(this).css("-webkit-box-shadow", "none");
        $(this).find("#prodDetails").css("position", "relative").css("top", "initial");
        $(this).find(".product-attrWrapper").css("position", "relative").css("top", "initial")
    } else {
        if ($(this).find(".product-hoverWrapper").is(":visible")) {
            $(this).find(".buybutton, .hoverWish").css("display", "block");
            $(this).find(".product-title, .product-price span#disc, .product-price span#preOrd").css("display", "none");
            $(this).find(".cashBack, .freebie-ico, .ratingsWrapper").css("display", "none");
            $(this).find(".product-attrWrapper").css("visibility", "visible");
            $(this).css("box-shadow", "0px 0px 1px #000 inset");
            $(this).css("-moz-box-shadow", "0px 0px 1px #000 inset");
            $(this).css("-webkit-box-shadow", "0px 0px 1px #000 inset");
            if ($(this).hasClass("gridLayout3")) {
                $(this).find(".product-attrWrapper").css("position", "absolute").css("top", 60);
                $(this).find("#prodDetails").css("position", "absolute").css("top", 36)
            } else {
                if ($(this).hasClass("gridLayout4")) {
                    $(this).find(".product-attrWrapper").css("position", "absolute").css("top", 54);
                    $(this).find("#prodDetails").css("position", "absolute").css("top", 30)
                }
            }
        }
    }
});
$(".hoverProdCont4Grid").live("mouseover", function() {
    $(this).find(".buybutton, .hoverWish").css("display", "block");
    $(this).find(".product-title, .product-price span").css("display", "none");
    $(this).find(".freebie-ico, .ratingsWrapper").css("display", "none");
    var A = $(this).find("#prodDetails").height();
    $(this).find("#prodDetails").css("position", "absolute").css("top", 38);
    $(this).find(".product_list_view_highlights").css("position", "absolute").css("top", 38 + A);
    $(this).find(".hoverProductWrapper.product-txtWrapper.prodSoldoutTxt").css("top", "0px");
    $(this).find(".hoverProductWrapper.prodSoldoutTxt .product_list_view_highlights").css("top", "0px").css("position", "relative");
    $(this).find(".hoverProductWrapper.prodSoldoutTxt #prodDetails").css("top", "0px").css("position", "relative");
    $(this).css("box-shadow", "0px 0px 1px #000 inset");
    $(this).css("-moz-box-shadow", "0px 0px 1px #000 inset");
    $(this).css("-webkit-box-shadow", "0px 0px 1px #000 inset")
});
$(".hoverProdCont4Grid").live("mouseout", function() {
    $(this).find(".buybutton, .hoverWish").css("display", "none");
    $(this).find(".product-title, .product-price span").css("display", "block");
    $(this).find(".freebie-ico, .ratingsWrapper").css("display", "block");
    $(this).find("#prodDetails").css("position", "relative").css("top", 0);
    $(this).find(".product_list_view_highlights").css("position", "relative").css("top", 0);
    $(this).find(".hoverProductWrapper.product-txtWrapper.prodSoldoutTxt").css("top", "0px");
    $(this).css("box-shadow", "none");
    $(this).css("-moz-box-shadow", "none");
    $(this).css("-webkit-box-shadow", "none")
});

function getGridHoverProductContHtml(N, B, J, A, P) {
    if (window.stateProperties && !(stateProperties.set == true)) {
        getStateProperties(N);
        stateProperties.set = false
    }
    if (window.showTag == true) {
        var F = "";
        F = N.productTag;
        var D;
        if (window.jsonMapForTags) {
            if (D = jsonMapForTags[F]) {
                imageUrl = D.tagImageUrl;
                var E = "";
                if (D.offer == true) {
                    E = D.offerDescription
                }
            }
        }
    }
    if (!P) {
        P = ""
    }
    var C = "";
    C += '<div class="productWrapper">';
    if ((stateProperties.comingSoon || stateProperties.discontinued) && !window.hPRecentlyWidget) {
        if (stateProperties.comingSoon) {
            C += '<div class="comingSoonTag statusTag"></div>'
        } else {
            if (stateProperties.discontinued) {
                C += '<div class="discontinuedGridTag">' + langSpecificLabels["label.productStatus.discontinued"] + "</div>"
            }
        }
    }
    if (!gpsf && (!isPersonalizedHP || window.displayHPTrendingProduct) && N.shippingDays != null && N.shippingDays == 1) {
        C += '<div class="shippingImage">';
        C += '<span class="tooltip">';
        C += '<label class="lang dispatched-one-day-lang">' + langSpecificLabels["label.categoryPage.dispatchedInOneBusinessDay"] + "</label>";
        C += "</span>";
        C += "</div>"
    }
    C += '<div class="outerImg"><div prodId="' + N.id + '" class="' + (gridForFashionFlag == "true" ? "hoverProductImage " : "") + " product-image " + (stateProperties.soldOut || stateProperties.discontinued ? "prodSoldout" : "") + '" alt="' + N.name + '" at1="' + N.name + '" >';
    if (gridForFashionFlag == "true") {
        if (N.offers.length != 0) {
            var O = 0;
            var H = 0;
            for (O = 0; O < N.offers.length; O++) {
                if (N.offers[O].images.length != 0) {
                    for (H = 0; H < N.offers[O].images.length; H++) {
                        C += '<a class="hit-ss-logger somn-track lazyBg prodLink ' + (H == 0 && O == 0 ? "" : " hidden ") + '" pos="' + B + ";" + A + '" v="p" isPartialSearch="' + partialSearch + '" categoryId="' + N.categoryId + '" href="' + Snapdeal.getAbsoluteStaticPath(N.pageUrl) + '" pogId="' + N.id + '" hidOmnTrack="' + P + '">';
                        if (!(stateProperties.comingSoon || stateProperties.discontinued)) {
                            if (window.showTag == true) {
                                if (D && imageUrl) {
                                    C += '<div class ="productTagImage" tagName="' + F + '">';
                                    C += '<img src="' + Snapdeal.getResourcePath(N.offers[O].images[H]).replace("166x194", prodGridImgSizeCust) + '"  data-attr="' + N.offers[O].images[H] + '" title ="' + E + '" />';
                                    C += "</div>"
                                }
                                window.showTag == false
                            }
                        }
                        if (H != 0) {
                            C += '<img class="" anim="true" hoverSrc="' + Snapdeal.getResourcePath(N.offers[O].images[H]).replace("166x194", prodGridImgSizeCust) + '" border="0"alt="' + N.name + '" />'
                        } else {
                            C += '<img src="' + Snapdeal.getResourcePath(N.offers[O].images[H]).replace("166x194", prodGridImgSizeCust) + '" border="0" alt="' + N.name + '" />'
                        }
                        C += "</a>"
                    }
                }
            }
        }
    } else {
        if (gridForElectronicsFlag == "true") {
            C += '<a class="hit-ss-logger somn-track prodLink" pos="' + B + ";" + A + '" v="p" isPartialSearch="' + partialSearch + '" categoryId="' + N.categoryId + '" href="' + Snapdeal.getAbsoluteStaticPath(N.pageUrl) + '" pogId="' + N.id + '" hidOmnTrack="' + P + '">';
            if (!(stateProperties.comingSoon || stateProperties.discontinued)) {
                if (window.showTag == true) {
                    if (D && imageUrl) {
                        C += '<div class ="productTagImage" tagName="' + F + '">';
                        C += '<img src="' + imageUrl + '"  data-attr="' + imageUrl + '" title ="' + E + '" /></li></ul>';
                        C += "</div>"
                    }
                    window.showTag == false
                }
            }
            if (J != 1) {
                C += '<img class="" anim="true" lazySrc="' + Snapdeal.getResourcePath(N.image).replace("166x194", prodGridImgSizeCust) + '" src="" border="0"alt="' + N.name + '" title="' + N.name + '" />'
            } else {
                C += '<img src="' + Snapdeal.getResourcePath(N.image).replace("166x194", prodGridImgSizeCust) + '" border="0" alt="' + N.name + '" />'
            }
            C += "</a>"
        }
    }
    C += "" + ((stateProperties.soldOut && !stateProperties.notAvaillableAtTheMoment) == true ? '<div class="soldout-tag prod-grid-sold-out-lang">' + langSpecificLabels["label.categoryPage.soldOut"] + "</div>" : "") + '</div></div><div class="hoverProductWrapper product-txtWrapper ' + (stateProperties.soldOut || stateProperties.discontinued ? "prodSoldoutTxt" : "") + '" >';
    if (window.hPRecentlyWidget) {
        if (stateProperties.comingSoon) {
            C += '<div class="redText">' + langSpecificLabels["label.productStatus.comingSoon"] + "</div>"
        } else {
            if (stateProperties.discontinued) {
                C += '<div class="redText">' + langSpecificLabels["label.productStatus.discontinued"] + "</div>"
            } else {
                if (stateProperties.notAvaillableAtTheMoment) {
                    C += '<div class="redText">' + langSpecificLabels["label.productStatus.notAvailableAtTheMoment"] + "</div>"
                }
            }
        }
        window.hPRecentlyWidget = false
    }
    C += '<div class="product-title">';
    C += '<a class="hit-ss-logger somn-track hell prodLink" pos="' + B + ";" + A + '" v="p" isPartialSearch="' + partialSearch + '" categoryId="' + N.categoryId + '" href="' + Snapdeal.getAbsoluteStaticPath(N.pageUrl) + '" pogId="' + N.id + '" hidOmnTrack="' + P + '">';
    if (window.characterLength != "" && window.characterLength != undefined) {
        C += sanitizeProductName(N.name, characterLength)
    } else {
        C += N.name
    }
    C += "</a></div>";
    if (!stateProperties.comingSoon) {
        C += '<a id="prodDetails" rel="nofollow" class="hit-ss-logger somn-track prodLink" pos="' + B + ";" + A + '" v="p" isPartialSearch="' + partialSearch + '" categoryId="' + N.categoryId + '" href="' + Snapdeal.getAbsoluteStaticPath(N.pageUrl) + '" pogId="' + N.id + '" hidOmnTrack="' + P + '">';
        if ((!isPersonalizedHP || window.displayHPTrendingProduct) && N.noOfReviews > 0) {
            C += '<div class="ratingsWrapper"><div class="ratingStarsSmall" style="background-position: 0px -' + imageOffsetForRating(N.avgRating) * 10 + 'px;"></div><div class="lfloat"> (' + N.noOfReviews + ") </div></div>"
        }
        C += '<div class="product-price">';
        if (N.prebook && !N.launched) {
            C += '<div class="fnt12 prod-prebook"><span id="preOrd">Pre Order at</span><strong class="blackText">Rs ' + N.displayPrice + "</strong></div>"
        } else {
            if ((!isPersonalizedHP || window.displayHPTrendingProduct) && N.price != null && N.voucherPrice != null) {
                if (gridForFashionFlag == "true") {
                    C += '<div style="display:inline-flex;"><span id="price">Rs ' + N.displayPrice + '</span>&nbsp;&nbsp;<span id="disc" class="' + (N.discount > 0 ? "" : "invisible") + '"><strike>Rs ' + N.price + "</strike> <s>(" + N.discount + "%)</s></span></div>"
                } else {
                    if (gridForElectronicsFlag == "true") {
                        C += "<div>Rs " + N.displayPrice + '<span class="' + (N.discount > 0 ? "" : "invisible") + '"><strike>Rs ' + N.price + "</strike> <s>(" + N.discount + "%)</s></span></div>"
                    }
                }
            } else {
                if (isPersonalizedHP && N.price != null && N.voucherPrice != null) {
                    C += "<div>Rs " + N.displayPrice + "</div>"
                }
            }
        } if (gridForElectronicsFlag == "true") {
            if (N.displayPrice >= emi3MonthHoverJS) {
                C += '<div class="emiMonthsHoverGrid">EMI starts at Rs.' + N.emi + " </div>"
            }
        }
        if ((!isPersonalizedHP || window.displayHPTrendingProduct) && N.externalCashBackApplicable == true && N.cashBackValue > 0) {
            var K = N.cashBackValue.toString().split(".");
            if (N.cashBackType == "PCT") {
                if (K[1] > 1) {
                    C += '<div class="cashBack">Insta Cashback:&nbsp;' + N.cashBackValue + "%</div>"
                } else {
                    C += '<div class="cashBack">Insta Cashback:&nbsp;' + K[0] + "%</div>"
                }
            }
            if (N.cashBackType == "ABS") {
                C += '<div class="cashBack">Insta Cashback:&nbsp;Rs ' + K[0] + "</div>"
            }
        }
        C += "</div>";
        if ((!isPersonalizedHP || window.displayHPTrendingProduct) && N.freebies != null && N.freebies.length > 0) {
            C += '<div class="freebie-ico"><i class="list-gridIcons freebieIcon"></i><label class="lang freebie-lang">' + langSpecificLabels["label.categoryPage.freebie"] + "</label></div>"
        }
        C += "</a>"
    }
    if (gridForElectronicsFlag == "true") {
        if (highlightsEnabledFlag == true || highlightsEnabledFlag == "true") {
            if (N.highlights && typeof isWishlistPage == "undefined" && !window.displayHPTrendingProduct) {
                var M = "";
                M += '<div class="lfloat product_list_view_highlights">';
                M += '<div class="product_list_view_heading"></div>';
                M += '<div class="key-features-outer">';
                M += '<ul class="key-features">';
                if (N.highlights.length != undefined) {
                    var Q = (N.highlights.length > 4 ? 4 : N.highlights.length);
                    for (var H = 0; H < Q; H++) {
                        M += "<li>" + N.highlights[H] + "</li>"
                    }
                } else {
                    M += "<li>" + N.highlights + "</li>"
                }
                M += "</ul>";
                M += "</div>";
                M += "</div>";
                if (N.highlights.length != undefined && N.highlights.length == 0) {} else {
                    C += M
                }
            }
        }
    }
    if (gridForFashionFlag == "true") {
        if (!stateProperties.comingSoon && !stateProperties.soldOut && !stateProperties.discontinued) {
            if (N.initAttributesFull.length != 0) {
                C += '<div class="product-attrWrapper" style="visibility:hidden;padding-left:0px;">';
                C += '<span class="attrName">' + N.initAttributesFull[0].name + " : </span>";
                if (N.initAttributesFull[0].name != "Color" && N.initAttributesFull[0].name != "color" && N.initAttributesFull[0].name != "Colour" && N.initAttributesFull[0].name != "colour") {
                    for (var H = 0; H < N.initAttributesFull.length; H++) {
                        if (N.initAttributesFull[H].soldOut == false) {
                            C += '<span class="attrValue">' + N.initAttributesFull[H].value + " <i>-</i></span>"
                        } else {
                            C += '<span class="attrValue soldOutAttr"><strike>' + N.initAttributesFull[H].value + "</strike> <i>-</i></span>"
                        }
                    }
                } else {
                    if (N.initAttributesFull[0].name == "Color" || N.initAttributesFull[0].name == "color" || N.initAttributesFull[0].name == "Colour" || N.initAttributesFull[0].name == "colour") {
                        for (var H = 0; H < N.initAttributesFull.length; H++) {
                            if (N.initAttributesFull[H].soldOut == false) {
                                C += '<span class="attrValue" style="display:none;">' + N.initAttributesFull[H].value + "</span>";
                                if (N.initAttributesFull[H].value != "Multi" && N.initAttributesFull[H].value != "White" && N.initAttributesFull[H].value != "white" && N.initAttributesFull[H].value != "multi") {
                                    C += '<span class="attrValueSpan" style="background:' + N.initAttributesFull[H].value.replace(/\ /g, "") + '"></span>'
                                } else {
                                    if (N.initAttributesFull[H].value == "Multi" || N.initAttributesFull[H].value == "multi") {
                                        C += '<span class="attrValueSpan Multi"></span>'
                                    } else {
                                        if (N.initAttributesFull[H].value == "White" || N.initAttributesFull[H].value == "white") {
                                            C += '<span class="attrValueSpan white" style="background:' + N.initAttributesFull[H].value.replace(/\ /g, "") + '"></span>'
                                        }
                                    }
                                }
                            } else {
                                C += '<span class="attrValue" style="display:none;">' + N.initAttributesFull[H].value + "</span>";
                                if (N.initAttributesFull[H].value != "Multi" && N.initAttributesFull[H].value != "White" && N.initAttributesFull[H].value != "white" && N.initAttributesFull[H].value != "multi") {
                                    C += '<strike><span class="attrValueSpan soldOutAttr" style="background:' + N.initAttributesFull[H].value.replace(/\ /g, "") + '"></span></strike>'
                                } else {
                                    if (N.initAttributesFull[H].value == "Multi" || N.initAttributesFull[H].value == "multi") {
                                        C += '<strike><span class="attrValueSpan soldOutAttr Multi"></span></strike>'
                                    } else {
                                        if (N.initAttributesFull[H].value == "White" || N.initAttributesFull[H].value == "white") {
                                            C += '<strike><span class="attrValueSpan soldOutAttr white" style="background:' + N.initAttributesFull[H].value.replace(/\ /g, "") + '"></span></strike>'
                                        }
                                    }
                                }
                            }
                        }
                    }
                } if (N.initAttributesFull[0].subAttributes.length != 0) {
                    C += "<div>";
                    C += '<span class="subAttrName">' + N.initAttributesFull[0].subAttributes[0].name + " : </span>";
                    if (N.initAttributesFull[0].subAttributes[0].name != "Color" && N.initAttributesFull[0].subAttributes[0].name != "color" && N.initAttributesFull[0].subAttributes[0].name != "Colour" && N.initAttributesFull[0].subAttributes[0].name != "colour") {
                        var I = new Array();
                        for (var H = 0; H < N.initAttributesFull.length; H++) {
                            if (N.initAttributesFull[H].subAttributes.length != 0) {
                                for (var G = 0; G < N.initAttributesFull[H].subAttributes.length; G++) {
                                    if (I.indexOf(N.initAttributesFull[H].subAttributes[G].value) < 0) {
                                        I.push(N.initAttributesFull[H].subAttributes[G].value);
                                        if (N.initAttributesFull[H].subAttributes[G].soldOut == false) {
                                            C += '<span class="subAttrValue">' + N.initAttributesFull[H].subAttributes[G].value + " <i>-</i></span>"
                                        } else {
                                            C += '<span class="subAttrValue soldOutAttr"><strike>' + N.initAttributesFull[H].subAttributes[G].value + "</strike> <i>-</i></span>"
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (N.initAttributesFull[0].subAttributes[0].name == "Color" || N.initAttributesFull[0].subAttributes[0].name == "color" || N.initAttributesFull[0].subAttributes[0].name == "Colour" || N.initAttributesFull[0].subAttributes[0].name == "colour") {
                            var L = new Array();
                            for (var H = 0; H < N.initAttributesFull.length; H++) {
                                if (N.initAttributesFull[H].subAttributes.length != 0) {
                                    for (var G = 0; G < N.initAttributesFull[H].subAttributes.length; G++) {
                                        if (L.indexOf(N.initAttributesFull[H].subAttributes[G].value) < 0) {
                                            L.push(N.initAttributesFull[H].subAttributes[G].value);
                                            if (N.initAttributesFull[H].subAttributes[G].soldOut == false) {
                                                C += '<span class="subAttrValue" style="display:none;">' + N.initAttributesFull[H].subAttributes[G].value + "</span>";
                                                if (N.initAttributesFull[H].subAttributes[G].value != "Multi" && N.initAttributesFull[H].subAttributes[G].value != "White" && N.initAttributesFull[H].subAttributes[G].value != "white" && N.initAttributesFull[H].subAttributes[G].value != "multi") {
                                                    C += '<span class="subAttrValueSpan" style="background:' + N.initAttributesFull[H].subAttributes[G].value.replace(/\ /g, "") + '"></span>'
                                                } else {
                                                    if (N.initAttributesFull[H].subAttributes[G].value == "Multi" || N.initAttributesFull[H].subAttributes[G].value == "multi") {
                                                        C += '<span class="subAttrValueSpan Multi"></span>'
                                                    } else {
                                                        if (N.initAttributesFull[H].subAttributes[G].value == "White" || N.initAttributesFull[H].subAttributes[G].value == "white") {
                                                            C += '<span class="subAttrValueSpan white" style="background:' + N.initAttributesFull[H].subAttributes[G].value.replace(/\ /g, "") + '"></span>'
                                                        }
                                                    }
                                                }
                                            } else {
                                                C += '<span class="subAttrValue" style="display:none;">' + N.initAttributesFull[H].subAttributes[G].value + "</span>";
                                                if (N.initAttributesFull[H].subAttributes[G].value != "Multi" && N.initAttributesFull[H].subAttributes[G].value != "White" && N.initAttributesFull[H].subAttributes[G].value != "white" && N.initAttributesFull[H].subAttributes[G].value != "multi") {
                                                    C += '<strike><span class="subAttrValueSpan soldOutAttr" style="background:' + N.initAttributesFull[H].subAttributes[G].value.replace(/\ /g, "") + '"></span></strike>'
                                                } else {
                                                    if (N.initAttributesFull[H].subAttributes[G].value == "Multi" || N.initAttributesFull[H].subAttributes[G].value == "multi") {
                                                        C += '<strike><span class="subAttrValueSpan soldOutAttr Multi"></span></strike>'
                                                    } else {
                                                        if (N.initAttributesFull[H].subAttributes[G].value == "White" || N.initAttributesFull[H].subAttributes[G].value == "white") {
                                                            C += '<strike><span class="subAttrValueSpan soldOutAttr white" style="background:' + N.initAttributesFull[H].subAttributes[G].value.replace(/\ /g, "") + '"></span></strike>'
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    C += "</div>"
                }
                C += "</div>"
            }
        }
    }
    C += "</div></div>";
    return C
}

function getQuickviewWishlist(A) {
    var B = "";
    B += '<div class="wishOnProduct" pog="' + A.id + '" data-selected="false"><i class="icon-grey-heart"></i><span class="fnt11 wish-text">' + langSpecificLabels["label.categoryPage.addToWishlist"] + "</span></div>";
    B += '<div class="quickView-wrapper" quickViewId="' + A.id + '"><span class="fnt11">' + langSpecificLabels["label.categoryPage.quickView"] + " </span></div>";
    return B
}

function initSdScrollHover(B) {
    var A = parseInt((B.css("maxHeight") || "").split("px")[0]) || 0;
    if (B.height() == A) {
        B.slimScroll({
            height: A,
            alwaysVisible: true,
            railVisible: true,
            size: "14px",
            color: "#c4c4c4",
            opacity: 1,
            railColor: "#f4f4f4",
            railOpacity: 1,
            borderRadius: 3,
            railBorderRadius: 3
        });
        B.css("bottom", "0px");
        B.parent().css("display", "block")
    }
}

function generateHeight() {
    $(".product_grid_row.prodGrid").each(function() {
        if ($(this).find(".hoverProdCont4Grid").size() > 0) {
            var A = $(this).find(".hoverProdCont4Grid").eq(0).find(".hoverProductWrapper").height() + $(this).find(".hoverProdCont4Grid").eq(0).find(".product_list_view_highlights").height() + $(this).find(".hoverProdCont4Grid").eq(0).find(".addToCompare").height() + $(this).find(".hoverProdCont4Grid").eq(0).find(".emiMonthsHoverGrid").height();
            $(this).find(".hoverProductWrapper").each(function() {
                var B = $(this).height() + $(this).find(".product_list_view_highlights").height() + $(this).find(".emiMonthsHoverGrid").height() + $(this).parents(".hoverProdCont4Grid").find(".addToCompare").height();
                if (B > A) {
                    A = B
                }
            });
            $(this).find(".hoverProductWrapper").each(function() {
                $(this).css("height", A + 5)
            })
        }
    });
    $(".product_grid_row").each(function() {
        if ($(this).hasClass("prodGrid")) {
            $(this).removeClass("prodGrid")
        }
    })
}

function subLevelCheck(B) {
    for (var A = 0; A < B.length; A++) {
        if (B[A].live) {
            return true
        }
    }
    return false
}

function onBuyClick(A) {
    var B = A;
    if ($("#listBuy-" + B).attr("href").indexOf("catalog=undefined") != -1) {
        $("#attribute-error-message-" + B).show();
        return false
    }
    return true
};

function getTrackingParams() {
    var A = "";
    var K = "";
    var J = window.document.URL.toString();
    if (J.indexOf("?") > 0) {
        var O = J.split("?");
        K = O[1].split("&")
    }
    var L = $("#aff").val();
    var P = ["utm_source", "utm_content", "utm_medium", "utm_campaign", "ref", "utm_term"];
    var D = ["null", "null", "null", "null", "null", "null"];
    var Q = P.length;
    var F = 0;
    var B = "";
    var H = 1;
    for (F = 0; F < K.length; F++) {
        var C = K[F].split("=");
        var E = 0;
        for (E = 0; E < Q; E++) {
            if (C[0] == P[E]) {
                if (C[1] != "") {
                    D[E] = C[1]
                }
                break
            }
        }
        if (C[0] == "subid") {
            B = C[1]
        }
    }
    var M = document.referrer;
    for (F = 0; F < Q; F++) {
        if (P[F] == "utm_source" && D[F] == "null") {
            if (M != "" && (M.toLowerCase().indexOf("google") > -1 || M.toLowerCase().indexOf("yahoo") > -1 || M.toLowerCase().indexOf("bing") > -1)) {
                D[F] = "SEO";
                D[1] = null;
                D[2] = null;
                D[3] = null;
                D[4] = null;
                D[5] = null
            } else {
                if (M != "" && M.toLowerCase().indexOf("snapdeal") == -1) {
                    D[F] = "INLINK";
                    D[1] = null;
                    D[2] = null;
                    D[3] = null;
                    D[4] = null;
                    D[5] = null
                } else {
                    D[F] = "DIRECT";
                    H = 0
                }
            }
        }
        if (P[F] == "utm_content") {
            if (M != "" && (M.toLowerCase().indexOf("google") > -1 || M.toLowerCase().indexOf("yahoo") > -1)) {
                var N = decodeURIComponent(document.referrer);
                var I = /&q=(.*?)&/gi;
                I = I.compile(I);
                var G = N.match(I);
                if (G[1]) {
                    D[F] = G[1]
                }
            }
        }
        if (L) {
            if (P[F] == "utm_source") {
                D[F] = "CORPORATE"
            } else {
                if (P[F] == "utm_campaign") {
                    D[F] = L
                }
            }
        }
        if (P[F] == "ref" && B != "") {
            D[F] = B
        }
        A = A + P[F] + "=" + D[F] + "|"
    }
    if (Snapdeal.Cookie.get("st") == null) {
        Snapdeal.Cookie.set("st", A, 90, "/", ".snapdeal.com");
        Snapdeal.Cookie.set("vt", A, 1 / 48, "/", ".snapdeal.com")
    } else {
        if (H == 1 || Snapdeal.Cookie.get("vt") == null) {
            Snapdeal.Cookie.set("vt", A, 1 / 48, "/", ".snapdeal.com")
        } else {
            paramsFromCookie = Snapdeal.Cookie.get("vt");
            Snapdeal.Cookie.set("vt", paramsFromCookie, 1 / 48, "/", ".snapdeal.com")
        }
    } if (Snapdeal.Cookie.get("lt") == null) {
        Snapdeal.Cookie.set("lt", A, 30, "/", ".snapdeal.com")
    } else {
        if (H == 1) {
            Snapdeal.Cookie.set("lt", A, 30, "/", ".snapdeal.com")
        } else {
            ltParamsFromCookie = Snapdeal.Cookie.get("lt");
            Snapdeal.Cookie.set("lt", ltParamsFromCookie, 30, "/", ".snapdeal.com")
        }
    }
};

function loadFiles(C) {
    if ($("#" + C).length == 0) {
        return
    }
    deferedScript = $("#" + C).html().replace("<!--COMMENTSTARTED", "").replace("COMMENTENDED-->", "");
    var B = decodeURIComponent(deferedScript.replace(/\+/g, "%20"));
    finalString = B.replace(/<script/g, "<script");
    (function A(D) {
        try {
            var G = D.split("<\/script>");
            var E = 0;
            $.ajaxSetup({
                cache: true
            });

            function F() {
                if (E > G.length) {
                    if (window.afterDeferred) {
                        window.afterDeferred();
                        window.afterDeferred = function() {}
                    }
                    return
                }
                var J = G[E];
                E++;
                if (!$.trim(J)) {
                    F();
                    return
                }
                var K = J + "<\/script>";
                var I;
                K.replace(/<script([^>]+)>/, function(L) {
                    L.replace(/src=["'](.*)["']/, function(M, N) {
                        I = N
                    })
                });
                document.write = function(L) {
                    A(L)
                };
                if ($.trim(I)) {
                    $.getScript($.trim(I), function() {
                        F()
                    })
                } else {
                    $("#deferredScript").append(K);
                    F()
                }
            }
            $("#deferredScript").ajaxError(function(K, L, J, I) {
                if (J.dataType == "script") {
                    F()
                }
            });
            F()
        } catch (H) {}
    })(finalString)
}
$(window).load(function() {
    if (loadingTime == 0) {
        loadingTime = (new Date().getTime() - startTime) / 1000
    }
    loadFiles("deferredScript");
    $("#deferredScript").bind("loaded", function() {
        loadFiles("deferredScript2")
    });
    $(".lazyBg").removeClass("lazyBg")
});
Snapdeal = Snapdeal || {};
Snapdeal.sellerChat = (function() {
    var H = "";
    var E = "";
    var B = false;
    var G = function(I) {
        this.isSellerChatEnabled = I
    };
    var F = function() {
        return this.isSellerChatEnabled
    };
    var D = function() {
        miootChat.Initialize()
    };
    var C = function(J, I) {
        this.uName = J;
        this.email = I
    };
    var A = function() {
        return [this.uName, this.email]
    };
    return {
        setIsSellerChatEnabled: G,
        getIsSellerChatEnabled: F,
        enableChatDiv: D,
        setPersonalDetails: C,
        getPersonalDetails: A
    }
})();
$(window).load(function() {
    if (Snapdeal.sellerChat.getIsSellerChatEnabled()) {
        var A = $(".chatWithSeller div");
        A.attr("buyername", (Snapdeal.sellerChat.getPersonalDetails()[0] || ""));
        A.attr("buyeremail", (Snapdeal.sellerChat.getPersonalDetails()[1] || ""));
        Snapdeal.sellerChat.enableChatDiv()
    }
});
