<?php

namespace App;

use Illuminate\Contracts\Auth\Guard;
use App\Repositories\UserRepository;
use Laravel\Socialite\Contracts\Factory as Socialite;

class AuthenticateUser {

    private $users;
    private $socialite;
    private $auth;

    public function __construct(UserRepository $users, Socialite $socialite, Guard $auth) {
        $this->users = $users;
        $this->socialite = $socialite;
        $this->auth = $auth;
    }

    public function execute($hasCode, AuthenticateUserListener $listener, $provider = null) {

        if (!$hasCode) {
            return $this->getAutherizationFirst($provider);
        }
        $user = $this->users->findByUsernameOrCreate($this->getSocialUser($provider), $provider);
        $this->auth->login($user, true);
        return $listener->userHasLoggedIn($user);
    }

    public function logout() {
        $this->auth->logout();
        return redirect('/');
    }

    private function getAutherizationFirst($provider) {
        return $this->socialite->driver($provider)->redirect();
    }

    private function getFacebookUser($provider) {
        return $this->socialite->with($provider)->user();
    }

    private function getSocialUser($provider) {
        return $this->socialite->driver($provider)->user();
    }

}
