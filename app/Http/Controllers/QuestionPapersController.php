<?php namespace App\Http\Controllers;
use App\Http\Requests\UploadQpRequest;
use Request;
use Response;
use Input;
use Storage;


////use App\Http\Controllers\Controller;
//use Illuminate\Routing\Controller;
use App\QuestionPapers;
use DB;


class QuestionPapersController extends Controller {

        private $qpapers;
	
	protected $qpform;
       
        function __construct(QuestionPapers $qpapers){
	   $this->qpapers = $qpapers;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
             $query = Input::get('q','');
	    
	      $_token = csrf_token();        

		$str ='';
	$years = array();
	 for($i=2005;$i<date("Y");$i++){
	  $years[$i] = $i;   
	 }
	 $syllabas = DB::table('syllabas')->orderBy('name', 'asc')->lists('name','id');
         $classes = DB::table('class')->orderBy('name', 'asc')->lists('name','id');
	   $question_papers = $this->qpapers->paginate(7);
	  
	   if (!empty($query) && Request::ajax()) {
		
	   $question_query = $this->qpapers->Orderby( 'id', 'desc' );
            
            foreach($query as $key=>$value){
                if(!empty($value)|| $value!=''){
		$wherein_value = explode(',',$value);
                $question_query->whereIn($key,$wherein_value);
		$str .= $key.'='.$value;
		$str .='&';
		}
            }

	    $question_papers = $question_query->paginate(7);

	   return Response::json(view('qpapers.items', array('question_papers'=>$question_papers,'q'=>$query,'token'=>$_token))->render());
	   }else if (Request::ajax()) {
            return Response::json(view('qpapers.items', array('question_papers'=>$question_papers,'q'=>$query,'token'=>$_token))->render());
        }
	   return view('qpapers.index',array('question_papers'=>$question_papers,'token'=>$_token,'q'=>'','syllabas'=>$syllabas,'classes'=>$classes,'years'=>$years)); 
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	 $years = array();
	 $years[NULL] = '-- Select --';
	 for($i=2005;$i<date("Y");$i++){
	  $years[$i] = $i;   
	 }
	 $syllabas = DB::table('syllabas')->orderBy('name', 'asc')->lists('name','id');
         $classes = DB::table('class')->orderBy('name', 'asc')->lists('name','id');
	  return view('qpapers.createqp',array('syllabas'=>$syllabas,'classes'=>$classes,'years'=>$years));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UploadQpRequest $request,QuestionPapers $qpapers)
	{


	    $file = $request->file('qpfile');
	    if($request->hasFile('qpfile')){
            if($file->isValid()){
	       $filename = time() . $file->getClientOriginalName();
//$file->move(public_path().'/uploads',$filename);
		   				   $disk = Storage::disk('s3');
		   //$exists = Storage::disk('s3')->exists($filename);
		   $disk->put($filename,file_get_contents($file));
	    }

	    $qpapers->qpfile = $filename;
	    }
	    $qpapers->title = $request->input('title');
	    $qpapers->type = $request->input('type');
	    $qpapers->year = $request->input('year');
	    $qpapers->syllabas = $request->input('syllabas');
	    $qpapers->class = $request->input('class');
	    
	    $qpapers->save();
            return redirect('/')->withNotification('Uploaded successfully');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	  $qpaper = $this->qpapers->whereId($id)->first();
	  
           return view('show',compact('qpaper'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(QuestionPapers $qpapers)
	{
	  $qpapers->delete();
	  die;
	  return redirect('/')->withNotification('Deleted successfully');;
	}

}
