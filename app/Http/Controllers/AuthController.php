<?php namespace App\Http\Controllers;

//use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AuthenticateUser;
use App\AuthenticateUserListener;
use Illuminate\Http\Request;

class AuthController extends Controller implements AuthenticateUserListener {

    
    /**
     * @param AuthenticateUser $authenticateUser
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function login(AuthenticateUser $authenticateUser, Request $request , $provider=null)
    {
        $hasCode = $request->has('code');
        return $authenticateUser->execute($hasCode,$this, $provider);
    }
    
    
    /**
     * When a user has successfully been logged in...
     *
     * @param $user
     * @return \Illuminate\Routing\Redirector
     */
    public function userHasLoggedIn($user)
    {
        return redirect('/');
    }
    
    public function logout(AuthenticateUser $authenticateUser){
        return $authenticateUser->logout();
    }

}
