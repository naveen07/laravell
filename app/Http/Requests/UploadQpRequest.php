<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UploadQpRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		return [
		    'title'   => 'required',
		    'type'   => 'required',
		    'year'    => 'required',
		    'syllabas'=> 'required',
		    'qpfile'  =>'required|mimes:doc,docx,pdf',
		     'g-recaptcha-response' => 'required|captcha'
		];
	}

	/**
	 * Get the sanitized input for the request.
	 *
	 * @return array
	 */
	public function sanitize()
	{
		return $this->all();
	}

}
