<?php
use DB;

function get_resourcename($table,$id){
    $name = DB::table($table)->where('id', $id)->pluck('name');
    return $name;
}