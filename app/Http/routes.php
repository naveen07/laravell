<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//get('/', function()
//{
//    if (Auth::check()) return redirect('/');
//    return redirect('login');
//});
Route::get('/', 'QuestionPapersController@index');
//get('/', function()
//{
//    if (Auth::check()) return 'Welcome back, '  . Auth::user()->username;
//    return 'Hi guest. ' . link_to('login', 'Login With facebook!');
//});

Route::get('home', 'HomeController@index');
Route::get('login','AuthController@login');
Route::get('logout','AuthController@logout');

Route::post('data-filter',function()
            {
            $query = Input::get('q','');
            
            $question_query = App\QuestionPapers::Orderby( 'id', 'desc' );
            foreach($query as $key=>$value){
                if(!empty($value)|| $value!='')
                $question_query->whereIn($key,[$value]);
            }
            $question_papers = $question_query->get();

            return Response::json(view('qpapers.items',array('question_papers'=>$question_papers))->render);
            });

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

 Route::get('login/{provider?}', 'AuthController@login');

Route::bind('qp',function($id)
            {
                return App\QuestionPapers::where('id',$id)->first();
            });

$router->resource('qp','QuestionPapersController',['only' => [
	'create','store','destroy'
       ]]);
