<?php

namespace App\Repositories;

use App\User;
use DB;

class UserRepository {

    public function findByUserNameOrCreate($userData, $provider) {

        $user = User::where('email', '=', $userData->email)->first();

        if (!$user) {
            $user = User::create([
                        'provider_id' => $userData->id,
                        'username' => $userData->email,
                        'name' => $userData->name,
                        'email' => $userData->email,
                        'avatar' => $userData->avatar,
                        'locale' => isset($userData->user['locale'])
            ]);

            if (isset($userdata->user['locale']))
                $user['locale'] = $userData->user['locale'];
            if (isset($userdata->user['gender']))
                $user['gender'] = $userData->user['gender'];
           
           // $userrole= Role::where('name','=','user');    
            // attaching user role
           // $user->attachRole($admin); // parameter can be an Role object, array, or id

            // or eloquent's original technique
           // $user->roles()->attach($userrole->id); // id only
        }

        $this->checkIfUserNeedsUpdating($userData, $user, $provider);
        return $user;
    }

    public function checkIfUserNeedsUpdating($userData, $user, $provider) {

         // getting user information socialite services
        $socialData = [
            'name' => $userData->name,
            'avatar' => $userData->avatar,
            'email' => $userData->email,
            'username' => $userData->email,
            'locale' => isset($userData->user['locale'])
        ];

        if (isset($userdata->user['locale']))
            $socialData['locale'] = $userData->user['locale'];
        if (isset($userdata->user['gender']))
            $socialData['gender'] = $userData->user['gender'];

       
            
        //getting data from database used while updating
            
        $dbData = [
            'name' => $user->name,
            'locale' => $user->user['locale'],
            'avatar' => $user->avatar,
            'email' => $user->email,
            'username' => $user->email,
            'locale' => $user->user['locale']
        ];

        if (isset($userdata->user['locale']))
            $dbData['locale'] = $user->user['locale'];
        if (isset($userdata->user['gender']))
            $dbData['gender'] = $user->user['gender'];

        if (!empty(array_diff($socialData, $dbData))) {
            $user->name = $userData->name;
            if (isset($userdata->user['gender']))
            $user->gender = $userData->user['gender'];
            if (isset($userdata->user['locale']))
                $user->locale = $userData->user['locale'];
      

        if (!empty(array_diff($socialData, $dbData))) {
            $user->name = $userData->name;
            $user->gender = $userData->user['gender'];            
            $user->locale = isset($userData->user['locale']);
            $user->avatar = $userData->avatar;
            $user->email = $userData->email;
            $user->username = $userData->email;
            $userrole = DB::table('roles')->where('name', 'user')->pluck('id');
            
            $userroleexits = DB::table('role_user')->where('user_id', $user->id)->pluck('user_id');
           // $userrole= Role::where('name','=','user');    
            // attaching user role
           // $user->attachRole($admin); // parameter can be an Role object, array, or id
            //dd($userrole);
            // or eloquent's original technique
            
            if(!$userroleexits)
            $user->roles()->attach($userrole); // id only
           // $user->attachRole($userrole);
            
            $user->save();
        }
    }

}
