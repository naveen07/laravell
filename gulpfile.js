var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less('app.less');
    //mix.styles([
    //    'custom.css'
    //],'public/output/final.css','public/css');
    mix.scriptsIn("public/js/app");
    mix.stylesIn("public/css/app");
    //mix.stylesIn("lic/css");
    //mix.scripts(["jquery.select2.js","select2.full.js"],"public/js/main.js");
    mix.version(["css/app/all.css","js/app/all.js"]);
});
