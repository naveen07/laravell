<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'User',
		'secret' => '',
	],
        
      'facebook' => [
            'client_id' => '432703476887791',
            'client_secret' =>'9de859809b855119eb9f900240d8ecb9',
            'redirect' => 'http://localhost:8000/login/facebook'
        ], 
    
     /*   'facebook' => [
            'client_id' => '960077617389162',
            'client_secret' =>'3ba18028636f818642d346c91bf2532d',
            'redirect' => 'http://localhost:8000/login/facebook'
        ], */
     
        'google' => [
            'client_id' => '99956943137-tc5uscdfg7tapok01adf10klttoeedm5.apps.googleusercontent.com',
            'client_secret' =>'pMylmhjHrFhFwbjODzXgsN7b',
            'redirect' => 'http://localhost:8000/login/google'
        ]
    
];
