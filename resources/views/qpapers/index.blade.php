@extends('master')
@section('content')

 {!! HTML::style('css/custom.css'); !!}
  {!! HTML::style('css/social-buttons.css'); !!}

<div class="row col-sm-12">
     <div class="col-sm-3">
  {!! Form::select('year', array('Years') + $years, null,['id'=>'year','class'=>'form-control filterable','multiple'=>'multiple']) !!}
   </div>
  <div class="col-sm-3">
 {!! Form::select('type', array('1'=>'Previous paper','2'=>'Model paper'),  null,['id'=>'type','class'=>'form-control filterable','multiple'=>'multiple']) !!}
 </div>
     <div class="col-sm-3">
  {!! Form::select('syllabas', array('Syllabas') + $syllabas, null,['id'=>'syllabas','class'=>'form-control filterable','multiple'=>'multiple']) !!}
     </div>
       <div class="col-sm-3">
  {!! Form::select('class', array('Class') + $classes, null,['id'=>'class','class'=>'form-control filterable','multiple'=>'multiple']) !!}
       </div>
</div>
     
      @if(Session::has('notification'))
        <div class="alert alert-info">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
         <h4>{{ Session::get('notification') }}</h4>
        </div>
      @endif
         
 <div class="qpaperdata col-sm-12">
@include('/qpapers/items')
 </div>
  {!! HTML::script('js/filter.js') !!}
 @stop
