          <ul id="qplist" class="list-unstyled">
       
        @foreach ($question_papers as $question_paper)            
        <li>
        <div class="col-md-12" id="qpdata">
        <!--<div class="pull-left">-->
        <h3>{{ $question_paper->title }}{!! link_to_route('qp.destroy','',[$question_paper->id],['class'=>'pull-right rest-delete glyphicon glyphicon-remove pull-right','data-method'=>"DELETE" ,'csrf-token'=>"$token",'data-confirm'=>"Are you sure?"]) !!}</h3>
        <b>Uploaded at:</b> {{ $question_paper->created_at }}
            <b>Year:</b> {!! $question_paper->year !!}
            <b>Syllabas:</b> {!! get_resourcename('syllabas',$question_paper->syllabas) !!}
            <b>Class:</b> {!! get_resourcename('class',$question_paper->class) !!}
        <a href="{{ URL::to('https://s3-us-west-2.amazonaws.com/vqpapers/'. $question_paper->qpfile)  }}" id="download-paper" class="pull-right glyphicon">Download</a>   
        <!--</div> -->

        <!--{!! Form::open(['method'=>'DELETE', 'route'=>['qp.destroy',$question_paper->id]]) !!}-->
        <!--{!! Form::submit('Delete',['class' => 'btn btn-danger']) !!}-->
        <!--{!! Form::close() !!}-->

</div></li>
        @endforeach 
        </ul>
<script>restful.init($('.rest-delete'));</script>
  {!! $question_papers->appends(['q' => $q])->render(); !!}