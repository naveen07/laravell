@extends('master')

@section('content')
<!-- Latest compiled and minified CSS -->
{!! HTML::script('https://www.google.com/recaptcha/api.js'); !!}
 {!! HTML::style('css/custom.css'); !!}

<div class="row" id="createqp">
<div class="col-sm-10">
<h2 class="col-sm-10" id="uploadheader">Upload</h2>
  <div class="clearfix"></div>
{!! Form::open(['route'=>'qp.store','files'=>true,'class'=>"form-horizontal"]) !!}

<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
{!! Form::label('title','Title',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-6">
{!! Form::text('title',null,['class'=>'form-control']) !!}
    {!! $errors->first('title','<span class="help-block">:message</span>') !!}
</div>
</div>
    
<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
{!! Form::label('type','Type',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-6">
{!! Form::select('type', array(''=>'--Select--','1'=>'Previous paper','2'=>'Model paper'),  null,['class'=>'form-control']) !!}
{!! $errors->first('qtype','<span class="help-block">:message</span>') !!}
</div>
</div>
    
<div class="form-group {{ $errors->has('year') ? 'has-error' : '' }}">
{!! Form::label('year','Year',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-6">
{!! Form::select('year', $years, null,['class'=>'form-control searchable']) !!}
{!! $errors->first('year','<span class="help-block">:message</span>') !!}
</div>
</div>
    
<div class="form-group {{ $errors->has('syllabas') ? 'has-error' : '' }}">
{!! Form::label('syllabas','Syllabas',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-6">
{!! Form::select('syllabas', array(''=>'--Select--') + $syllabas, null,['class'=>'form-control searchable']) !!}
{!! $errors->first('syllabas','<span class="help-block">:message</span>') !!}
</div>
</div>
    
<div class="form-group">
{!! Form::label('class','Class',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-6">
{!! Form::select('class', array('--Select--') + $classes, null,['class'=>'form-control searchable']) !!}
</div>
</div>
    
<div class="form-group {{ $errors->has('qpfile') ? 'has-error' : '' }}">
{!! Form::label('file','File',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-6">
{!! Form::file('qpfile',['class'=>'form-control']) !!}
{!! $errors->first('qpfile','<span class="help-block">:message</span>') !!}
</div>
</div>
    
<div class="form-group">
<div class="col-sm-offset-2 col-sm-6">
{!! app('captcha')->display(); !!}
</div>
</div>
    
<div class="form-group">
<div class="col-sm-offset-2 col-sm-6">
{!! Form::submit('Upload',['class'=>'btn btn-primary'])  !!}
{!! link_to('/','Cancel',['class'=>'btn btn-default']) !!}
</div>
</div>
{!! Form::close() !!}
</div>
</div>
<script type="text/javascript">
  $('.searchable').select2({
   placeholder: "-- Select --",
allowClear: true
  });
</script>
@stop