{!! Form::model($song,['route'=>['song_update_path',$song->slug],'method'=>'PATCH']) !!}
@extends('master')
@section('content')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/css/select2.min.css" />
<script src="/js/select2.full.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
        
<h2>Upload Question Paper</h2>
{!! Form::model(['route'=>['update'],'files'=>true,'class'=>"form-horizontal"]) !!}

<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
{!! Form::label('title','Title',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-10">
{!! Form::text('title',null,['class'=>'form-control']) !!}
    {!! $errors->first('title','<span class="help-block">:message</span>') !!}
</div>
</div>
    
<div class="form-group {{ $errors->has('qtype') ? 'has-error' : '' }}">
{!! Form::label('qtype','Type',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-10">
{!! Form::select('qtype', array(''=>'--Select--','1'=>'Previous paper','2'=>'Model paper'),  null,['class'=>'form-control']) !!}
{!! $errors->first('qtype','<span class="help-block">:message</span>') !!}
</div>
</div>
    
<div class="form-group {{ $errors->has('year') ? 'has-error' : '' }}">
{!! Form::label('year','Year',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-10">
{!! Form::select('year', $years, null,['class'=>'form-control searchable']) !!}
{!! $errors->first('year','<span class="help-block">:message</span>') !!}
</div>
</div>
    
<div class="form-group {{ $errors->has('syllabas') ? 'has-error' : '' }}">
{!! Form::label('syllabas','Syllabas',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-10">
{!! Form::select('syllabas', array(''=>'--Select--') + $syllabas, null,['class'=>'form-control searchable']) !!}
{!! $errors->first('syllabas','<span class="help-block">:message</span>') !!}
</div>
</div>
    
<div class="form-group">
{!! Form::label('class','Class',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-10">
{!! Form::select('class', array('--Select--') + $classes, null,['class'=>'form-control searchable']) !!}
</div>
</div>
    
<div class="form-group {{ $errors->has('qpfile') ? 'has-error' : '' }}">
{!! Form::label('file','File',['class'=>"col-sm-2 control-label"]) !!}
<div class="col-sm-10">
{!! Form::file('qpfile',['class'=>'form-control']) !!}
{!! $errors->first('qpfile','<span class="help-block">:message</span>') !!}
</div>
</div>
    
<div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<div class="g-recaptcha" data-sitekey="6LfugwATAAAAAOuuuTsC98pXb64OOIW-Mcf-PghZ"></div>
</div>
</div>
    
<div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
{!! Form::submit('Upload',['class'=>'btn btn-primary'])  !!}
{!! link_to_route('qp.index','Cancel',null,['class'=>'btn btn-default']) !!}
</div>
</div>
{!! Form::close() !!}

<script type="text/javascript">
  $('.searchable').select2({
   placeholder: "-- Select --",
allowClear: true
  });
</script>
@stop