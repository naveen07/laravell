<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{!! csrf_token() !!}" />
<link href='http://www.google.com/fonts#UsePlace:use/Collection:Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<title>VQ Papers</title>
{!! HTML::style(elixir("css/app/all.css")) !!}
{!! HTML::script(elixir("js/app/all.js")) !!}
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>
    <body>
        <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
    {!! link_to_route('qp.create','Upload',null,['class'=>'btn btn-primary pull-right upload-button','multiple'=>'multiple']) !!}
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">{!! HTML::image('images/Logo.png') !!}</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
@if(!Auth::check())
<li><a href="login/facebook" class="btn btn-xs btn-facebook pull-right"><i class="fa fa-facebook"></i> | Connect with Facebook</a></li>
<li><a href="login/google" class="btn btn-xs btn-google-plus pull-right"><i class="fa fa-google-plus"></i> | Connect with Google</a> </li>
@endif
@if(Auth::check())
    <li>{!! Auth::user()->name !!}</li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <img src={!! Auth::user()->avatar !!} width=25 height=25 /> <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="logout" > Logout </a></li>

              </ul>
            </li>
@endif
          </ul>

        </div><!--/.nav-collapse -->
      </div>
    </nav>
        <div class="container" id="middle-container" >
            @yield('content')
        </div>
    </body>
</html>
