<!DOCTYPE html>
<html lang="en">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href='http://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
{!! HTML::style('css/error.css'); !!}
    <body>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>