<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionPaperTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('questionpapers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('qpfile');
			$table->bigInteger('type');
			$table->bigInteger('year');
			$table->bigInteger('syllabus');
			$table->bigInteger('class');
			$table->bigInteger('subject');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('questionpapers');
	}

}
